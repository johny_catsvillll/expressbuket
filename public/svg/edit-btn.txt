<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M16.8334 9.80176L17.5391 9.09564C18.267 8.36814 18.0787 7.37664 17.3516 6.64914C16.6245 5.92164 15.6326 5.73339 14.9047 6.46164L14.199 7.16776" stroke="#008AC1" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M6.81152 14.5534L9.44627 17.1881M15.6803 10.9545L16.8334 9.80175L14.1983 7.16663L13.0455 8.31938L15.6803 10.9545Z" stroke="#008AC1" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M12 23.25C18.2132 23.25 23.25 18.2132 23.25 12C23.25 5.7868 18.2132 0.75 12 0.75C5.7868 0.75 0.75 5.7868 0.75 12C0.75 18.2132 5.7868 23.25 12 23.25Z" stroke="#008AC1" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M9.44625 17.1881L15.6803 10.9545L13.0455 8.3194L6.8115 14.5534L6 17.9996L9.44625 17.1881V17.1881Z" stroke="#008AC1" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
</svg>
