function strstr(haystack, needle, bool) {
    var pos = 0;

    haystack += "";
    pos = haystack.indexOf(needle); if (pos == -1) {
        return false;
    } else {
        if (bool) {
            return haystack.substr(0, pos);
        } else {
            return haystack.slice(pos);
        }
    }
}

function checkSum() {
    var sum = parseInt($('input[name="standartPrice"]').val());

    if($.session.get("upak") == 1) {
        sum += 150;
    }

    if($.session.get('stoik') == 1) {
        sum += 50
    }

    if($.session.get("green") == 1) {
        sum += 200;
    }

    if(sum < 500) {
        sum = sum + 300;
        $('.zone-price').text('300 р');
    }
    else {
        $('.zone-price').text('бесплатно');
    }

    var promo = $('input[name="promo"]').val();

    if(promo) {
        sum = sum - 500;
    }

    $('.js-total-price').text(sum);
    $('input[name="sum"]').val(sum);

}

$(document).ready(function(){


    checkSum();
    if($('input[name="sum"]').val() < 500) {
        $('input[name="sum"]').val(+$('input[name="sum"]').val() + 300);
    }


    $('.otkrytka-text').keyup(function() {
        var el = $(this);
        var val = el.val();
        $.session.set('otkrytka-text', val);
    });


    if($.session.get('otkrytka-text')) {
        $('.otkrytka-text').val($.session.get('otkrytka-text'));
    }

    if($.session.get('stoik') == undefined && $('#stoik').length > 0) {
        $('#stoik')[0].click();
        $.session.set("stoik",1);
        checkSum();
    }

    $('.cart-prices-row .radio-btn input').change(function() {
        var el = $(this);
        var val = el.val();
        $.session.set("city", val);
    });

    $.each($('.cart-prices-row .radio-btn'), function(indx, element) {
        var el = $(element).find('input');
        var val = el.val();
        if($.session.get("city") == val) {
            el.prop('checked', 'checked');
        }
    });


    if($.session.get("upak") == 1) {
        $('#upak').prop('checked', 'checked');
        $('input[name="sum"]').val(+$('input[name="sum"]').val() + 150);
        $('.js-total-price').text(+$('input[name="sum"]').val());
        checkSum();
    }

    if($.session.get("green") == 1) {
        $('#green').prop('checked', 'checked');
        $('input[name="sum"]').val(+$('input[name="sum"]').val() + 200);
        $('.js-total-price').text(+$('input[name="sum"]').val());
        checkSum();
    }

    if($.session.get("stoik") == 1) {
        $('#stoik').prop('checked', 'checked');
        $('input[name="sum"]').val(+$('input[name="sum"]').val() + 50);
        $('.js-total-price').text(+$('input[name="sum"]').val());
        checkSum();
    }


    if($.session.get("checkotkr") == 1) {
        $('#checkotkr').prop('checked', 'checked');
    }

    if($.session.get("photo") == 1) {
        $('#photo').prop('checked', 'checked');
    }

    if($.session.get("question-address") == 1) {
        //$('#question-address input').prop('checked', 'checked');
        $('#question-address')[0].click();
        $('#address').attr('data-validation','');
        $('#address').attr('placeholder','Узнать адрес доставки у получателя');
        $('#address').val('Узнать адрес доставки у получателя');
    }


    if($.session.get("call_before_order") == 1) {
        $('#call_before_order input').prop('checked', 'checked');
    }


    $('#call_before_order').click(function() {
        if($("#call_before_order input").prop("checked")){
            $.session.set("call_before_order",1);
        }
        else{
            $.session.set("call_before_order",0);
        }
    });



    /*if(strstr(location.href,'page/korzina')){
     if(!Cookies.get('gift-modal2')){
     Cookies.set('gift-modal2', 'value', { expires: 7 });
     $('#gift-modal').modal('show');
     }
     }*/
    /*if(!Cookies.get('free_delivery')){
     Cookies.set('free_delivery', 'value', { expires: 7 });
     $('#delivery-modal').modal('show');
     }*/
    var count=1;
    var extra=0;
    var color=0;
    var color_title;
    $('.fancybox').fancybox({helpers: {
        overlay: {
            locked: false
        }
    }
    });
    var count=1;
    var temp;


    $(".phone").mask("+7 (999) 999-9999");
    var times=["06:00","06:30","07:00","07:30","08:00","08:30","09:00","09:30","10:00","10:30","11:00","11:30","12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00","19:30","20:00","20:30","21:00","21:30","22:00","22:30","23:00","23:30","00:00"];var $range=$("#range");var d=new Date();
    var today=("0" + (d.getDate())).slice(-2)+"."+("0" + (d.getMonth() + 1)).slice(-2)+"."+d.getFullYear();
    var tomorrow=("0" + (d.getDate()+1)).slice(-2)+"."+("0" + (d.getMonth() + 1)).slice(-2)+"."+d.getFullYear();
    var hours=d.getHours();
    $('#pay-form').sisyphus({
        excludeFields: $( ":hidden" ),
    });


    $range.ionRangeSlider();
    var slider=$range.data("ionRangeSlider");
    $('#checkotkr').click(function(){

        if($("#checkotkr").prop("checked")){
            $.session.set("checkotkr",1);
        }
        else{
            $.session.set("checkotkr",0);
        }

        $(".textotkr").fadeToggle("normal");
        $.post('/setsum/otkrytka', { val: 'otkrytka' }, function (response) {});
    });
    $('#photo').click(function(){

        if($("#photo").prop("checked")){
            $.session.set("photo",1);
        }
        else{
            $.session.set("photo",0);
        }

    });
    /*$('input[name="zonename"]').click(function(){
     $.post('/setsum/city', { val: 'city', city: $(this).val() }, function (response) {});
     });*/

    $('#question-address').click(function(){
        if($('#question-address').find('input').prop('checked')){
            $.session.set("question-address",0);
            $('#address').attr('data-validation','');
            $('#address').val('Узнать адрес доставки у получателя');
        }
        else{
            $('#address').attr('data-validation','required');
            $('#address').val('');
            $.session.set("question-address",1);
        }
    });
    if(hours>=19)
        $('#picker').datepicker({inline:true,changeYear:false,changeMonth:true,minDate:1,});
    else
        $('#picker').datepicker({inline:true,changeYear:false,changeMonth:true,minDate:0,});
    // $('#oldsum').val($('.model-sum').html());
    // $('#sum').val($('.model-sum').html());
    var sum = parseInt($('#sum').val());
    var sumcart = parseInt($('#sumcart').val());
    $('body').on("change", '#picker', function(){

        var time=d.getHours()+3+":00";
        var day = $("#picker").val().substr(0, $("#picker").val().length - 5);
        if(day=='14.02' || day=='06.03' || day=='07.03' || day=='08.03'){
            $('.date-slider').hide();
            $('.time-title').hide();
            $('.time-title2').show();
            $('.date-hint').html('Доставка '+ day +' осуществляется в течение дня по предварительному звонку, при этом эффект сюрприза сохраняется');
            //$('#sum').val(sum);
            /*sumcart = parseInt($('#sumcart').val());
             var nsum = parseInt($('#sum').val());
             $('#sum').val(parseInt(sumcart + 0.3*sumcart+parseInt($('#zone_price').val())));
             var newsum = $('#sum').val();
             $('.order-price').html(newsum + ' <i class="glyphicon glyphicon-ruble"></i>');
             $('.upsum').html('РЎС‚РѕРёРјРѕСЃС‚СЊ Р±СѓРєРµС‚Р° РІ РїСЂР°Р·РґРЅРёС‡РЅС‹Рµ РґРЅРё СѓРІРµР»РёС‡РёР»Р°СЃСЊ РёР·-Р·Р° РїРѕРІС‹С€РµРЅРёСЏ С†РµРЅ РїРѕСЃС‚Р°РІС‰РёРєРѕРІ. Р¦РµРЅР° Р·Р°РєР°Р·Р° СЃ РґРѕСЃС‚Р°РІРєРѕР№ '
             + newsum + ' СЂСѓР±.');
             $('.upsum').show();
             sumcart = parseInt(newsum - $('.delivery-price').attr('data-price'));*/
        }
        else{
            $('.date-slider').show();
            $('.time-title').show();
            $('.time-title2').hide();
            $('.date-hint').html('В период утро и вечер стоимость доставки - '+$('.delivery-price').attr('data-price-2')+'рублей');
            //$('#sum').val(sum);
            sumcart = parseInt($('#sumcart').val());
            //$('.order-price').html(sum + ' <i class="glyphicon glyphicon-ruble"></i>');
            $('.upsum').hide();
            sumcart = parseInt($('#sum').val() - $('.delivery-price').attr('data-price'));
        };

        if(today==$("#picker").val()){
            console.log('я сюда зачем зашел?');
            for(find in times){
                if(times[find]==time){
                    slider.update({from:find,from_min:find,to:find+4,});
                    var from=find;
                }
            }
            $('.timerange').each(function(i,elem){
                if(parseInt(from)>parseInt($(this).attr('data-from')))
                    $(this).addClass('disable');
                else $(this).removeClass('disable');
                $(this).removeClass("active");
            });
        }
        else{
            if(tomorrow==$("#picker").val() && hours>=19){
                slider.update({from:6,from_min:10,to:28,});
            }
            else{
                slider.update({from:6,from_min:0,to:28,});
            }

            $('.timerange').each(function(i,elem){
                if(parseInt(from)>parseInt($(this).attr('data-from'))){
                    $(this).addClass('disable');
                }
                else {
                    if(10>parseInt($(this).attr('data-from')) && tomorrow==$("#picker").val() && hours>=19){
                        $(this).addClass('disable');
                    }
                    else
                        $(this).removeClass('disable');
                }
                if($(this).hasClass("active")){
                    $(this).removeClass("active")
                };
                if($(this).attr('data-from')==6)
                    $(this).addClass("active")
            });
            /* 2 СЏРЅРІР°СЂСЏ СЃ 10 С‡Р°СЃРѕРІ */
            if(day=='02.01'){
                slider.update({from:6,from_min:8,to:28,});
                $('.timerange').each(function(i,elem){
                    if(parseInt(6)>parseInt($(this).attr('data-from')))
                        $(this).addClass('disable');
                    else $(this).removeClass('disable');
                    $(this).removeClass("active");
                });

            }
        }

    });
    $('.timerange').click(function(){
        $('.timerange').each(function(i,elem){
            if($(this).hasClass("active")){
                $(this).removeClass("active")
            };
        });
        $(this).addClass('active');
        slider.update({
            from:$(this).attr('data-from'),
            to:$(this).attr('data-to'),
            from_min:$(this).attr('data-from'),
        });
        if(today!=$("#picker").val()){
            if(tomorrow==$("#picker").val() && hours>=19)
                slider.update({from_min:10,});
            else
                slider.update({from_min:0,});
        }
        else{
            var time=d.getHours()+3+":00";
            for(find in times){
                if(times[find]==time){
                    var from=find;
                }}
            if(from<$(this).attr('data-from')){
                slider.update({from_min:from,});
            }
        }
        if($("#picker").val()=='02.01.2019'){
            slider.update({from_min:8,});
            $('.timerange').each(function(i,elem){
                if(parseInt(6)>parseInt($(this).attr('data-from')))
                    $(this).addClass('disable');
                else $(this).removeClass('disable');
                $(this).removeClass("active");
            });

        }
        $('.delivery-from').html($('.irs-from').html());
        $('.delivery-to').html($('.irs-to').html());
        if($(this).hasClass("double")){
            $('.delivery-price').html($('.delivery-price').attr('data-price-2'));
            $('.wp-text').text('включая платную доставку');
            $('#zone_price').val(parseInt($('.delivery-price').html()));
            $('#sum').val(parseInt(sumcart)+parseInt($('#zone_price').val()));
            $('.order-price').html($('#sum').val() + ' <i class="glyphicon glyphicon-ruble"></i>');
            $('.upsum').html('Стоимость букета в праздничные дни увеличилась из-за повышения цен поставщиков. Цена заказа с доставкой '
                + $('#sum').val() + ' руб.');
        }
        else{
            $('.delivery-price').html($('.delivery-price').attr('data-price'));
            $('.wp-text').text('включая бесплатную доставку');
            $('#zone_price').val(parseInt($('.delivery-price').html()));
            $('#sum').val(parseInt(sumcart)+parseInt($('#zone_price').val()));
            $('.order-price').html($('#sum').val() + ' <i class="glyphicon glyphicon-ruble"></i>');
            $('.upsum').html('Стоимость букета в праздничные дни увеличилась из-за повышения цен поставщиков. Цена заказа с доставкой '
                + $('#sum').val() + ' руб.');
        }
    });
    $range.on("change",function(){

        var $this=$(this),value=$this.prop("value").split(";"),from=$this.data("from"),to=$this.data("to");
        $('.delivery-from').html(value[0]);
        $('.delivery-to').html(value[1]);
        if(from>27||to<7){
            $('.delivery-price').html($('.delivery-price').attr('data-price-2'));
            $('#zone_price').val(parseInt($('.delivery-price').html()));
            $('#sum').val(parseInt(sumcart)+parseInt($('#zone_price').val()));
            console.log($('#sum').val());
            $('.order-price').html($('#sum').val() + ' <i class="glyphicon glyphicon-ruble"></i>');
            $('.wp-text').text('включая платную доставку');

            if($('#sum').val() <= 500) {
                $('#sum').val(+$('#sum').val() + 300);
                $('.wp-text').text('включая платную доставку');
            }


            //$('#sum').val(parseInt(sumcart+parseInt($('#zone_price').val())));
            //$('.order-price').html($('#sum').val() + ' <i class="glyphicon glyphicon-ruble"></i>');
            $('.upsum').html('Стоимость букета в праздничные дни увеличилась из-за повышения цен поставщиков. Цена заказа с доставкой '
                + $('#sum').val() + ' руб.');
        }
        else{

            $('#sum').val(parseInt(sumcart)+parseInt($('#zone_price').val()));
            $('.wp-text').text('включая платную доставку');
            $('.order-price').html($('#sum').val() + ' <i class="glyphicon glyphicon-ruble"></i>');
            if($('input[name="sum"]').val() <= 500) {
                $('.delivery-price').html($('.delivery-price').attr('data-price2'));
                $('.wp-text').text('включая платную доставку');
            }
            else {
                $('.delivery-price').html($('.delivery-price').attr('data-price'));
                $('.wp-text').text('включая бесплатную доставку');
            }

            $('#zone_price').val(parseInt($('.delivery-price').html()));
            //$('#sum').val(parseInt(sumcart+parseInt($('#zone_price').val())));$('.order-price').html($('#sum').val() + ' <i class="glyphicon glyphicon-ruble"></i>');
            $('.upsum').html('Стоимость букета в праздничные дни увеличилась из-за повышения цен поставщиков. Цена заказа с доставкой '
                + $('#sum').val() + ' руб.');
        }
    });
    $('.checkotkr').click(function(){$(".textotkr").toggle();});
    $('.delotkr').click(function(){$('.show-text').hide();$(this).hide();$('.checkotkr1').toggle();$(".textotkr").hide();$(".otkrytka-text").val('');});if($("#checkotkr").prop("checked")){$(".textotkr").show();};
    /* РґРѕСЃС‚Р°РІРєР° */
    $('#pay-form1').on('click','.radio-btn',function(){
        /*if($(this).find('input').data('price')==0)
         $('.zone-price').html('Р±РµСЃРїР»Р°С‚РЅРѕ');
         else
         $('.zone-price').html($(this).find('input').data('price'));*/
        $('input[name="zone"]').val($(this).find('input').val());
        /*$('input[name="zone_price"]').val($(this).find('input').data('price'));
         $('.itogo .cart-prices-price b').html(parseInt($('.cart-total .cart-prices-price b').html())+$(this).find('input').data('price'));
         if($("#stoik").prop("checked")){
         $('.itogo .cart-prices-price b').html(parseInt($('.itogo .cart-prices-price b').html())+50);
         }
         if($("#upak").prop("checked")){
         $('.itogo .cart-prices-price b').html(parseInt($('.itogo .cart-prices-price b').html())+150);
         }
         if($("#green").prop("checked")){
         $('.itogo .cart-prices-price b').html(parseInt($('.itogo .cart-prices-price b').html())+200);
         }
         $('.stage12 input[name="sum"]').val($('.itogo .cart-prices-price b').html());
         $.session.set("sum",$('.stage12 input[name="sum"]').val());
         $.post('/setsum/'+$.session.get("sum"), { val: $.session.get("sum") }, function (response) {});*/
        $.session.set("zone",$('input[name="zone"]').val());

        //$.post('/setzone/'+$.session.get("zone"), { val: $.session.get("zone") }, function (response) {});
    });
    /* Р”РѕРїРѕР»РЅРµРЅРёСЏ Рє Р±СѓРєРµС‚Сѓ */
    $('body').on('click', '#stoik', function(){
        if($("#stoik").prop("checked")){
            $.session.set("stoik",1);
            var add1 = $('.stage12 input[name="sum"]').val(parseInt($('.stage12 input[name="sum"]').val())+50);
        }
        else{
            $.session.set("stoik",0);
            var add1 = $('.stage12 input[name="sum"]').val(parseInt($('.stage12 input[name="sum"]').val())-50);
        }

        //$('.itogo .cart-prices-price').html('<b>'+parseInt($('.stage12 input[name="sum"]').val())+'</b><i class="glyphicon glyphicon-ruble"></i>');
        checkSum();
    });
    $('body').on('click', '#upak', function(){
        if($("#upak").prop("checked")){
            $.session.set("upak",1);

            var add1 = $('.stage12 input[name="sum"]').val(parseInt($('.stage12 input[name="sum"]').val())+150);
        }
        else{
            $.session.set("upak",0);
            var add1 = $('.stage12 input[name="sum"]').val(parseInt($('.stage12 input[name="sum"]').val())-150);
        }

        //$('.itogo .cart-prices-price').html('<b>'+parseInt($('.stage12 input[name="sum"]').val())+'</b><i class="glyphicon glyphicon-ruble"></i>');
        checkSum();
    });
    $('body').on('click', '#green', function(){

        if($("#green").prop("checked")){
            $.session.set("green",1);
            var add1 = $('.stage12 input[name="sum"]').val(parseInt($('.stage12 input[name="sum"]').val())+200);
        }
        else{
            $.session.set("green",0);
            var add1 = $('.stage12 input[name="sum"]').val(parseInt($('.stage12 input[name="sum"]').val())-200);
        }

        //$('.itogo .cart-prices-price').html('<b>'+parseInt($('.stage12 input[name="sum"]').val())+'</b><i class="glyphicon glyphicon-ruble"></i>');
        checkSum();
    });

    if($('#promo').val()){
        $.ajax({
            type: 'post',
            url: "/findpromo",
            data: {'promocode':$('#promo').val(),'sum': parseInt($('.stage12 input[name="sum"]').val())}
        }).done(function( txt ) {
            console.log(txt);
            if(txt==0)
                $(".promo-text").html('<span class="red">Промокод не найден</span>');
            else{
                if(isNaN(txt))
                    $(".promo-text").html('<span class="red">' + txt + '</span>');
                else {
                    $(".promo-text").html('Скидка - ' + txt + ' руб.');
                    // if($("#stoik").prop("checked"))
                    //     $( "#stoik" ).prop( "disabled", true );
                    // if($("#upak").prop("checked"))
                    //     $( "#upak" ).prop( "disabled", true );
                    // if($("#green").prop("checked"))
                    //     $( "#green" ).prop( "disabled", true );
                    var add2 = $('.stage12 input[name="sum"]').val(parseInt($('.stage12 input[name="sum"]').val())-parseInt(txt));
                    $.session.set("sum",$('.stage12 input[name="sum"]').val());
                    $.post('/setsum/usepromo', { val: 'usepromo' }, function (response) {});
                    $.post('/setsum/'+$.session.get("sum"), { val: $.session.get("sum") }, function (response) {});
                    $('.itogo .cart-prices-price').html($('.stage12 input[name="sum"]').val()+'<i class="glyphicon glyphicon-ruble"></i>');
                    $('.active-promo').remove();
                }

            }
        });
    }

    /* РџРѕРёСЃРє РїСЂРѕРјРѕРєРѕРґРѕ */
    $('.active-promo').on("click",function(){
        if($('#promo').val())
            $.ajax({
                type: 'get',
                url: "/findpromo",
                data: {'promocode':$('#promo').val(),'sum': parseInt($('.stage12 input[name="sum"]').val())}
            }).done(function( txt ) {
                console.log(txt);
                if(txt==0)
                    $(".promo-text").html('<span class="red">Промокод не найден</span>');
                else{
                    if(isNaN(txt))
                        $(".promo-text").html('<span class="red">' + txt + '</span>');
                    else {
                        $(".promo-text").html('Скидка - ' + txt + ' руб.');
                        // if($("#stoik").prop("checked"))
                        //     $( "#stoik" ).prop( "disabled", true );
                        // if($("#upak").prop("checked"))
                        //     $( "#upak" ).prop( "disabled", true );
                        // if($("#green").prop("checked"))
                        //     $( "#green" ).prop( "disabled", true );
                        var add2 = $('.stage12 input[name="sum"]').val(parseInt($('.stage12 input[name="sum"]').val())-parseInt(txt));
                        $('.itogo .cart-prices-price').html($('.stage12 input[name="sum"]').val()+'<i class="glyphicon glyphicon-ruble"></i>');
                        $('.active-promo').remove();
                    }

                }
            });
        else
            $(".promo-text").html('<span class="red">Введите промокод!</span>');
    });
    $('form .payment-method').click(function(){
        $(this).find('input').prop('checked',true);
        $(this).parent().submit()
    });
    $('select.address').change(function(){
        if($(this).val()=='Нужно сделать сюрприз')
            $('.address-hint').html('Мы повезем букет <span class="tooltips">без звонка получателю<span class="hidden-xs">В случае отсутствия получателя в указанному адресу в момент доставки, курьер ожидает 15 минут. По истечении 15 минут заказ будет возвращен в салон. Если Вы сомневаетесь, что получатель будет на месте, выберете пункт «Уточнить адрес и время доставки». При звонке мы не скажем про цветы)</span></span> в указанное Вами время');
        else
            $('.address-hint').html('Мы позвоним получателю и уточним удобно ли принять заказ в указанное Вами время. При звонке мы не скажем про цветы.');
    });
    $('.thumbnails a').click(function(){$('.thumbnails a').each(function(i,elem){if($(this).hasClass("active")){$(this).removeClass("active")};});$(this).addClass('active');$('.product-page-img').find('img').attr('src',$(this).attr("href"));return false;});
    $('.photos-slider').lightSlider({
        item:3,
        slideMove:2,
        easing:'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:600,
        responsive:[{breakpoint:1350,settings:{item:3,slideMove:1,slideMargin:6,}},
            {breakpoint:1250,settings:{item:2,slideMove:1}},
            {breakpoint:900,settings:{item:1,slideMove:1}}]});
    $('.team-slider').lightSlider({
        item:1,
        slideMove:1,
        easing:'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:600,
        slideMargin:0,
    });
    $('.photos-slider2').lightSlider({
        item:3,
        slideMove:2,
        easing:'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:600,
        responsive:[{breakpoint:1350,settings:{item:3,slideMove:1,slideMargin:6,}},
            {breakpoint:1250,settings:{item:2,slideMove:1}},
            {breakpoint:900,settings:{item:1,slideMove:1}}]});
    $('#tab-instagram').click(function(){
        $('#instagram').css('visibility','visible');
        $('#instagram').css('margin-top','0');
    });
    $('.testimonials-slider').lightSlider({item:3,slideMove:2,easing:'cubic-bezier(0.25, 0, 0.25, 1)',speed:600,responsive:[{breakpoint:1350,settings:{item:2,slideMove:1,slideMargin:6,}},{breakpoint:1250,settings:{item:1,slideMove:1}},]});
    $('.products-slider').lightSlider({item:4,slideMove:2,slideMargin:2,easing:'cubic-bezier(0.25, 0, 0.25, 1)',speed:600,responsive:[{breakpoint:1200,settings:{item:3,slideMove:1,slideMargin:6,}},{breakpoint:550,settings:{item:2,slideMove:1}},]});
    $('.product').hover(function(){
        $(this).find('.catalog-consist-inner').slideDown('fast');
    },function(){
        $(this).find('.catalog-consist-inner').slideUp('fast');
    });
    $('#toTop').UItoTop({easingType:'easeOutQuart'});


    $('.js-cart-count .js-count-plus').click(function() {

        var count = $('.js-count').text();
        $('.js-count').text(+count + 1);
        calculatePrice();

    });

    $('.js-cart-count .js-count-minus').click(function() {

        var count = $('.js-count').text();
        var typeProduct = $('input[name="is_single_item"]').val();

        if(typeProduct != 0 && count <= 7) {
            return false;
        }

        if(count > 1) {

            $('.js-count').text(+count - 1);
            calculatePrice();
        }

    });


    $('body').on('click', '.item-cart-count .js-count-plus', function() {

        var el = $(this);
        var count = el.closest('.count').find('.js-count').val();
        el.closest('.count').find('.js-count').val(+count + 1);
        changeCart('count', el.closest('tr').data('id'), el);
    });

    $('body').on('click', '.item-cart-count .js-count-minus', function() {

        var el = $(this);
        var count = el.closest('.count').find('.js-count').val();
        var typeProduct = el.closest('tr').data('single');

        if(typeProduct != 0 && count <= 7) {
            return false;
        }

        if(count > 1) {
            el.closest('.count').find('.js-count').val(+count - 1);
            changeCart('count', el.closest('tr').data('id'), el);
        }

    });

    $('body').on('click', '.delete-product', function() {
        var el = $(this);
        var productId = el.closest('tr').data('id');
        changeCart('delete', productId, el);

    });

    function changeCart(type, productId, element) {

        var count = element.closest('tr').find('.js-count').val();
        var color = element.closest('tr').data('color');
        var isExtra = element.closest('tr').data('extra');
        var stoik = $('#stoik').prop('checked');
        var upak = $('#upak').prop('checked');
        var green = $('#green').prop('checked');

        $.ajax({
            type: 'get',
            data: {type:type, productId:productId, count:count, stoik:stoik, upak:upak, green:green, color:color, extra:isExtra},
            url: '/page/korzina',
            success:function(result) {

                if(result.countProducts == 0) {
                    $('.ww-wrap').html(result.viewEmpty);
                    $('.wrap-price ').hide();
                    $('.cart-top-icon .badge').hide();
                }
                else {
                    $('.cart-table').html(result.view);
                    $('.cart-total .cart-prices-price b, .wrap-price .cart-top-price').text(result.allPrice);
                    $('.cart-top-icon .badge').text(result.countProducts);
                    $('.itogo .cart-prices-price b').text(result.priceWithPluses);
                    $('input[name="sum"]').val(result.priceWithPluses);
                    $('input[name="standartPrice"]').val(result.standartPrice);
                    checkSum();
                }
            }
        });


    }


    function calculatePrice() {

        var typeProduct = $('input[name="is_single_item"]').val();

        if(typeProduct != 0) {
            var count = $('.js-count').text();
            var price = $('input[name="price"]').val();
            var old_price = $('input[name="old_price"]').val();
            if(old_price) {
                $('.product-oldprice span b').text(+price * count);
                $('b[itemprop="price"]').text(+price * count);
            }
            else {
                $('b[itemprop="price"]').text(+price * count);
            }

        }
        else {
            var price = $('input[name="price"]').val();
            var old_price = $('input[name="old_price"]').val();

            var count = $('.js-count').text();

            var type = $('.variant-btn.active').data('type');

            if(type != 'extra') {

                if(old_price) {
                    $('.product-oldprice span b').text(+old_price * count);
                    $('b[itemprop="price"]').text(+price * count);
                }
                else {
                    $('b[itemprop="price"]').text(+price * count);
                }

            }
            else {

                var extra_price = $('input[name="extra_price"]').val();
                var extra_price_old = $('input[name="extra_old_price"]').val();

                if(extra_price_old) {
                    $('.product-oldprice span b').text(+extra_price_old * count);
                    $('b[itemprop="price"]').text(+extra_price_old * count);
                }
                else {
                    $('b[itemprop="price"]').text(+extra_price * count);
                }

            }
        }




    }

    $('.variant-btn').click(function() {
        var el = $(this);
        $('.variant-btn').removeClass('active');
        el.addClass('active');

        var typeProduct = $('input[name="is_single_item"]').val();

        if(typeProduct != 0) {
            $('.js-cart-count').find('.js-count').text(el.text());
        }

        calculatePrice();
    });

    $('.catalog-buy__button').click(function(e) {
        e.preventDefault();
        var el = $(this);
        var count = $('.js-count').text();

        if(!count) {
            count = 1;
        }

        var productId = el.data('id');
        var color = '';

        var isColor = $('input[name="color"]').val();

        if(isColor) {
            color = isColor;
        }

        var extra = '';

        var isExtra = $('.isExtra a.active').data('type');

        if(isExtra) {
            extra = isExtra;
        }

        $.ajax({
            type: 'get',
            data: {productId: productId, count:count, color:color, extra:extra},
            url: '/add-to-cart',
            success:function(result) {
                window.location.href = el.attr('href');
            }
        });


    });

    $('.btn-add-to-favorite').click(function() {
        var el = $(this);
        var productId = el.closest('.product').data('id');

        $.ajax({
            type: 'get',
            url: '/add-to-favorite',
            data:{productId:productId},
            success:function(result) {
                if(result.count) {
                    $('.link-favorite strong').text(result.count);
                }
                else {
                    $('.link-favorite strong').text('');
                }

                if(result.marker) {
                    el.find('svg path').removeClass('active-favorite');
                }
                else {
                    el.find('svg path').addClass('active-favorite');
                }
            }
        });


    });


    $('.clear-list-favorite').click(function(e) {
        e.preventDefault();
        $.ajax({
            type: 'get',
            url: '/clear-list-favorite',
            success:function(result) {
                $('.catalog').html('<h3 class="text-empty-favorite-list">Ваш список избранного пуст. Добавьте сюда понравившиеся букеты или другие товары, кликнув на сердечко в любой карточке товара.</h3>');
            }
        });

    });

    $.validate({
        onSuccess: function ($form) {
            $('.final-order').attr('disabled', 'disabled');
            return true;
        }
    });


    $.validate({
        onSuccess: function ($form) {
            $('.btn-send-form').attr('disabled', 'disabled');
            return true;
        }
    });



    $('.one-rules span').click(function() {
        var el = $(this);
        if(el.hasClass('active')) {
            el.removeClass('active');
        }
        else {
            el.addClass('active');
        }

        el.closest('div').find('.wrap-content-rules').slideToggle();
    });

    $('.btn-check-status').click(function() {

        $('#status .error-field').hide();
        var numberOrder = $('input[name="number"]').val();

        $.ajax({
            type:'get',
            url: '/check-number-order',
            data: {numberOrder:numberOrder},
            success:function(result) {
                if(result == 1) {
                    window.location.href = '/status-order?order=' + numberOrder;
                }
                else {
                    $('#status .error-field').show();
                }
            }
        });

    });

    $('.btn-auth').click(function() {

        var email = $('#auth input[name="email"]').val();
        var password = $('#auth input[name="password"]').val();

        $.ajax({
            type: 'get',
            url: '/check-is-user',
            data: {email:email, password:password},
            success:function(result) {
                if(result == 1) {
                    window.location.href = '/account';
                }
                else {
                    $('#auth .error-field').show();
                }
            }
        });


    });

    $('.color-label').click(function() {
        var el = $(this);
        var name = el.data('name');
        $('.colorbox-title').text('цвет - ' + name);
    });

    $('.see-products .content-see ul').mouseover(function() {
        $('.see-products').css({width: 180});
        $('.see-products h4').addClass('styleh4');
    }).mouseout(function() {
        $('.see-products').css({width: 100});
        $('.see-products h4').removeClass('styleh4');
    });


    $('.btn-hide-element').click(function() {
        var el = $(this);
        if(el.attr('data-active') == 1) {
            el.attr('data-active', 0);
            el.closest('div').animate({'right': -100});
        }
        else {
            el.attr('data-active', 1);
            el.closest('div').animate({'right': 0});
        }


    });

    $('.file-popup a').click(function(e) {
        e.preventDefault();
        $('.file-popup input[name="file"]').click();

    });


    $('.callback-popup input[type="submit"]').click(function(e) {
        e.preventDefault();


        var token = $('.callback-popup form input[name="_token"]').val();
        var name = $('.callback-popup form input[name="name"]').val();
        var phone = $('.callback-popup form input[name="phone"]').val();
        var text = $('.callback-popup form textarea').val();
        var file = $('.file-popup input[name="file"]')[0].files;
        var data = new FormData();


        // $.each(file, function(key, value) {
        //     data.append(key, value);
        // });
        data.append('image', file[0]);
        data.append('_token', token);
        data.append('name', name);
        data.append('phone', phone);
        data.append('text', text);

        console.log(data);

        $.ajax({
            type: 'post',
            url: '/save-application',
            data:data,
            cache:false,
            dataType: 'json',
            processData:false,
            contentType:false,
            success:function(result) {
                console.log('here');

                $('.callback-popup').modal('hide');

                var d = $.fn.dialog({
                    disposeOnHide: true,
                    title: 'Спасибо, Ваша заявка принята!',
                    content: 'Наш менеджер свяжется с Вами в ближайшее время',
                    cssClass: 'order_dialog'
                });
                d.setVisible(true);
                setTimeout(function () {
                    d.setVisible(false);
                }, 3000)
            }
        });

    });


});


