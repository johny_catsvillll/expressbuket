(function($){
	var openedDialog = null;
	
	var Dialog = function() {
		// private
		this.element_ = null;
		this.backgroundEl_ = null;
		this.titleEl_ = null;
		this.titleTextEl_ = null;
		this.titleCloseEl_ = null;
		this.contentEl_ = null;
		this.buttonEl_ = null;
		this.visible_ = false;
		
		// public
		this.cssClass = null;
		this.hasTitleCloseButton = true;
		this.backgroundElementOpacity = 0.5;
		this.disposeOnHide = true;
		this.title = '';
		this.content = '';
		this.buttons = [];
	};
	
	Dialog.prototype.addButton = function(title, opt_cssClass, opt_callback, opt_default) {
		opt_callback = opt_callback || $.proxy(this.hide_, this);
		opt_default = opt_default || false;
		opt_cssClass = opt_cssClass || 'goog-buttonset';

		var button = $('<button class="'+opt_cssClass+(opt_default ? '-default' : '')
				+'">'+title+'</button>');
		
		this.buttonEl_.append(button);
		button.bind('click.dialog', opt_callback);
	};
	
	Dialog.prototype.getElement = function() {
		return this.element_;
	};
	
	Dialog.prototype.render = function() {
		if (openedDialog)
			return;
		
		this.element_ = $('<div class="modal-dialog'+(this.cssClass ? ' '+this.cssClass : '')
			+'"></div>');
		this.titleEl_ = $('<div class="modal-header"></div>');
		this.titleTextEl_ = $('<h4 class="modal-title">'+this.title+'</h4>');
		this.titleCloseEl_ = $('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>');
		this.titleEl_.append(this.titleCloseEl_, this.titleTextEl_);
		
		this.contentEl_ = $('<div class="modal-body"></div>');

		if (typeof this.content === 'string') {
			this.contentEl_.html(this.content);
		} else {
			this.contentEl_.append(this.content);
			this.content.show();
		}
		
		this.buttonEl_ = $('<div class="modal-dialog-buttons"></div>');
		this.element_.append(this.titleEl_, this.contentEl_, this.buttonEl_);
		
		this.titleCloseEl_.toggle(this.hasTitleCloseButton);
		
		this.backgroundEl_ = $('<div class="modal-dialog-bg"></div>');
		this.backgroundEl_.css({'opacity': this.backgroundElementOpacity});
		this.backgroundEl_.hide();
		
		for (var i = 0, button; button = this.buttons[i]; i++) {
			this.addButton(
				button['title'],
				button['cssClass'],
				button['callback'], 
				button['isDefault']
			);
		}
		
		this.element_.hide();
		
		$('body').append(this.backgroundEl_, this.element_);
		
		this.titleCloseEl_.bind('click.dialog', $.proxy(this.onTitleCloseClick_, this));
		this.backgroundEl_.bind('click', $.proxy(this.onTitleCloseClick_, this));
	};
	
	Dialog.prototype.dispose = function() {
		this.titleCloseEl_.unbind('.dialog');
		this.backgroundEl_.remove();
		this.buttons_ = [];
		
		if (typeof this.content === 'object') {
			this.content.hide();
			$('body').append(this.content);
			this.content = null;
		}
		
		this.element_.remove();
		this.element_ = this.backgroundEl_ = null;
	};
	
	Dialog.prototype.onTitleCloseClick_ = function() {
		this.setVisible(false);
	};
	
	Dialog.prototype.setVisible = function(visible) {
		if (visible == this.visible_)
			return;
		
		if (visible) {
			if (!openedDialog)
				this.show_();
		} else {
			this.hide_();
		}
	};
	
	Dialog.prototype.show_ = function() {
		this.resizeBackground_();
		this.reposition();		
		$(window).bind('resize.dialog', $.proxy(this.resizeBackground_, this));
		
		this.showPopup_(true);
		this.visible_ = true;
		
		openedDialog = this;
	};

	Dialog.prototype.hide_ = function() {
		$(window).unbind('resize.dialog');
		this.showPopup_(false);
		this.visible_ = false;
		
		openedDialog = null;
		
		if (this.disposeOnHide)
			this.dispose();
	};
	
	Dialog.prototype.showPopup_ = function(visible) {	
		if (visible){
			this.backgroundEl_.show();
			this.element_.show();
		} else{
			this.backgroundEl_.hide();
			this.element_.hide();
		}
	};
	
	Dialog.prototype.resizeBackground_ = function() {
		this.backgroundEl_.hide();
		
		var viewportWidth = $(window).width();
		var viewportHeight = $(window).height();
		var w = Math.max(viewportWidth,
			Math.max(document.body.scrollWidth, document.documentElement.scrollWidth));
		var h = Math.max(viewportHeight,
			Math.max(document.body.scrollHeight, document.documentElement.scrollHeight));
		
		this.backgroundEl_.show();
		this.backgroundEl_.width(w);
		this.backgroundEl_.height(h);
	};
	
	Dialog.prototype.reposition = function() {
		if (this.element_.css('position') == 'fixed') {
			var x = 0;
			var y = 0;
		} else {
			var x = $(document).scrollLeft();
			var y = $(document).scrollTop();
		}
		
		var popupWidth = this.element_.width();
		var popupHeight = this.element_.height();
		var viewportWidth = $(window).width();
		var viewportHeight = $(window).height();
		
		var left = Math.max(x + viewportWidth / 2 - popupWidth / 2, 0);
		var top = Math.max(y + viewportHeight / 2 - popupHeight / 2, 0);
		this.element_.css({ top: top, left: left});
	};
	
	$.fn.extend({
		dialog: function(options) {
			options = options || {};
			
			var dialog = new Dialog();
			$.extend(dialog, options);
			dialog.render();
			
			return dialog;
		},
		getOpenedDialog: function() {
			return openedDialog;
		}
	});
	$(".modal-dialog-content form[action='/site/quick']#form0-submit").click(function(){
		console.log("work");
	});
})(jQuery);
