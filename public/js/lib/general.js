﻿(function ($) {
    var headerHeight = 100;
    var host = "https://" + window.location.hostname;
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > headerHeight) {
            $('.navbar-fixed').addClass('fixed-top');
            $('.categories-menu').addClass('fixed-top');
        } else {
            $('.navbar-fixed').removeClass('fixed-top');
            $('.categories-menu').removeClass('fixed-top');
        }
    });
    var __registerFormValidator = function (form, opt_zindex, opt_callback) {
        var validator = new FormValidator(
            form,
            [
                {name: 'name', rules: ''},
                {name: 'phone', rules: 'required'},
                {name: 'email', rules: 'valid_email'}
            ],
            function (errors, evt) {
                var jForm = $(form);
                var success = errors.length ? false : true;
                if (!success) {
                    var error = errors[0];
                    var inputEl = jForm.find('input[name=' + error.name + ']');
                    var tooltip = new Opentip(inputEl, {target: inputEl, showOn: null, removeElementsOnHide: true});
                    tooltip.setContent(error.message);
                    if (opt_zindex) {
                        Opentip.lastZIndex = opt_zindex ? opt_zindex : 1000;
                    }
                    tooltip.show();
                }
                else {
                    $.ajax(
                        {
                            type: "POST",
                            url: jForm.attr('action'),
                            data: jForm.serialize(),
                            success: function (data) {
                                var d = $.fn.dialog({
                                    disposeOnHide: true,
                                    title: 'Спасибо, Ваша заявка принята!',
                                    content: 'Наш менеджер свяжется с Вами в ближайшее время',
                                    cssClass: 'order_dialog'
                                });
                                d.setVisible(true);
                                setTimeout(function () {
                                    d.setVisible(false);
                                }, 3000)
                            }
                        });
                    jForm.find("input[type=text]").val('');
                }
                if (opt_callback) {
                    opt_callback(success);
                }
                if (evt && evt.preventDefault) {
                    evt.preventDefault();
                }
                else if (event) {
                    event.returnValue = false;
                }
            })
            .setMessage('required', 'Это поле обязательно для заполнения')
            .setMessage('valid_email', ' Введите корректный адрес эл. почты')
            .setMessage('max_length', 'Телефон не должн превышать 11 цифр')
            .setMessage('alpha_numeric', 'Поле должно содержать только цифры');
    };
    var __registerFormValidator2 = function (form, opt_zindex, opt_callback) {
        var validator = new FormValidator(form, [{name: 'phone', rules: 'required'}], function (errors, evt) {
            var jForm = $(form);
            var success = errors.length ? false : true;
            if (!success) {
                var error = errors[0];
                var inputEl = jForm.find('input[name=' + error.name + ']');
                var tooltip = new Opentip(inputEl, {target: inputEl, showOn: null, removeElementsOnHide: true});
                tooltip.setContent(error.message);
                if (opt_zindex) {
                    Opentip.lastZIndex = opt_zindex ? opt_zindex : 1000;
                }
                tooltip.show();
            } else {
                $.ajax({
                    type: "get", url: jForm.attr('action'), data: jForm.serialize(), success: function (data) {
                        var d = $.fn.dialog({
                            disposeOnHide: true,
                            title: 'Спасибо, Ваша заявка принята!',
                            content: 'Наш менеджер свяжется с Вами в ближайшее время',
                            cssClass: 'order_dialog'
                        });
                        d.setVisible(true);
                        setTimeout(function () {
                            d.setVisible(false);
                        }, 3000)
                    }
                });
                jForm.find("input[type=text]").val('');
            }
            if (opt_callback) {
                opt_callback(success);
            }
            if (evt && evt.preventDefault) {
                evt.preventDefault();
            } else if (event) {
                event.returnValue = false;
            }
        }).setMessage('required', 'Это поле обязательно для заполнения').setMessage('alpha_numeric', 'Поле должно содержать только цифры');
    };
    var __registerFormValidator3 = function (form, opt_zindex, opt_callback, variant) {
        var validator = new FormValidator(form, variant, function (errors, evt) {
            var jForm = $(form);
            var success = errors.length ? false : true;
            if (!success) {
                var error = errors[0];
                var inputEl = jForm.find('input[name=' + error.name + ']');
                var tooltip = new Opentip(inputEl, {target: inputEl, showOn: null, removeElementsOnHide: true});
                tooltip.setContent(error.message);
                if (opt_zindex) {
                    Opentip.lastZIndex = opt_zindex ? opt_zindex : 1000;
                }
                tooltip.show();
            } else {
                $.ajax({
                    type: "POST", url: jForm.attr('action'), data: jForm.serialize(), success: function (data) {
                        var d = $.fn.dialog({
                            disposeOnHide: true,
                            title: 'Спасибо, Ваша заявка принята!',
                            content: 'Наш менеджер свяжется с Вами в ближайшее время',
                            cssClass: 'order_dialog'
                        });
                        d.setVisible(true);
                        setTimeout(function () {
                            d.setVisible(false);
                        }, 3000)
                    }
                });
                jForm.find("input[type=text]").val('');
            }
            if (opt_callback) {
                opt_callback(success);
            }
            if (evt && evt.preventDefault) {
                evt.preventDefault();
            } else if (event) {
                event.returnValue = false;
            }
        })
            .setMessage('required', 'Это поле обязательно для заполнения')
            .setMessage('valid_email', ' Введите корректный адрес эл. почты')
            .setMessage('alpha_numeric', 'Поле должно содержать только цифры');
    };
    var __makeFormElement = function (name, placeholder, required) {
        if (required == undefined) required = false;
        if (required)
            return '<div class="row"><input class="order-input" type="text" required id="' + name + '0" name="' + name + '" placeholder="' + placeholder + '" /></div>';
        else
            return '<div class="row"><input class="order-input" type="text" id="' + name + '0" name="' + name + '" placeholder="' + placeholder + '" /></div>';
    };

    jQuery(function ($) {
        var dates = new Array('01.01.2019', '08.03.2019');
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '<Пред',
            nextText: 'След>',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Нед',
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            beforeShowDay: disable
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        function disable(d) {
            var dat = $.datepicker.formatDate("dd.mm.yy", d);
            for (var i = 0; i < dates.length; i++) {
                if ($.inArray(dat, dates) != -1)  return [false];
                else return [true];
            }
        };
        // $('.__cmd_order').click(function (event) {
        //     var content = '<form id="form0" action="/save-application" method="get">'
        //         + __makeFormElement('name', 'Введите имя:')
        //         + __makeFormElement('phone', 'Введите телефон*:', 1)
        //         + '<p class="text-center"><input type="checkbox" name="accepted" required="" checked="checked">&nbsp;Согласен с обработкой персональных данных</p><p class="text-center">Приступая к заполнению формы, я подтверждаю, что ознакомлен<br> c <a style="text-decoration:underline;" target="_blank" href="' + host + '/page/politika-konfidentsialnosti">положением об обработке и защите персональных данных</a></p>'
        //         + '<div class="text-center"><button id="form0-submit" class="btn white-button">Отправить</button></div>'
        //         + '</form>';
        //     var dialog = $.fn.dialog({
        //         disposeOnHide: true,
        //         title: 'Мы свяжемся с Вами в течение нескольких минут.',
        //         content: content, cssClass: 'order_dialog'
        //     });
        //     __registerFormValidator2(dialog.getElement().find('form').get(0), 700, function (success) {
        //         if (success) dialog.setVisible(false);
        //     });
        //     dialog.setVisible(true);
        //     $("#phone0").mask("+7 (999) 999-9999");
        // });
        $('.quick_btn').click(function (event) {
            var content = '<form id="form0" action="/site/quick" method="POST">'
                + __makeFormElement('name', 'Введите имя:')
                + __makeFormElement('phone', 'Введите телефон*:', 1)
                + '<p class="text-center"><input type="checkbox" name="accepted" required="" checked="checked">&nbsp;Согласен с обработкой персональных данных</p><p class="text-center">Приступая к заполнению формы, я подтверждаю, что ознакомлен<br> c <a style="text-decoration:underline;" target="_blank" href="' + host + '/page/politika-konfidentsialnosti">положением об обработке и защите персональных данных</a></p>'
                + '<input type="hidden" name="bid" value="'
                + $(this).attr("id").substr(1) + '" />'
                + '<div class="text-center"><button id="form0-submit" class="btn white-button">Заказать</button></div>' + '</form>';
            var dialog = $.fn.dialog({
                disposeOnHide: true,
                title: 'Мы свяжемся с Вами в течение нескольких минут.',
                content: content,
                cssClass: 'order_dialog'
            });
            __registerFormValidator2(dialog.getElement().find('form').get(0), 700, function (success) {
                if (success) dialog.setVisible(false);
            });
            dialog.setVisible(true);
            $("#phone0").mask("+7 (999) 999-9999");
        });
        $("body").on("click", ".modal-dialog-content form[action='/site/quick']", function () {
            window.yaCounter31145141.reachGoal('IV-CALL');
        });
        $("body").on("click", "input[name='subscribe']", function () {
            window.yaCounter31145141.reachGoal('IV-SUBSCRIBE');
        });
        $("body").on('click', "span.catalog-buy__button", function () {
            window.yaCounter31145141.reachGoal("IV-BUY-BUTTON");
        });
        $("body").on('click', "div.top-cart-item a.top-cart-item__buy", function () {
            window.yaCounter31145141.reachGoal("IV-BUY-BASKET");
        });
        $("body").on('click', "div.top-cart-item a.top-cart-item__buy", function () {
            window.yaCounter31145141.reachGoal("IV-BUY-BASKET");
        });
        $("body").on('click', "form#pay-form input[type='submit']", function () {
            console.log("OFFER");
            window.yaCounter31145141.reachGoal("IV-OFFER");
        });
    });
})(jQuery); 