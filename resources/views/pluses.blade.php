@extends('layout')

@section('content')
    <div class="container">
        <div class="row news">
            <div class="breadcrumbs">
                <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url"><span itemprop="title">Главная</span></a></div>
                <div class="active"><span>Преимущества</span></div>
            </div>
            <i class="hidden-xs hicon"></i>
            <h1 class="col-xs-12 title">Преимущества</h1>
            <div class="content">
                @foreach($advantages as $advantage)
                    <div class="col-xs-12 col-sm-6 col-md-3 why-we-block">
                    <div class="why-we-img"><img src="{{ asset('storage/'. $advantage->image) }}" alt="{{ $advantage->title }}" class="img-responsive"></div>
                    <span class="why-we-title">{{ $advantage->title }}</span><br>
                    <span class="why-we-description">{{ $advantage->description }}</span>
                </div>
                    @if($loop->iteration % 4 == 0)
                        <div class="clearfix">&nbsp;</div>
                    @endif
                @endforeach

            </div>
        </div>
    </div>
    @include('blocks.news')
@endsection