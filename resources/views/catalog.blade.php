@extends('layout')



@section('seo')
	@if(isset($category))
		<title>{{ isset($category) ? $category->name : '' }}</title>
		<meta name="keywords" content="{{ isset($category) ? $category->meta_keywords : '' }}">
		<meta name="description" content="{{ isset($category) ? $category->meta_description : '' }}">
	@else
		@parent
	@endif
@endsection


@section('content')
	<div class="container">
		<div class="breadcrumbs">
			<div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<a href="/" itemprop="url"><span itemprop="title">Главная</span></a>
			</div>
			@if(isset($category))
				<div class="active">
					<a href="/" itemprop="url"><span itemprop="title">Каталог</span></a>
				</div>
				<div class="active"><span>{{ $category->name }}</span></div>
			@else
				<div class="active"><span>Каталог</span></div>
			@endif
		</div>
		<i class="hidden-xs hicon"></i>
		@if(isset($category) && $category)
			<h1 class="col-xs-12 title">{{ $category->h1 ? $category->h1 : $category->name }}</h1>
		@else
			@if($priceName || $flowerName || $whomName || $factName || $styleName || $specName)
				<h1 class="col-xs-12 title">ФИЛЬТР {{ $priceName.' '.$flowerName.' '. $whomName.' '. $factName.' '.$styleName.' '.$specName }}</h1>
			@else
				<h1 class="col-xs-12 title">Каталог букетов</h1>
			@endif
		@endif
		<div class="category-content">
			@if(isset($category))
				{!! $category->top_text !!}
			@else
				@php
					$catalog = \LaravelAdminPanel\Models\Page::where('slug', Request::path())->first();
				@endphp
				@if(request()->all())

				@else
					{!! $catalog->top_text !!}
				@endif
			@endif
		</div>
		@include('blocks.filter')
		<div class="catalog">
			@foreach($products as $product)
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">

						<div class="product" data-id="{{ $product->id }}">
						<div class="product-inner">
							<span class="product-size">{{ $product->width }} {{ isset($product->height) ? 'x '. $product->height. ' см' : ''}}</span>
							@if($product->is_hit)
								<span class="product-label product-hit"><i>!</i>хит продаж</span>
							@endif

							@if($product->is_new)
								<span class="product-label product-newest"><i><img src="/img/icons/newest.png" alt="Новинка"></i>новинка</span>
							@endif

							@if($product->old_price)
								<span class="product-label product-sale"><i>%</i>букет со скидкой</span>
							@endif

							@if($product->old_price)
								<span class="product-oldprice line-trough"><span><b>{{ $product->old_price }}</b> <span class="hidden-xs">руб</span></span></span>
							@endif

							<a href="{{ route('product', ['slug' => $product->slug]) }}" title="{{ $product->name }}">
								<img class="product-img img-responsive"
									 src="{{ url('storage/'.$product->main_img) }}" alt="{{ $product->name }} - Expressbuket" title="{{ $product->name }} - Expressbuket">
								<div class="catalog-consist">
									<div class="catalog-consist-inner" style="display: none;">
										@if($product->composition)
											{!! str_limit(strip_tags($product->composition), 190, '...') !!}
										@else

											@php
												$products1 = [];

                                                foreach (json_decode($product->construktor_sting) as $key => $pr) {
                                                    $product1 = \App\Product::find($pr->itemId);
                                                    $product1->count = $pr->count;
                                                    array_push($products1, $product1);
                                                }
											@endphp

											@foreach($products1 as $pr)
												{{ $pr->name.' - '. $pr->count }} штук <br>
											@endforeach

										@endif
									</div>
								</div>
							</a>
							<div class="text-center product-title ">
								<a href="{{ route('product', ['slug' => $product->slug]) }}" title="{{ $product->name }}">{{ $product->name }}</a>
							</div>
							<div class="text-center product-price">
								@if($product->price)
									<b>{{ $product->price }}</b>
								@elseif(json_decode($product->construktor_sting))
									<b>{{ getHelperPrice($product->construktor_sting) }}</b>
								@endif
								<span class="glyphicon glyphicon-ruble" aria-hidden="true"></span>
								<a href="javascript:void(0);" class="btn-add-to-favorite" title="Добавить в избранное">
									<svg width="32" height="28" viewBox="0 0 32 28" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path @if(!(session('favorite') && isset(session('favorite')[$product->id]))) class="active-favorite" @endif d="M23.5 0.5C20.25 0.5 17.05 2.6 16 5.5C14.95 2.6 11.75 0.5 8.5 0.5C6.51088 0.5 4.60322 1.29018 3.1967 2.6967C1.79018 4.10322 1 6.01088 1 8C1 14.5 6.5 21 16 27.5C25.5 21 31 14.5 31 8C31 6.01088 30.2098 4.10322 28.8033 2.6967C27.3968 1.29018 25.4891 0.5 23.5 0.5Z" fill="#FF8877" stroke="#FF8877" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
									</svg>

								</a>
							</div>
						</div>
						<div class="catalog-buttons">
							<div class="text-center">
								<a id="64" class="col-xs-11 white-button"
								   href="{{ route('product', ['slug' => $product->slug]) }}">Заказать <i class="why-we-arr"></i></a>
							</div>
							<div class="text-center">
								<a id="b{{ $product->id }}" class="one-click quick_btn">Купить в 1 клик</a>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
		{{ $products->links() }}
	</div>
	<div class="container category-content">
		@if(isset($category))
			{!! $category->bottom_text !!}
		@else
		@if(request()->all())

		@else
			{!! $catalog->bottom_text !!}
		@endif

		@endif
	@include('blocks.news')

@endsection
