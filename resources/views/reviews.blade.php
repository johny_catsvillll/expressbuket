@extends('layout')
@section('content')
    <div class="container">
        <div class="breadcrumbs">
            <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/" itemprop="url"><span itemprop="title">Главная</span></a></div>
            <div class="active"><span>Отзывы</span></div>
        </div>
        <div class="row">
            @include('blocks.left-sidebar')
            <div class="col-xs-12 col-md-8 content testimonials">
                <h1 class="content-title float-left">{{ $seo ? $seo->h1 : '' }}</h1>
                <div class="testimonials-form float-right">
                    @if($message)
                        <p class="note text-center">{{ $message }}</p>
                    @else
                        <button type="submit" name="" class="white-button" data-toggle="modal" data-target="#testModal">
                            Оставить отзыв <i class="why-we-arr"></i></button>
                        <div id="testModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                        <h4 class="modal-title">Вы можете оставить свой отзыв в форме ниже.</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form enctype="multipart/form-data" id="otzivi-form" action="/page/otzyvy"
                                              method="post">
                                            {{ csrf_field() }}
                                            <label for="Otzivi_title" class="required">Имя <span
                                                        class="required">*</span>
                                            </label>
                                            <input data-validation="required" data-validation-error-msg="Обязательное поле"
                                                   size="60" maxlength="255" class="order-input" placeholder="Ваше имя"
                                                   required="required" name="name" id="Otzivi_title" type="text">
                                            <label for="Otzivi_order_id" class="required">Номер заказа <span
                                                        class="required">*</span></label>
                                            <input data-validation="required" data-validation-error-msg="Обязательное поле"
                                                   size="60" maxlength="255" class="order-input" placeholder="Номер заказа"
                                                   required="required" name="order_id" id="Otzivi_order_id"
                                                   type="text">
                                            <label for="Otzivi_content" class="required">Текст <span
                                                        class="required">*</span></label>
                                            <textarea data-validation="required"
                                                      data-validation-error-msg="Обязательное поле" class="textarea"
                                                      placeholder="Ваш отзыв" required="required" name="text"
                                                      id="Otzivi_content"></textarea>
                                            <p class="text-center">
                                            <div class="g-recaptcha" data-sitekey="6LegwZEUAAAAAL5CoGCLv7LSVBDqfiqv88-oUDkq"></div>
                                            </p>
                                            <p class="text-center">
                                                <button type="submit" name="" class="white-button">Оставить отзыв <i
                                                            class="why-we-arr"></i></button>
                                            </p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="clearfix"></div>
                @foreach($reviews as $review)
                    <div itemscope="" itemtype="http://schema.org/Review">
                        <div itemprop="itemReviewed" itemscope="" itemtype="http://schema.org/LocalBusiness"
                             style="display:none">
                            <img itemprop="image" src="https://expressbuket.com/img/logo.png" alt="Expressbuket">
                            <span itemprop="name">Expressbuket</span>
                        </div>
                        <div itemprop="name" style="display:none;">Отзыв о магазине</div>
                        @if($review->text_link)

                            <div class="photo">

                                <img style="width:130px;" src="{{ asset('storage/'.$review->text_image) }}" alt="{{ $review->name }}">
                                <p class="text-center ot-btn"><a href="{{ $review->text_link }}"
                                                                 class="white-button m20"><span>Заказать такой</span></a>
                                </p>
                            </div>
                        @elseif($review->order)

                            @php
                                $products = (array)json_decode($review->order->products);
                                $product = \App\Product::where('id', $products[0]->productId)->first();
                            @endphp

                            @if(!$product)
                                @continue
                            @endif

                            <div class="photo">


                                <img style="width:130px;" src="{{ asset('storage/'.$product->main_img) }}" alt="{{ $review->name }}">
                                <p class="text-center ot-btn"><a href="{{ url('buket/'. $product->slug) }}"
                                                                 class="white-button m20"><span>Заказать такой</span></a>
                                </p>
                            </div>
                        @else

                            <div class="photo">
                                <img style="width:130px;" src="{{ asset('storage/'.$review->text_image) }}" alt="{{ $review->name }}">
                                <p class="text-center ot-btn">
                                </p>
                            </div>
                        @endif
                            <div class="testimonial-content">
                                <div class="testimonial-header" itemprop="author" itemscope=""
                                     itemtype="http://schema.org/Person">
                                    <span class="testimonial-title" itemprop="name">{{ $review->name }}</span>
                                    <span class="testimonial-date">{{ $review->created_at }}</span>
                                </div>
                                <div class="testimonial-text" itemprop="reviewBody">
                                    <p>{!! $review->text !!}</p>
                                </div>
                                <div itemprop="publisher" itemscope="" itemtype="http://schema.org/Person">
                                    <meta itemprop="name" content="{{ $review->name }}">
                                </div>
                            </div>



                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @include('blocks.news')

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection