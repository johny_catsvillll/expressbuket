@extends('layout')

@section('content')
    <div class="text-page">
        <div class="container">
            <div class="row">

                <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
                <div class="breadcrumbs">
                    <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="/" itemprop="url"><span itemprop="title">Главная</span></a></div><div class="active"><span>Контакты</span></div></div>
                @if($seo && $seo->sidebar)
                    @include('blocks.left-sidebar')
                @endif
                <div class="@if($seo && $seo->sidebar)col-xs-12 col-md-8 @endif content">
                    <h1 class="content-title">{{ $seo ? $seo->h1 : '' }}</h1>
                    <p><strong>Адреса магазинов:</strong></p>
                    @foreach($magazines as $magazine)
                        <p>{{ $loop->iteration }}. г. {{ $magazine->cityId->name }}, {{ $magazine->address }}</p>
                    @endforeach
                    {!! $seo ? $seo->top_text : '' !!}
                    <script type="text/javascript">
                        ymaps.ready(init);
                        function init(){
                            var myMap;
                            myMap = new ymaps.Map("map", {
                                center: [48.693998,44.715876],
                                zoom: 10,
                                controls: []
                            });
                            myMap.behaviors.disable('scrollZoom');

                            myMap.controls.add("zoomControl", {
                                position: {top: 15, left: 15}
                            });
                            var placemarks = [];
                            var coords = [
                                @foreach($magazines as $magazine)
                                    [{{ $magazine->coordinates }}],
                                @endforeach
                            ];
                            var names = [
                                @foreach($magazines as $magazine)
                                    'г. {{ $magazine->cityId->name }}, {{ $magazine->address }}',
                                @endforeach
                            ];
                            var placemark_style = { iconLayout: 'default#image',
                                iconImageHref: '{{ asset('upload/mark.png') }}',
                                iconImageSize: [51, 73],
                                iconImageOffset: [-25, -73] };
                            coords.forEach(function(item, i, arr) {
                                placemarks[i] = new ymaps.Placemark(coords[i] , {
                                        balloonContentHeader: "EXPRESS БУКЕТ",
                                        balloonContentBody: names[i]+"<br>Работаем круглосуточно",
                                        balloonContentFooter: "Телефон: +7 (8442) 500-731",
                                        hintContent: names[i]
                                    },
                                    placemark_style);
                                myMap.geoObjects.add(placemarks[i]);
                            });


                        }
                    </script>
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
    @include('blocks.news')
@endsection