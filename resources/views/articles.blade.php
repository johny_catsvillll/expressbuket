@extends('layout')

@section('content')
    <div class="text-page">
        <div class="container">
            <div class="row">

                <div class="container">
                    <div class="row news">
                        <div class="breadcrumbs">
                            <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                                <a href="/" itemprop="url"><span itemprop="title">Главная</span></a></div>
                            <div class="active"><span>Статьи</span></div>
                        </div>
                        @include('blocks.left-sidebar')
                        <div class="col-xs-12 col-md-8 content" style="float:right">
                            <h1 class="content-title">{{ $seo ? $seo->h1 : '' }}</h1>
                            <div id="listView">
                                <div id="ajaxListView" class="list-view">
                                    <div class="items">
                                        @foreach($articles as $article)
                                            <div class="media col-xs-12 col-md-6">
                                            <div class="media-left">
                                                <a href="{{ url('stati/'. $article->slug) }}">
                                                    <img class="media-object"
                                                         src="{{ asset('storage/'.$article->image) }}"
                                                         alt="{{ $article->name }}">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <span>{{ date('d.m.Y', strtotime($article->publication)) }}</span>
                                                <a href="{{ url('stati/'. $article->slug) }}"
                                                   title="{{ $article->name }}" class="media-heading">
                                                    {{ $article->name }}</a>
                                                {!! $article->exceprt !!}
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    {{ $articles->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @include('blocks.news')
@endsection