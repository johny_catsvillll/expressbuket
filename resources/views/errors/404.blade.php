@extends('layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="breadcrumbs">
                <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url"><span itemprop="title">Главная</span></a></div><div class="active"><span>404</span></div></div>	</div>
    </div>
    <h1 class="col-xs-12 title">
        Ошибка 404: страница не найдена
    </h1>

    <div class="container">
        <div class="catalog">
            <p>Что-то пошло не так, но запрашиваемой Вами страницы не существует или она была удалена.</p>
            <p>Но Вы можете вернуться на нашу <a href="/" title="Перейти на главную страницу">главную страницу</a> сайта или поискать подходящий букет на
                <a href="{{ url('page/katalog') }}" title="Перейти в каталог">странице каталога.</a></p>
        </div>
    </div>
@endsection