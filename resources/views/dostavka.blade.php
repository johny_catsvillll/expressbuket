@extends('layout')

@section('content')
    <div class="text-page">
        <div class="container">
            <div class="row">

                <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
                <div class="breadcrumbs">
                    <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="/" itemprop="url"><span itemprop="title">Главная</span></a></div>
                    <div class="active"><span>Доставка</span></div>
                </div>
                @if($seo && $seo->sidebar)
                    @include('blocks.left-sidebar')
                @endif
                <div class="@if($seo && $seo->sidebar )col-xs-12 col-md-8 @endif content">
                    <h1 class="content-title">{{ $seo ? $seo->h1 : '' }}</h1>
                    {!! $seo ? $seo->top_text : '' !!}
                </div>
            </div>
        </div>
    </div>
    @include('blocks.news')
@endsection