@extends('layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="breadcrumbs">
                <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url"><span itemprop="title">Главная</span></a></div><div class="active"><span>Успешна покупка</span></div></div>	</div>
    </div>
    <h1 class="col-xs-12 title">
        Товар успешно принят!
    </h1>

    <div class="container">
        <div class="catalog">
            <p>В скором времени с Вами свяжется менеджер для подтверждения информации.</p>
            <p>ы можете вернуться на нашу <a href="/" title="Перейти на главную страницу">главную страницу</a> сайта или поискать подходящий букет на
                <a href="{{ url('page/katalog') }}" title="Перейти в каталог">странице каталога.</a></p>
        </div>
    </div>
@endsection