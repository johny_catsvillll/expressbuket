@extends('layout')

@section('content')
    <div class="container">
        <div class="breadcrumbs">
            <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/" itemprop="url"><span itemprop="title">Главная</span></a>
            </div>
            <div class="active">
                <span>Избранное</span>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="catalog row">
            @if($products)
                @foreach($products as $product)
                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">

                        <div class="product" data-id="{{ $product->id }}">
                            <div class="product-inner">
                                <span class="product-size">{{ $product->width }} {{ isset($product->height) ? 'x '. $product->height : ''}} см</span>
                                @if($product->is_hit)
                                    <span class="product-label product-hit"><i>!</i>хит продаж</span>
                                @endif

                                @if($product->is_new)
                                    <span class="product-label product-newest"><i><img src="/img/icons/newest.png" alt="Новинка"></i>новинка</span>
                                @endif

                                @if($product->discount)
                                    <span class="product-label product-sale"><i>%</i>букет со скидкой</span>
                                @endif

                                <a href="{{ route('product', ['slug' => $product->slug]) }}" title="{{ $product->name }}">
                                    <img class="product-img img-responsive"
                                         src="{{ url('storage/'.$product->main_img) }}" alt="{{ $product->name }} - Expressbuket" title="{{ $product->name }} - Expressbuket">
                                    <div class="catalog-consist">
                                        <div class="catalog-consist-inner" style="display: none;">
                                            {{ str_limit(strip_tags($product->description), 100) }}
                                        </div>
                                    </div>
                                </a>
                                <div class="text-center product-title ">
                                    <a href="{{ route('product', ['slug' => $product->slug]) }}" title="{{ $product->name }}">{{ $product->name }}</a>
                                </div>
                                <div class="text-center product-price">
                                    <b>{{ $product->discount ? $product->price - ($product->price * $product->discount) / 100 : $product->price }}</b>
                                    <span class="glyphicon glyphicon-ruble" aria-hidden="true"></span>
                                    <a href="javascript:void(0);" class="btn-add-to-favorite" title="Добавить в избранное">
                                        <svg width="32" height="28" viewBox="0 0 32 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path @if(!(session('favorite') && isset(session('favorite')[$product->id]))) class="active-favorite" @endif d="M23.5 0.5C20.25 0.5 17.05 2.6 16 5.5C14.95 2.6 11.75 0.5 8.5 0.5C6.51088 0.5 4.60322 1.29018 3.1967 2.6967C1.79018 4.10322 1 6.01088 1 8C1 14.5 6.5 21 16 27.5C25.5 21 31 14.5 31 8C31 6.01088 30.2098 4.10322 28.8033 2.6967C27.3968 1.29018 25.4891 0.5 23.5 0.5Z" fill="#FF8877" stroke="#FF8877" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>

                                    </a>
                                </div>
                            </div>
                            <div class="catalog-buttons">
                                <div class="text-center">
                                    <a id="64" class="col-xs-11 white-button"
                                       href="{{ route('product', ['slug' => $product->slug]) }}">Заказать <i class="why-we-arr"></i></a>
                                </div>
                                <div class="text-center">
                                    <a id="b64" class="one-click quick_btn">Купить в 1 клик</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <h3 class="text-empty-favorite-list">Ваш список избранного пуст. Добавьте сюда понравившиеся букеты или другие товары, кликнув на сердечко в любой карточке товара.</h3>
            @endif
        </div>
        <p class="text-center btn-favorites">
            <a href="{{ url('page/katalog') }}" class="white-button clear-list-favorite">
                <strong>
                    <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13.5938 1.875H10.3125V1.38281C10.3125 1.01607 10.1668 0.664344 9.90748 0.405016C9.64816 0.145689 9.29643 0 8.92969 0L6.07031 0C5.70357 0 5.35184 0.145689 5.09252 0.405016C4.83319 0.664344 4.6875 1.01607 4.6875 1.38281V1.875H1.40625C1.28193 1.875 1.1627 1.92439 1.07479 2.01229C0.986886 2.1002 0.9375 2.21943 0.9375 2.34375C0.9375 2.46807 0.986886 2.5873 1.07479 2.67521C1.1627 2.76311 1.28193 2.8125 1.40625 2.8125H2.27344L2.92969 13.4062C2.9415 13.8252 3.1132 14.2238 3.40956 14.5201C3.70593 14.8165 4.10448 14.9882 4.52344 15H10.4766C10.8996 14.9941 11.304 14.825 11.6054 14.5281C11.9067 14.2311 12.0817 13.8292 12.0938 13.4062L12.7266 2.8125H13.5938C13.7181 2.8125 13.8373 2.76311 13.9252 2.67521C14.0131 2.5873 14.0625 2.46807 14.0625 2.34375C14.0625 2.21943 14.0131 2.1002 13.9252 2.01229C13.8373 1.92439 13.7181 1.875 13.5938 1.875ZM5.625 1.38281C5.625 1.26471 5.67192 1.15144 5.75543 1.06793C5.83894 0.984417 5.95221 0.9375 6.07031 0.9375H8.92969C9.04779 0.9375 9.16106 0.984417 9.24457 1.06793C9.32808 1.15144 9.375 1.26471 9.375 1.38281V1.875H5.625V1.38281ZM5.64844 13.125H5.625C5.50464 13.1252 5.38883 13.079 5.30156 12.9961C5.2143 12.9132 5.16227 12.7999 5.15625 12.6797L4.6875 4.24219C4.68128 4.11787 4.72471 3.99617 4.80822 3.90387C4.89173 3.81156 5.00849 3.75622 5.13281 3.75C5.25713 3.74378 5.37883 3.78721 5.47113 3.87072C5.56344 3.95423 5.61878 4.07099 5.625 4.19531L6.09375 12.6328C6.09691 12.6944 6.08788 12.756 6.0672 12.8141C6.04651 12.8721 6.01458 12.9256 5.97321 12.9713C5.93184 13.017 5.88187 13.0541 5.82614 13.0805C5.77041 13.1069 5.71002 13.122 5.64844 13.125ZM7.96875 12.6562C7.96875 12.7806 7.91936 12.8998 7.83146 12.9877C7.74355 13.0756 7.62432 13.125 7.5 13.125C7.37568 13.125 7.25645 13.0756 7.16854 12.9877C7.08064 12.8998 7.03125 12.7806 7.03125 12.6562V4.21875C7.03125 4.09443 7.08064 3.9752 7.16854 3.88729C7.25645 3.79939 7.37568 3.75 7.5 3.75C7.62432 3.75 7.74355 3.79939 7.83146 3.88729C7.91936 3.9752 7.96875 4.09443 7.96875 4.21875V12.6562ZM9.84375 12.6797C9.83773 12.7999 9.7857 12.9132 9.69844 12.9961C9.61117 13.079 9.49536 13.1252 9.375 13.125H9.35156C9.28998 13.122 9.22959 13.1069 9.17386 13.0805C9.11814 13.0541 9.06816 13.017 9.02679 12.9713C8.98542 12.9256 8.95349 12.8721 8.9328 12.8141C8.91212 12.756 8.90309 12.6944 8.90625 12.6328L9.375 4.19531C9.38122 4.07099 9.43656 3.95423 9.52887 3.87072C9.62117 3.78721 9.74287 3.74378 9.86719 3.75C9.99151 3.75622 10.1083 3.81156 10.1918 3.90387C10.2753 3.99617 10.3187 4.11787 10.3125 4.24219L9.84375 12.6797Z" fill="#FF8877"/>
                    </svg>
                </strong>
                <span>Очистить список</span></a>
            <a href="{{ url('/') }}" class="white-button">
                <strong>
                    <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0)">
                            <path d="M0.46875 7.26562L7.5 1.17188L14.5312 7.26562" stroke="#FF8877" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M12.6562 4.45312V2.34375H10.5469" stroke="#FF8877" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M2.34375 7.53916V14.5313H5.625V9.84377H9.375V14.5313H12.6562V7.51947" stroke="#FF8877" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        </g>
                        <defs>
                            <clipPath id="clip0">
                                <rect width="15" height="15" fill="white"/>
                            </clipPath>
                        </defs>
                    </svg>
                </strong>
                <span>Вернуться на главную</span></a>
        </p>

    </div>
@endsection