@extends('layout')

@section('content')
    <div class="container">
        <div class="row news">
            <div class="breadcrumbs">
                <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url"><span itemprop="title">Главная</span></a></div><div class="active"><span>Карта сайта</span></div></div>	<i class="hidden-xs hicon"></i>
            <h1 class="col-xs-12 title">Карта сайта</h1>
            <div class="content">
                <ul>
                    <li><b><a href="/">Главная</a></b></li>
                    <li><b>Категории</b>
                        <ul>

                            @foreach($categoriesMap as $category)
                                <li>
                                    <a @if($category->parent_id || $category->slug == 'akciya') href="/{{ $category->slug }}" @endif><b>{{ $category->name }}</b></a>
                                    @if($category->children)
                                        <ul>
                                            @foreach($category->children as $subcategory)
                                                <li><a href="/{{ $category->slug }}/{{ $subcategory->slug }}">{{ $subcategory->name }}</a></li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach


                    <li><b><a href="/page/stati" title="Статьи">Статьи</a></b>
                        <ul>

                            @foreach($articles as $article)
                                <li>
                                <a href="{{ '/stati/'.$article->slug }}"><b>{{ $article->name }}</b></a>
                            </li>
                            @endforeach
                        </ul>
                    </li>

                    <li><b><a href="/page/novosti" title="Новости">Новости</a></b>
                        <ul>

                            @foreach($news as $new)
                                <li>
                                    <a href="{{ '/novosti/'.$new->slug }}"><b>{{ $new->name }}</b></a>
                                </li>
                            @endforeach
                        </ul>
                    </li>

                </ul>
                <hr style="clear:both">
            </div>
        </div>
    </div>
@endsection