@extends('layout')

@section('seo')
    <title>{{ $article->title }}</title>
    <meta name="keywords" content="{{ $article->meta_keywords }}">
    <meta name="description" content="{{ $article->meta_description }}">
@endsection

@section('content')
    <div class="text-page">
        <div class="container">
            <div class="row">

                <div itemscope="" itemtype="http://schema.org/NewsArticle" class="container">
                    <meta itemscope="" itemprop="mainEntityOfPage" itemtype="https://schema.org/WebPage"
                          itemid="{{ url(Request::path()) }}">
                    <div class="row news">
                        <div class="breadcrumbs">
                            <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                                <a href="/" itemprop="url"><span itemprop="title">Главная</span></a></div>
                            <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                                <a href="/page/stati" itemprop="url"><span itemprop="title">Статьи</span></a></div>
                            <div class="active"><span>{{ $article->name }}</span></div>
                        </div>
                        @include('blocks.left-sidebar')
                        <div class="col-xs-12 col-md-8 content" style="float:right">
                            <h1 itemprop="headline" class="content-title">{{ $article->h1 }}</h1>
                            <div itemprop="author" itemscope="" itemtype="https://schema.org/Person"
                                 style="display:none;">
                                <span itemprop="name">Expressbuket</span>
                            </div>
                            <div itemprop="publisher" itemscope="" itemtype="https://schema.org/Organization">
                                <div itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject"
                                     style="display:none;">
                                    <img src="{{ asset('img/logo.png') }}">
                                    <meta itemprop="url" content="https://google.com/logo.jpg">
                                    <meta itemprop="width" content="277">
                                    <meta itemprop="height" content="50">
                                </div>
                                <meta itemprop="name" content="Expressbuket">
                            </div>
                            <meta itemprop="datePublished" content="{{ $article->created_at }}">
                            <div itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject"
                                 style="float:left;margin-right:10px;">
                                <img src="{{ asset('storage/'.$article->image) }}"
                                     alt="{{ $article->name }}">
                                <meta itemprop="url"
                                      content="{{ asset('storage/'.$article->image) }}">
                                <meta itemprop="width" content="129">
                                <meta itemprop="height" content="136">
                            </div>
                            {!! $article->text !!}
                            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                            <script src="//yastatic.net/share2/share.js"></script>
                            <div class="ya-share2 ya-share2_inited"
                                 data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,viber,whatsapp,skype,telegram">
                                <div class="ya-share2__container ya-share2__container_size_m">
                                    <ul class="ya-share2__list ya-share2__list_direction_horizontal">
                                        <li class="ya-share2__item ya-share2__item_service_vkontakte"><a
                                                    class="ya-share2__link"
                                                    href="https://vk.com/share.php?url={{ url(Request::path()) }}&amp;utm_source=share2"
                                                    rel="nofollow noopener" target="_blank" title="ВКонтакте"><span
                                                        class="ya-share2__badge"><span
                                                            class="ya-share2__icon"></span><span
                                                            class="ya-share2__counter"></span></span><span
                                                        class="ya-share2__title">ВКонтакте</span></a></li>
                                        <li class="ya-share2__item ya-share2__item_service_facebook"><a
                                                    class="ya-share2__link"
                                                    href="https://www.facebook.com/sharer.php?src=sp&amp;u={{ url(Request::path()) }}&amp;utm_source=share2"
                                                    rel="nofollow noopener" target="_blank" title="Facebook"><span
                                                        class="ya-share2__badge"><span
                                                            class="ya-share2__icon"></span><span
                                                            class="ya-share2__counter"></span></span><span
                                                        class="ya-share2__title">Facebook</span></a></li>
                                        <li class="ya-share2__item ya-share2__item_service_odnoklassniki"><a
                                                    class="ya-share2__link"
                                                    href="https://connect.ok.ru/offer?url={{ url(Request::path()) }}&amp;utm_source=share2"
                                                    rel="nofollow noopener" target="_blank" title="Одноклассники"><span
                                                        class="ya-share2__badge"><span
                                                            class="ya-share2__icon"></span><span
                                                            class="ya-share2__counter"></span></span><span
                                                        class="ya-share2__title">Одноклассники</span></a></li>
                                        <li class="ya-share2__item ya-share2__item_service_moimir"><a
                                                    class="ya-share2__link"
                                                    href="https://connect.mail.ru/share?url={{ url(Request::path()) }}&amp;utm_source=share2"
                                                    rel="nofollow noopener" target="_blank" title="Мой Мир"><span
                                                        class="ya-share2__badge"><span
                                                            class="ya-share2__icon"></span><span
                                                            class="ya-share2__counter"></span></span><span
                                                        class="ya-share2__title">Мой Мир</span></a></li>
                                        <li class="ya-share2__item ya-share2__item_service_twitter"><a
                                                    class="ya-share2__link"
                                                    href="https://twitter.com/intent/tweet?text={{ $article->name }}&amp;url={{ url(Request::path()) }}&amp;utm_source=share2"
                                                    rel="nofollow noopener" target="_blank" title="Twitter"><span
                                                        class="ya-share2__badge"><span
                                                            class="ya-share2__icon"></span></span><span
                                                        class="ya-share2__title">Twitter</span></a></li>
                                        <li class="ya-share2__item ya-share2__item_service_viber"><a
                                                    class="ya-share2__link"
                                                    href="viber://forward?text={{ url(Request::path()) }}&amp;utm_source=share2"
                                                    rel="nofollow" target="_blank" title="Viber"><span
                                                        class="ya-share2__badge"><span
                                                            class="ya-share2__icon"></span></span><span
                                                        class="ya-share2__title">Viber</span></a></li>
                                        <li class="ya-share2__item ya-share2__item_service_whatsapp"><a
                                                    class="ya-share2__link"
                                                    href="https://api.whatsapp.com/send?text={{ url(Request::path()) }}&amp;utm_source=share2"
                                                    rel="nofollow noopener" target="_blank" title="WhatsApp"><span
                                                        class="ya-share2__badge"><span
                                                            class="ya-share2__icon"></span></span><span
                                                        class="ya-share2__title">WhatsApp</span></a></li>
                                        <li class="ya-share2__item ya-share2__item_service_skype"><a
                                                    class="ya-share2__link"
                                                    href="https://web.skype.com/share?url={{ url(Request::path()) }}&amp;utm_source=share2"
                                                    rel="nofollow noopener" target="_blank" title="Skype"><span
                                                        class="ya-share2__badge"><span
                                                            class="ya-share2__icon"></span></span><span
                                                        class="ya-share2__title">Skype</span></a></li>
                                        <li class="ya-share2__item ya-share2__item_service_telegram"><a
                                                    class="ya-share2__link"
                                                    href="https://telegram.me/share/url?url={{ url(Request::path()) }}&amp;text={{ $article->name }}&amp;utm_source=share2"
                                                    rel="nofollow noopener" target="_blank" title="Telegram"><span
                                                        class="ya-share2__badge"><span
                                                            class="ya-share2__icon"></span></span><span
                                                        class="ya-share2__title">Telegram</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    @include('blocks.news')
@endsection