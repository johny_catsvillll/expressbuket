@extends('layout')
@section('content')
		<div class="container">
			<div class="breadcrumbs">
				<div class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
					<a href="/" itemprop="url"><span itemprop="title">Главная</span></a>
				</div>
				<div class="active"><span>Фотогалерея</span></div>
			</div>	
			<i class="hidden-xs hicon"></i>
			<h1 class="title">Фотогалерея</h1>
			<div class="container-fluid photos">
			  	<div role="tabpanel" class="tab-pane container active" id="delivery">
					@foreach($photos as $photo)
						<div class="col-xs-12 col-md-4 pull-left">
							<div class="photo" style="margin-top:0">
								<img src="{{ asset('storage/'. $photo->image) }}" alt="{{ $photo->title }}" title="{{ $photo->title }}">
							</div>
						</div>
					@endforeach
				</div>

			</div>
		</div>
		{{ $photos->links() }}
	@include('blocks.news')
@endsection