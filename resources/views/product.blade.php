@extends('layout')

@section('seo')
	@if(isset($product))
		<title>{{ isset($product) ? $product->name : '' }}</title>
		<meta name="keywords" content="{{ isset($product) ? $product->meta_keywords : '' }}">
		<meta name="description" content="{{ isset($product) ? $product->meta_description : '' }}">
	@endif
@endsection


@section('content')

	@if($product->price)
		<input type="hidden" name="price" value="{{ $product->price}}">
	@elseif(json_decode($product->construktor_sting))
		<input type="hidden" name="price" value="{{ getHelperPrice($product->construktor_sting)}}">
	@endif
	<input type="hidden" name="old_price" value="{{ $product->old_price }}">
	<input type="hidden" name="extra_price" value="{{ $product->extra_price }}">
	<input type="hidden" name="extra_old_price" value="{{ $product->extra_old_price }}">

		<div class="container">
			<div class="breadcrumbs">
				<div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
					<a href="/" itemprop="url"><span itemprop="title">Главная</span></a>
				</div>
				<div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
					<a href="/page/katalog" itemprop="url"><span itemprop="title">Каталог</span></a>
				</div>
				<div class="active"><span>{{ $product->name }}</span></div>
			</div>
			<div itemscope="" itemtype="http://schema.org/Product" class="container product-content">
				<div class="row" style="position:relative">
					<div itemprop="name" class="hidden-md hidden-lg content-title">{{ $product->name }}</div>
					@if($product->is_hit)
						<span class="hidden-md hidden-lg product-label product-hit" style="top:80px"><i>!</i>хит продаж</span>
					@endif

					@if($product->is_new)
						<span class="product-label product-newest"><i><img src="/img/icons/newest.png" alt="Новинка"></i>новинка</span>
					@endif

						<div class="col-xs-12 col-md-6 product-left">
							<div class="thumbnails">

								<a href="{{ asset('storage/'.$product->main_img) }}" class="fancybox active">
									<img src="{{ asset('storage/'.$product->main_img) }}"
										 alt="{{ $product->name }}" title="{{ $product->name }} - Expressbuket"></a>

								@if($product->image)
									@foreach(json_decode($product->image) as $img)
										<a href="{{ asset('storage/'.$img) }}" class="fancybox active">
											<img src="{{ asset('storage/'.$img) }}"
												 alt="{{ $product->name }}" title="{{ $product->name }} - Expressbuket"></a>
									@endforeach
								@endif

							</div>
							<div class="product-page-img">
								<img itemprop="image" src="{{ asset('storage/'. $product->main_img) }}"
									 alt="{{ $product->name }}" title="{{ $product->name }} - Expressbuket" class="img-responsive">
								<div class="size-box-in">
									@if($product->height)
									<div class="h-size" style="display:inline-block;">{{ $product->height }} см</div>
									@endif

									@if($product->width)
										<div class="w-size" style="display:inline-block;">{{ $product->width }} см</div>
									@endif
								</div>
							</div>
						</div>
					<div class="col-xs-12 col-md-6 product-right">
						<h1 class="hidden-xs hidden-sm content-title">{{ $product->name }}</h1>
						@if($product->is_hit)
							<span class="hidden-xs hidden-sm product-label product-hit"><i>!</i>хит продаж</span>
						@endif

						@if($product->is_new)
							<span class="product-label product-newest"><i><img src="/img/icons/newest.png" alt="Новинка"></i>новинка</span>
						@endif

						@if($product->extra_price)
							<div class="dib isExtra">
								<a class="variant-btn first active" data-type="standart">Стандартный<br><i>как на фото</i></a>
								<a class="variant-btn extra" data-type="extra">Экстра<br><i>+30% цветов</i></a>
							</div>
						@endif

						@if($product->is_single_item)
							<div class="dib">
								<a class="variant-btn roses active">7</a>
								<a class="variant-btn roses ">9</a>
								<a class="variant-btn roses ">11</a>
								<a class="variant-btn roses ">15</a>
								<a class="variant-btn roses ">21</a>
								<div class="clearfix"></div>
								<a class="variant-btn roses ">25</a>
								<a class="variant-btn roses ">35</a>
								<a class="variant-btn roses ">51</a>
								<a class="variant-btn roses ">75</a>
								<a class="variant-btn roses ">101</a>
							</div>
						@endif

						<input type="hidden" name="is_single_item" value="{{ $product->is_single_item ? 1 : 0 }}">

						<div class="clearfix"></div>
						<div class="js-cart-count">
						  	<div class="count">     
						     	<div class="count-control js-count-minus">-</div>
						     	<div class="count-num js-count">{{ $product->is_single_item ? 7 : 1 }}</div>
						     	<div class="count-control js-count-plus">+</div>
								@if($product->is_single_item)
									<span class="roses-text">&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;число роз в букете</span>
								@endif
						    </div>
						</div>
						<div class="clearfix"></div>

						@if($product->is_color)
							<div class="colorbox">
								<input class="color-radio" type="radio" name="color" id="red" checked value="красный">
								<label for="red" class="color-label" onclick="" data-color="#f00" data-name="красный" style="background-color: #f00">
								</label>
								<input class="color-radio" type="radio" name="color" id="pink" value="розовый">
								<label for="pink" class="color-label" onclick="" data-color="#fcc" data-name="розовый" style="background-color: #fcc"></label>
								<input class="color-radio" type="radio" name="color" id="yellow" value="желтый">
								<label for="yellow" class="color-label" onclick="" data-color="#fc0" data-name="желтый" style="background-color: #fc0"></label>
								<input class="color-radio" type="radio" name="color" id="white" value="белый">
								<label for="white" class="color-label" onclick="" data-color="#fff" data-name="белый" style="background-color: #fff"></label>
								<input class="color-radio" type="radio" name="color" id="mix">
								<label for="mix" class="color-label radio-rainbow" onclick="" data-color="url(http://neweb.tmweb.ru/img/rainbow.jpg)" data-name="разноцветный"></label>
							</div>
							<div class="colorbox-title roses-text">
								цвет - красный
							</div>
						@endif

						@if($product->price)
							<div class="one-price" style="display:none">{{ $product->old_price ? $product->old_price : $product->price }}</div>
						@elseif(json_decode($product->construktor_sting))
							<div class="one-price" style="display:none">{{ getHelperPrice($product->construktor_sting) }}</div>
						@endif
						<div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
							<meta itemprop="priceCurrency" content="RUB">
							<div class="product-price normal active">
								@if($product->old_price)
									<span class="product-oldprice line-trough"><span><b>{{ $product->old_price }}</b></span></span>
									@if($product->price)
										<b itemprop="price">{{ $product->price }}</b>
									@else
										<b itemprop="price">{{ getHelperPrice($product->construktor_sting) }}</b>
									@endif
								@elseif($product->price)
									@if($product->is_single_item)
											<b itemprop="price">{{ $product->price * 7 }}</b>
									@else
											<b itemprop="price">{{ $product->price }}</b>
									@endif

								@else
									<b itemprop="price">{{ getHelperPrice($product->construktor_sting) }}</b>
								@endif
									<span class="glyphicon glyphicon-ruble" aria-hidden="true"></span>

							</div>
							<div class="product-price extra">
						    	<b>{{ $product->old_price ? $product->old_price : $product->price }}</b> <span class="glyphicon glyphicon-ruble" aria-hidden="true"></span>
							</div>
							<link itemprop="availability" href="http://schema.org/InStock">
						</div>
					  	<div class="product-buttons">
					    	<a href="{{ url('page/korzina') }}" class="white-button catalog-buy__button" data-id="{{ $product->id }}">Заказать букет<i class="why-we-arr"></i></a>
					    	<!--a href="#" id="b64" class="red-button quick_btn">Купить в 1 клик<i class="red-button-arrow"></i></a-->
					  	</div>
					  	<div class="clearfix"></div>
					  	<div class="panel-group" id="accordion">
						    <div class="panel">
						        <div class="panel-heading">
						            <div class="panel-title">
						                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
						                <span class="product-ico1">
						                </span>Доставка</a>
						            </div>
						        </div>
						        <div id="collapseOne" class="panel-collapse collapse">
						            <div class="panel-body">
						                <p>Доставка по Волгограду и Волжскому:<br>
						                c 8:00 до 20:00 - <b>бесплатно</b><br>
						                c 20:00 до 8:00 - <b>300 <i class="fa fa-rub"></i></b><br>
						                Ближайшая доставка возможна сегодня в течение 3-х часов.</p>
						                <p class="hint">* Стоимость доставки в отдаленные районы или за пределы города уточняйте у оператора.</p>
						            </div>
						        </div>
						    </div>
						    <div class="panel">
						        <div class="panel-heading">
						            <div class="panel-title">
						                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
						                <span class="glyphicon glyphicon-ruble">
						                </span>Оплата</a>
						            </div>
						        </div>
						        <div id="collapseTwo" class="panel-collapse collapse">
						            <div class="panel-body">
						                <table class="table" style="width:99%;">
											<tbody>
												<tr>
													<td style="border-top:none">
														Карты Visa или MasterCard
													</td>
													<td style="border-top:none">
														Alfa-Click	
													</td>
												</tr>
												<tr>
													<td cellspacing="10">
														PayPal
													</td>
													<td cellspacing="10">
														Терминалы Qiwi
													</td>
												</tr>
												<tr>
													<td>
														Оплата через банк
													</td>
													<td>
														Оплата за счет баланса телефона
													</td>
												</tr>
												<tr>
													<td>
														Яндекс.Деньги
													</td>
													<td>
														WebMoney
													</td>
												</tr>
												<tr>
													<td>
														Оплата наличными в Евросети или Связном
													</td>
													<td>
														Оплата курьеру
													</td>
												</tr>
										</tbody>
									</table>
						            </div>
						        </div>
						    </div>
						    @if(count(json_decode($product->construktor_sting)) > 0)
								<div class="panel">
									<div class="panel-heading">
										<div class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
						                <span class="product-ico2">
						                </span>Состав</a>
										</div>
									</div>
									<div id="collapseThree" class="panel-collapse collapse">
										<div class="panel-body">

											@php
												$products = [];

												foreach (json_decode($product->construktor_sting) as $key => $pr) {
													$product1 = \App\Product::find($pr->itemId);
													$product1->count = $pr->count;
													array_push($products, $product1);
												}
											@endphp

											@foreach($products as $pr)
												{{ $pr->name.' - '. $pr->count }} штук <br>
											@endforeach

										</div>
									</div>
								</div>
							@else
								<div class="panel">
									<div class="panel-heading">
										<div class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
						                <span class="product-ico2">
						                </span>Состав</a>
										</div>
									</div>
									<div id="collapseThree" class="panel-collapse collapse">
										<div class="panel-body">{!! $product->composition !!}</div>
									</div>
								</div>
							@endif

						    <div class="panel">
						        <div class="panel-heading">
						            <div class="panel-title">
						                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true">
						                <span class="product-ico3">
						                </span>Описание</a>
						            </div>
						        </div>
						        <div id="collapseFour" class="panel-collapse collapse in">
						            <div itemprop="description" class="panel-body">
										<p><span style="font-family: Arial;">{!! $product->description !!}</span></div>
						        </div>
						    </div>
					  	</div>
					  	<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>		
						<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,viber,whatsapp,skype,telegram"></div>
						<div class="product-garant">
							<img src="/img/garant.png" alt="">
							<p>Получателю не понравился букет? Ничего страшного! Сообщите нам в течении 24 часов -&nbsp;мы заменим букет на новый</p>
						</div>       
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row testimonials">
					<i class="hidden-xs hicon"></i>
					<div class="title">Отзывы</div>
					<ul class="testimonials-slider">
						@foreach($reviews as $review)
							<li>
								<div class="testimonial-content">
									<div class="testimonial-header">
										<span class="testimonial-title">{{ $review->name }}</span>
										<span class="testimonial-date">{{ $review->created_at }}</span>
									</div>
									<div class="testimonial-text">
										<p>{!! $review->text !!}</p>
									</div>
								</div>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
			<div class="clearfix text-center">
				<a href="{{ url('page/otzyvy') }}" class="white-button"><span>Показать все отзывы</span><i class="why-we-arr"></i></a>
			</div>
			<div class="row">
				<i class="hicon"></i>
				<div class="title">Смотрите также</div>
				<div class="catalog">
			  		<ul class="products-slider">
						@foreach($otherProducts as $product)

			  	      	<li>
			        		<div class="product">
			          			<div class="product-inner">
			            			<span class="product-size">{{ $product->width }} {{ isset($product->height) ? 'x '. $product->height. ' см' : ''}}</span>
									@if($product->is_hit)
			            		    	<span class="product-label product-hit"><i>!</i>хит продаж</span>
									@endif

									@if($product->is_new)
										<span class="product-label product-newest"><i><img src="/img/icons/newest.png" alt="Новинка"></i>новинка</span>
									@endif

				        			<span class="product-oldprice line-trough"></span>
							        <a class="product-img-wrp" href="{{ route('product', ['product' => $product->slug]) }}"
									   title="{{ $product->name }}">
							        	<img class="product-img img-responsive" src="{{ asset('storage/'. $product->main_img) }}" alt="{{ $product->name }}" title="{{ $product->name }}">
							          	<div class="catalog-consist">
								            <div class="catalog-consist-inner" style="display: none;">
												@if($product->composition)
													{!! str_limit(strip_tags($product->composition), 190, '...') !!}
												@else

													@php
														$products1 = [];

                                                        foreach (json_decode($product->construktor_sting) as $key => $pr) {
                                                            $product1 = \App\Product::find($pr->itemId);
                                                            $product1->count = $pr->count;
                                                            array_push($products1, $product1);
                                                        }
													@endphp

													@foreach($products1 as $pr)
														{{ $pr->name.' - '. $pr->count }} штук <br>
													@endforeach

												@endif
											</div>
							          	</div>
							        </a>
							        <div class="text-center product-title ">
							        	<a href="{{ route('product', ['product' => $product->slug]) }}" title="{{ $product->name }}">{{ $product->name }}</a>
							        </div>
							        <div class="text-center product-price">
										@if($product->price)
											<b>{{ $product->price }}</b>
										@elseif(json_decode($product->construktor_sting))
											<b>{{ getHelperPrice($product->construktor_sting) }}</b>
										@endif
							        	<span class="glyphicon glyphicon-ruble" aria-hidden="true"></span>
							        </div>
			          			</div>
			        		</div>
			    		</li>
						@endforeach
			    	</ul>
			    </div>
			</div>
		</div>

	@include('blocks.news')
@endsection