@if ($paginator->hasPages())
    {{--<ul class="pagination" role="navigation">--}}
        {{-- Previous Page Link --}}
        {{--@if ($paginator->onFirstPage())--}}
            {{--<li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">--}}
                {{--<span class="page-link" aria-hidden="true">&lsaquo;</span>--}}
            {{--</li>--}}
        {{--@else--}}
            {{--<li class="page-item">--}}
                {{--<a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev"--}}
                   {{--aria-label="@lang('pagination.previous')">&lsaquo;</a>--}}
            {{--</li>--}}
        {{--@endif--}}

        {{-- Pagination Elements --}}
        {{--@foreach ($elements as $element)--}}
            {{-- "Three Dots" Separator --}}
            {{--@if (is_string($element))--}}
                {{--<li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>--}}
            {{--@endif--}}

            {{-- Array Of Links --}}
            {{--@if (is_array($element))--}}
                {{--@foreach ($element as $page => $url)--}}
                    {{--@if ($page == $paginator->currentPage())--}}
                        {{--<li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>--}}
                    {{--@else--}}
                        {{--<li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>--}}
                    {{--@endif--}}
                {{--@endforeach--}}
            {{--@endif--}}
        {{--@endforeach--}}

        {{-- Next Page Link --}}
        {{--@if ($paginator->hasMorePages())--}}
            {{--<li class="page-item">--}}
                {{--<a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next"--}}
                   {{--aria-label="@lang('pagination.next')">&rsaquo;</a>--}}
            {{--</li>--}}
        {{--@else--}}
            {{--<li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">--}}
                {{--<span class="page-link" aria-hidden="true">&rsaquo;</span>--}}
            {{--</li>--}}
        {{--@endif--}}
    {{--</ul>--}}
{{--@endif--}}

<div class="pager">
    <ul class="pagination" id="yw0">
        @if (!$paginator->onFirstPage())
            <li class="previous"><a href="{{ $paginator->previousPageUrl() == request()->url().'?page=1' ? strtok($paginator->previousPageUrl(), '?') : $paginator->previousPageUrl() }}"
                                           rel="">&lt; Предыдущая</a></li>
        @endif

        @foreach($elements as $element)

            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if($page == 1)
                            <li class="page">
                                <a href="{{ strtok($url, '?') }}" rel="">{{ $page }}</a>
                            </li>
                    @elseif ($page == $paginator->currentPage())
                        <li class="page selected">
                            <a href="{{ $url }}" rel="">{{ $page }}</a>
                        </li>
                    @else
                        <li class="page"><a href="{{ $url }}"
                                            rel="">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif

        @endforeach

        @if ($paginator->hasMorePages())
            <li class="next"><a href="{{ $paginator->nextPageUrl() }}" rel="next">Следующая &gt;</a></li>
        @endif



    </ul>
</div>

@endif