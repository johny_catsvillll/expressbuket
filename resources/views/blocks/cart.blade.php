<tbody>
    @foreach($products as $product)
        <tr data-id="{{ $product->id }}" data-color="{{ $product->color }}" data-extra="{{ $product->extra }}" class="js-cart-item item399" data-single="{{ $product->is_single_item ? 1 : 0 }}">
            <td class="item-cart-img">
                <img src="{{ asset('storage/'.$product->main_img) }}" alt="{{ $product->name }}" class="img-responsive"><br>

                @if($product->color)
                    @if($product->color == 'красный')
                        <label class="color-label" data-color="#fc0" onclick="" style="margin:0 auto;display:block;cursor:default;background: #f00 center no-repeat"></label>
                    @elseif($product->color == 'розовый')
                        <label class="color-label" data-color="#fc0" onclick="" style="margin:0 auto;display:block;cursor:default;background: #fcc center no-repeat"></label>
                    @elseif($product->color == 'желтый')
                        <label class="color-label" data-color="#fc0" onclick="" style="margin:0 auto;display:block;cursor:default;background: #fc0 center no-repeat"></label>
                    @elseif($product->color == 'белый')
                        <label class="color-label" data-color="#fc0" onclick="" style="margin:0 auto;display:block;cursor:default;background: #fff center no-repeat"></label>
                    @elseif($product->color == 'разноцветный')
                        <label class="color-label" data-color="url(http://neweb.tmweb.ru/img/rainbow.jpg)" onclick="" style="margin:0 auto;display:block;cursor:default;background: #fc0 center no-repeat"></label>
                    @endif


                @endif

            </td>
            <td class="hidden-xs item-cart-title">
                <a href="{{ route('product', ['slug' => $product->slug]) }}" title="{{ $product->name }}" class="item-cart-title-t">{{ $product->name }}</a>
                <span class="item-cart-size">
                                    {{ $product->width }} {{ isset($product->height) ? 'x '. $product->height. ' см' : ''}}
                                </span>
            </td>
            <td class="hidden-xs item-cart-delete">
                <i class="icon-cart-delete js-close delete-product"></i>
            </td>
            <td class="hidden-xs item-cart-count">
                <div class="count">
                    <div class="count-control js-count-minus">-</div>
                    <input type="number" class="count-num js-count" value="{{ $product->count }}">
                    <div class="count-control js-count-plus">+</div>
                </div>
            </td>
            <td class="item-cart-price">
                <a href="{{ route('product', ['slug' => $product->slug]) }}" title="{{ $product->name }}"
                   class="hidden-sm hidden-md hidden-lg item-cart-title-t">{{ $product->name }}</a>
                <i class="hidden-sm hidden-md hidden-lg icon-cart-delete js-close"></i>
                <div class="clearfix"></div>
                <div class="hidden-sm hidden-md hidden-lg count">
                    <div class="count-control js-count-minus">-</div>
                    <input type="number" class="count-num js-count" value="{{ $product->count }}">
                    <div class="count-control js-count-plus">+</div>
                </div>
                <div class="clearfix"></div>
                <span>
                                    <span class="js-price">{{ $product->allPrice }} <b id="extra" style="display:none"></b></span> <i class="glyphicon glyphicon-ruble"></i>
                                </span>
            </td>
        </tr>
    @endforeach
    <tr>
        <td class="hidden-xs hidden-sm"></td>
        <td colspan="4">
            <label class="col-xs-12 otkrytka">
                <input id="stoik" class="checkbox" type="checkbox" name="stoik" value="1" @if(isset($stoik) && $stoik == 'true') checked="checked" @endif>
                <span class="checkbox-custom"></span>
                <span class="label">Добавить стойкость букету <i>+ 50 р </i></span>
                <i class="tooltips hidden-xs">!<span>Мы к заказу добавим Chrysal – это подкормка для цветов. Данная добавка увеличивает жизнь в вазе на 60% и более по сравнению с обычной водой. Цветочная подкормка улучшает цветение, поддерживает качество и сохраняет цветы как можно дольше.</span></i>
            </label>
            <label class="col-xs-12 otkrytka">
                <input id="upak" class="checkbox" type="checkbox" name="upak" value="1" @if(isset($upak) && $upak == 'true') checked="checked" @endif>
                <span class="checkbox-custom"></span>
                <span class="label">Добавить красивую упаковку <i>+ 150 р</i></span>
                <i class="tooltips hidden-xs">!<span>Наши флористы добавят стильную упаковку, подходящую именно под ваш букет. Это будет элегантное дополнение к общему виду цветочной композиции.</span></i>
            </label>
            <label class="col-xs-12 otkrytka">
                <input id="green" class="checkbox" type="checkbox" name="green" value="1" @if(isset($green) && $green == 'true') checked="checked" @endif>
                <span class="checkbox-custom"></span>
                <span class="label">Добавить зелени <i>+ 200 р</i></span>
                <i class="tooltips hidden-xs">!<span>Наши флористы добавят к букету зелени для пышности и яркости букета. Данная добавка придаст букету индивидуальности и необычности. </span></i>
            </label>
        </td>
    </tr>
</tbody>