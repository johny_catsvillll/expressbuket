<div class="news">
    <div class="container">
        <div class="row">
            <i class="hidden-xs hicon"></i>
            <div class="title">Новости</div>
            @foreach($news as $new)
                <div class="media col-xs-12 col-md-4">
                    <div class="media-left">
                        <a href="{{ url('novosti/'.$new->slug) }}">
                            <img class="media-object" src="{{ asset('/storage/'.$new->image) }}" alt="{{ $new->name }}">
                        </a>
                    </div>
                    <div class="media-body">
                        <span>{{ $new->created_at->format('d.m.Y') }}</span>
                        <a href="{{ url('novosti/'.$new->slug) }}" class="media-heading">{{ $new->name }}</a>
                        {!! $new->exceprt !!}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<div class="clearfix text-center">
    <a href="{{ url('page/novosti') }}" class="white-button">Все новости <i class="why-we-arr"></i></a>
</div>