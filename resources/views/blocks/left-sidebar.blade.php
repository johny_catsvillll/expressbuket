<div class="hidden-xs hidden-sm col-md-4 left-sidebar">
    <ul class="left-sidebar-menu">
        @if(Request::is('page/novosti') || Request::is('novosti*'))
            <li><div class="active"><span>Новости</span></div></li>
        @else
            <li><a href="{{ asset('page/novosti') }}"><span>Новости</span></a></li>
        @endif

            @if(Request::is('page/dostavka'))
                <li><div class="active"><span>Доставка</span></div></li>
            @else
                <li><a href="{{ asset('page/dostavka') }}"><span>Доставка</span></a></li>
            @endif

            @if(Request::is('page/oplata'))
                <li><div class="active"><span>Оплата</span></div></li>
            @else
                <li><a href="{{ asset('page/oplata') }}"><span>Оплата</span></a></li>
            @endif

            @if(Request::is('page/stati')  || Request::is('stati*'))
                <li><div class="active"><span>Статьи</span></div></li>
            @else
                <li><a href="{{ asset('page/stati') }}"><span>Статьи</span></a></li>
            @endif

            @if(Request::is('page/otzyvy'))
                <li><div class="active"><span>Отзывы</span></div></li>
            @else
                <li><a href="{{ asset('page/otzyvy') }}"><span>Отзывы</span></a></li>
            @endif

            @if(Request::is('page/kontakty'))
                <li><div class="active"><span>Контакты</span></div></li>
            @else
                <li><a href="{{ asset('page/kontakty') }}"><span>Контакты</span></a></li>
            @endif

    </ul>
</div>