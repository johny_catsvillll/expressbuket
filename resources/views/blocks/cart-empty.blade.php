<div class="cart-empty container" style="background: rgb(255, 255, 255); border-radius: 5px; margin-top: 30px;">
    <div class="col-xs-12 alert alert-warning alert-dismissible" role="alert">
        Ваша корзина пуста. Вы можете выбрать товары в <a href="{{ url('page/katalog') }}">каталоге</a>.
    </div>
</div>