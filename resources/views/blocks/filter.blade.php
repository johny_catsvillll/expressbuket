<form id="filter-form" action="/page/katalog" method="post" class="has-validation-callback">
    <div class="hidden-md hidden-lg panel-group" id="accordion">
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title text-center">
                    <a data-toggle="collapse" class="dropdown col-xs-12" data-parent="#accordion" href="#collapseOne">
                        Показать фильтр</a>
                </div>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="col-xs-12">
                        <button class="dropdown" type="button" data-toggle="dropdown">{{ isset($priceText) && $priceText ? $priceText : 'Цена' }}</button>
                        <ul class="dropdown-menu">
                            <li><a href="{{ getLink('price', 0) }}">Все</a></li>
                            <li><a href="{{ getLink('price', 1) }}">До 1500</a></li>
                            <li><a href="{{ getLink('price', 1500) }}" >1500-3000</a></li>
                            <li><a href="{{ getLink('price', 3000) }}" >3000-5000</a></li>
                            <li><a href="{{ getLink('price', 5000) }}" >от 5000</a></li>
                            <li><a href="{{ getLink('price', 'desc') }}" >по убыванию</a></li>
                            <li><a href="{{ getLink('price', 'asc') }}" >по возрастанию</a></li>
                        </ul>
                        <input type="hidden" name="price">
                    </div>
                    <div class="col-xs-12">
                        <button class="dropdown" type="button" data-toggle="dropdown">{{ isset($flowerText) && $flowerText ? $flowerText : 'Цветы' }}</button>
                        @php
                            $flowers = \App\FilterFlower::get();
                        @endphp
                        <ul class="dropdown-menu">
                            <li><a href="{{ getLink('inc', 0) }}">Все</a></li>
                            @foreach($flowers as $flower)
                                <li><a href="{{ getLink('inc', $flower->id) }}">{{ $flower->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-xs-12">
                        <button class="dropdown" type="button" data-toggle="dropdown">{{ isset($whomText) && $whomText ? $whomText : 'Кому' }}</button>
                        @php
                            $whom = \App\FilterWhom::get();
                        @endphp
                        <ul class="dropdown-menu">
                            <li><a href="{{ getLink('whom', 0) }}">Все</a></li>
                            @foreach($whom as $who)
                                <li><a href="{{ getLink('whom', $who->id) }}">{{ $who->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-xs-12">
                        <button class="dropdown" type="button" data-toggle="dropdown">{{ isset($factText) && $factText ? $factText : 'По случаю' }}</button>
                        @php
                            $compositions = \App\FilterComposition::get();
                        @endphp
                        <ul class="dropdown-menu">
                            <li><a href="{{ getLink('fact', 0) }}">Все</a></li>
                            @foreach($compositions as $composition)
                                <li><a href="{{ getLink('fact', $composition->id) }}">{{ $composition->name }}</a></li>
                            @endforeach

                        </ul>
                    </div>
                    <div class="col-xs-12">
                        <button class="dropdown" type="button" data-toggle="dropdown">{{ isset($styleText) && $styleText ? $styleText : 'Стиль' }}</button>

                        @php
                            $styles = \App\FilterStyle::get();
                        @endphp

                        <ul class="dropdown-menu">
                            <li><a href="{{ getLink('style', 0) }}">Все</a></li>
                            @foreach($styles as $style)
                                <li><a href="{{ getLink('style', $style->id) }}">{{ $style->name }}</a></li>
                            @endforeach
                        </ul>


                    </div>
                    <div class="col-xs-12">
                        <button class="dropdown" type="button" data-toggle="dropdown">{{ isset($specText) && $specText ? $specText : 'Спецпредложения' }}</button>
                        <ul class="dropdown-menu">
                            <li><a href="{{ getLink('spec', 0) }}">Все</a></li>
                            <li><a href="{{ getLink('spec', 2) }}">Хит</a></li>
                            <li><a href="{{ getLink('spec', 1) }}">Со скидкой</a></li>
                            <li><a href="{{ getLink('spec', 3) }}">Новинка</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="hidden-xs hidden-sm row">
            <div class="col-md-4 col-lg-2">
                <button class="dropdown" type="button" data-toggle="dropdown">{{ isset($priceText) && $priceText ? $priceText : 'Цена' }}</button>
                <ul class="dropdown-menu">
                    <li><a href="{{ getLink('price', 0) }}">Все</a></li>
                    <li><a href="{{ getLink('price', 1) }}">До 1500</a></li>
                    <li><a href="{{ getLink('price', 1500) }}" >1500-3000</a></li>
                    <li><a href="{{ getLink('price', 3000) }}" >3000-5000</a></li>
                    <li><a href="{{ getLink('price', 5000) }}" >от 5000</a></li>
                    <li><a href="{{ getLink('price', 'desc') }}" >по убыванию</a></li>
                    <li><a href="{{ getLink('price', 'asc') }}" >по возрастанию</a></li>
                </ul>
            </div>
            <div class="col-md-4 col-lg-2">
                <button class="dropdown" type="button" data-toggle="dropdown">{{ isset($flowerText) && $flowerText ? $flowerText : 'Цветы' }}</button>
                <ul class="dropdown-menu">
                    <li><a href="{{ getLink('inc', 0) }}">Все</a></li>
                    @foreach($flowers as $flower)
                        <li><a href="{{ getLink('inc', $flower->id) }}">{{ $flower->name }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-4 col-lg-2">
                <button class="dropdown" type="button" data-toggle="dropdown">{{ isset($whomText) && $whomText ? $whomText : 'Кому' }}</button>
                <ul class="dropdown-menu">
                    <li><a href="{{ getLink('whom', 0) }}">Все</a></li>
                    @foreach($whom as $who)
                        <li><a href="{{ getLink('whom', $who->id) }}">{{ $who->name }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-4 col-lg-2">
                <button class="dropdown" type="button" data-toggle="dropdown">{{ isset($factText) && $factText ? $factText : 'По случаю' }}</button>
                @php
                    $compositions = \App\FilterComposition::get();
                @endphp
                <ul class="dropdown-menu">
                    <li><a href="{{ getLink('fact', 0) }}">Все</a></li>
                    @foreach($compositions as $composition)
                        <li><a href="{{ getLink('fact', $composition->id) }}">{{ $composition->name }}</a></li>
                    @endforeach

                </ul>
            </div>
            <div class="col-md-4 col-lg-2">
                <button class="dropdown" type="button" data-toggle="dropdown">{{ isset($styleText) && $styleText ? $styleText : 'Стиль' }}</button>
                @php
                    $styles = \App\FilterStyle::get();
                @endphp

                <ul class="dropdown-menu">
                    <li><a href="{{ getLink('style', 0) }}">Все</a></li>
                    @foreach($styles as $style)
                        <li><a href="{{ getLink('style', $style->id) }}">{{ $style->name }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-4 col-lg-2">
                <button class="dropdown" type="button" data-toggle="dropdown">{{ isset($specText) && $specText ? $specText : 'Спецпредложения' }}</button>
                <ul class="dropdown-menu">
                    <li><a href="{{ getLink('spec', 0) }}">Все</a></li>
                    <li><a href="{{ getLink('spec', 2) }}">Хит</a></li>
                    <li><a href="{{ getLink('spec', 1) }}">Со скидкой</a></li>
                    <li><a href="{{ getLink('spec', 3) }}">Новинка</a></li>
                </ul>
            </div>
        </div>
    </div>
</form>