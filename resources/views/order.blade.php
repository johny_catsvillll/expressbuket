@extends('layout')
@section('content')
	<div class="container stage12">
		<div class="row" style="margin:0;">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:20px;margin-top: 30px;margin-bottom:40px;">
				<div class="line-zakaz">
					<div class="col-xs-4 col-lg-4" style="position:relative;">
						<div class="line-zakaz-wrap">
							<div class="line-title hidden-xs hidden-sm">Корзина</div>
							<div class="line-circle active">1</div>
						</div>
					</div>
					<div class="col-xs-4 col-lg-4" style="position:relative;text-align:center">
						<div class="line-zakaz-wrap">
							<div class="line-title active" style="left: -35px;">Оформление</div>
							<div class="line-circle active">2</div>
						</div>
					</div>
					<div class="col-xs-4 col-lg-4" style="position:relative;">
						<div class="line-zakaz-wrap" style="float:right;">
							<div class="line-title hidden-xs hidden-sm" style="left: -5px;">Оплата</div>
							<div class="line-circle">3</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<span class="order-title first">Ваш заказ:</span>
				<z class="order-text">В <span id="region">{{ request()->zonename }}</span> на сумму
					<b class="order-price">{{ request()->sum <= 500 ? request()->sum + 300 :request()->sum }}</b>
							@if(request()->sum > 500)
								<span class="wp-text">
									<i style="color:rgba(159, 2, 38, 1);" class="glyphicon glyphicon-ruble"></i> включая бесплатную доставку
								</span>
							@else
								<span class="wp-text">
									<i style="color:rgba(159, 2, 38, 1);" class="glyphicon glyphicon-ruble"></i> включая платную доставку
								</span>
							@endif

			</div>
		</div>
		<div class="row order-table">
			@foreach($products as $product)
				<table class="col-xs-12 col-sm-6 col-md-4">
					<tbody>
					<tr>
						<td class="item-cart-img">
							<img src="{{ asset('storage/'.$product->main_img) }}" alt="{{ $product->name }}" class="img-responsive">
						</td>
						<td class="item-cart-title">
							<span class="item-cart-title-t">{{ $product->name }}</span>
							<span class="item-cart-size">
										{{ $product->allPrice }} <i class="glyphicon glyphicon-ruble"></i>
									</span>
						</td>
					</tr>
					</tbody>
				</table>
			@endforeach
			<table class="col-xs-12">
				<tbody>
				<input id="sum" type="hidden" name="sum" value="{{ request()->sum }}">
				<input  type="hidden" name="standartPrice" value="{{ request()->standartPrice }}">
				<input id="oldsum" type="hidden" name="sum" value="{{ request()->sum }}">
				<input id="zone_price" type="hidden" name="zone_price" value="0">
				<input id="sumcart" type="hidden" name="sumcart" value="{{ request()->sum }}">
				<input type="hidden" name="promo" value="{{ request()->promocode }}">
				@if(request()->stoik)
					<tr>
						<td>
							<label class="col-xs-12 otkrytka">
								<span class="label">Добавить стойкость букету <i>+ 50 р</i></span>
								<i class="tooltips hidden-xs">!<span>Мы к заказу добавим Chrysal – это подкормка для цветов. Данная добавка увеличивает жизнь в вазе на 60% и более по сравнению с обычной водой. Цветочная подкормка улучшает цветение, поддерживает качество и сохраняет цветы как можно дольше.</span></i>
							</label>
						</td>
					</tr>
				@endif

				@if(request()->upak)
					<tr>
						<td>
							<label class="col-xs-12 otkrytka">
								<span class="label">Добавить красивую упаковку <i>+ 150 р</i></span>
								<i class="tooltips hidden-xs">!<span>Наши флористы добавят стильную упаковку, подходящую именно под ваш букет. Это будет элегантное дополнение к общему виду цветочной композиции.</span></i>
							</label>
						</td>
					</tr>
				@endif


				@if(request()->green)
					<tr>
						<td>
							<label class="col-xs-12 otkrytka">
								<span class="label">Добавить зелени <i>+ 200 р</i></span>
								<i class="tooltips hidden-xs">!<span>Наши флористы добавят к букету зелени для пышности и яркости букета. Данная добавка придаст букету индивидуальности и необычности. </span></i>
							</label>
						</td>
					</tr>
				@endif



				</tbody>
			</table>
		</div>
		<form id="pay-form" action="/order2" method="post" class="has-validation-callback">
			{{ csrf_field() }}
			<div class="col-xs-12 col-md-12 cart-checkboxes">
				<label class="col-xs-12 col-sm-3 otkrytka">
					<input id="checkotkr" class="checkbox" type="checkbox" @if(request()->otkrytka) checked="checked" @endif  name="otkrytka" value="1">
					<span class="checkbox-custom"></span>
					<span class="label">Добавить открытку <i>бесплатно</i></span>
				</label>
				<label class="col-xs-12 col-sm-3 otkrytka">
					<input class="checkbox" type="checkbox" @if(request()->photo) checked="checked" @endif name="photo" value="1">
					<span class="checkbox-custom"></span>
					<span class="label">Сделать фото при получении</span>
				</label>

				@if(auth('account')->check())
					<label class="col-xs-12 col-sm-4 otkrytka">
						<input class="checkbox" type="checkbox" name="isBonus" value="1">
						<span class="checkbox-custom"></span>
						<span class="label">Списать бонусы при расчете суммы: {{ auth('account')->user()->balance ? auth('account')->user()->balance->sum('price') : 0 }} руб </span>
					</label>
				@endif


			</div>
			<div class="col-xs-12 col-md-6 textotkr" @if(!request()->otkrytkatext) style="display:none;" @endif>
				<label for="" class="label-textarea">Текст поздравления</label>
				<textarea name="otkrytkatext" id="" class="otkrytka-text">{{ request()->otkrytkatext }}</textarea>
			</div>
			@if(request()->stoik)
				<input type="hidden" name="stoik" value="1">
			@endif

			@if(request()->upak)
				<input type="hidden" name="upak" value="1">
			@endif

			@if(request()->green)
				<input type="hidden" name="green" value="1">
			@endif



			<input type="hidden" name="zone" value="Волгоград">
			<div class="row order-form">
				<span class="col-xs-12 order-title">Отправитель</span>
				<div class="col-xs-12 col-md-6 col-lg-4">
					<input type="text" data-validation="required" data-validation-error-msg="Обязательное поле" name="sender_phone"
						   @if(auth('account')->check()) value="{{ auth('account')->user()->phone }}" @endif placeholder="Ваш телефон" class="order-input phone">
				</div>
				<div class="col-xs-12 col-md-6 col-lg-4">
					<input type="text" name="sender_mail" @if(auth('account')->check()) value="{{ auth('account')->user()->email }}" @endif placeholder="Ваш e-mail" class="order-input mail">
				</div>
				<div class="col-xs-12 col-md-6 col-lg-4">
					<input type="text" data-validation="required" data-validation-error-msg="Обязательное поле" @if(auth('account')->check()) value="{{ auth('account')->user()->name }}" @endif name="sender_name" placeholder="Вашe имя" class="order-input fio">
				</div>
				<span class="col-xs-12 order-title">Получатель</span>
				<div class="col-xs-12 col-md-6 col-lg-4">
					<input type="text" data-validation="required" data-validation-error-msg="Обязательное поле" name="receiver_name" placeholder="Фамилия Имя Отчество" class="order-input fio">
				</div>
				<div class="col-xs-12 col-md-6 col-lg-4">
					<input type="text" data-validation="required" data-validation-error-msg="Обязательное поле" name="receiver_phone" placeholder="Телефон получателя" class="order-input phone">
				</div>
				<div class="col-xs-12 col-md-6 col-lg-4">
					<select class="order-input address" name="target_time">
						<option value="Позвонить перед доставкой уточнить время и адрес" >Уточнить адрес и время доставки</option>
						<option value="Нужно сделать сюрприз">Не звонить получателю перед доставкой</option>
					</select>
				</div>
				<div class="col-xs-12 hint address-hint">Мы позвоним получателю и уточним удобно ли принять заказ в указанное Вами время. При звонке мы не скажем про цветы.</div>
				<span class="col-xs-12 col-md-6 order-title">Дата и время доставки</span>
				<span class="col-xs-12 col-md-6 order-title time-title">
				      Доставка с <span class="delivery-from">09:00</span> до <span class="delivery-to">20:00</span>
				       - <span class="delivery-price" data-price="0" data-price-2="300">{{ request()->sum <= 500 ? 300 : 0 }}</span> рублей
				    </span>
				<span class="col-xs-12 col-md-6 order-title time-title2" style="display:none;">
			      		Время доставки
			    	</span>
				<div class="col-xs-12 col-md-6">
					<input id="picker" data-validation="required" data-validation-error-msg="Обязательное поле" type="text" name="date" placeholder="Дата доставки" class="order-input date" readonly="true">
					<div class="hint upsum" style="display:none">
						Стоимость букета в праздничные дни увеличилась из-за повышения цен поставщиков. Цена заказа с доставкой
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="date-slider">
						<input id="zone_price" type="hidden" name="zone_price" value="0">
						<input id="range" type="text" name="time" placeholder="Время доставки" class="order-input phone"
							   data-type="double"  data-grid="true" data-values="06:00,06:30,07:00,07:30,
					        08:00,08:30,09:00,09:30,10:00,10:30,11:00,11:30,12:00,12:30,13:00,13:30,14:00,14:30,15:00,
					        15:30,16:00,16:30,17:00,17:30,18:00,18:30,19:00,19:30,20:00,20:30,21:00,21:30,22:00,22:30,23:00,23:30,00:00"\
							   data-from="6" data-to="28" data-min-interval="4">
						<div class="text-center">
							<div class="timerange col-xs-12 col-md-4 double" data-from="0" data-to="6">
								<span class="time-selector-item-time">Утро</span>
								<span class="time-selector-separator">&nbsp;</span>
								<span class="time-selector-item-time time-selector-item-time2">06:00 - 09:00</span>
							</div>
							<div class="timerange col-xs-12 col-md-4 active" data-from="6" data-to="28">
								<span class="time-selector-item-time">День</span>
								<span class="time-selector-separator">&nbsp;</span>
								<span class="time-selector-item-time time-selector-item-time2">09:00 - 20:00</span>
							</div>
							<div class="timerange col-xs-12 col-md-4 double" data-from="28" data-to="36">
								<span class="time-selector-item-time">Вечер</span>
								<span class="time-selector-separator">&nbsp;</span>
								<span class="time-selector-item-time time-selector-item-time2">20:00 - 00:00</span>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="hint date-hint">В период утро и вечер стоимость доставки - 300 рублей</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<span class="col-xs-12 order-title">Адрес доставки</span>
					<textarea id="address" placeholder="Волгоград," class="textarea" name="address"
							  data-validation="required" data-validation-error-msg="Обязательное поле"></textarea>
					<label id="question-address" class="otkrytka" style="margin-top:0">
						<input  class="checkbox" type="checkbox" name="question_address" value="1">
						<span class="checkbox-custom"></span>
						<span class="label">Узнать адрес доставки у получателя</span>
					</label>
				</div>
				<div class="col-xs-12 col-md-6">
					<span class="col-xs-12 order-title">Комментарий (дополнительные пожелания):</span>
					<textarea placeholder="" class="textarea" name="comment"></textarea>
					<label id="call_before_order" class="otkrytka" style="margin-top:0">
						<input  class="checkbox" type="checkbox" name="call_before_order" value="1">
						<span class="checkbox-custom"></span>
						<span class="label">Хочу, чтобы мне позвонили для подтверждения заказа</span>
					</label>
				</div>
				<div class="col-xs-12 col-md-6"></div>
			</div>
			<input type="hidden" name="promocode" value="{{ request()->promocode }}">
			<div class="row text-center cart-final">
				<a href="{{ url('page/korzina') }}" title="" class="back-to-catalog hidden-xs">Вернуться к корзине</a>
				<button type="submit" name="" class="white-button final-order">Перейти к оплате <i class="why-we-arr"></i></button>
				<p class="text-center">
					<input type="checkbox" name="accepted" required="" checked="checked">&nbsp;Согласен с обработкой персональных данных
				</p>
				<p class="text-center oferta">Оформляя заказ, я подтверждаю, что ознакомлен<br> с <a style="text-decoration:underline;" target="_blank" href="{{ url('page/politika-konfidentsialnosti') }}">положением об обработке и защите персональных данных</a>
				</p>
			</div>
		</form>
		<div class="row hidden-sm hidden-md hidden-lg text-center"><a href="{{ url('page/korzina') }}" title="" class="back-to-catalog hidden-xs">Вернуться к корзине</a></div>
	</div>
	@include('blocks.news')
@endsection