<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="/favicon.png" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" property="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" property="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" property="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" property="stylesheet" href="{{ asset('css/select.css') }}">
    <link rel="stylesheet" property="stylesheet" href="{{ asset('css/lightslider.css') }}">
    <link rel="stylesheet" property="stylesheet" href="{{ asset('css/ion.rangeSlider.css') }}">
    <link rel="stylesheet" property="stylesheet" href="{{ asset('css/ion.rangeSlider.skinFlat.css') }}">
    <link rel="stylesheet" property="stylesheet" href="{{ asset('css/jquery.jgrowl.min.css') }}">
    <link rel="stylesheet" property="stylesheet" href="{{ asset('css/opentip.css') }}">
    <link rel="stylesheet" property="stylesheet" href="{{ asset('css/ui.totop.css') }}">
    <link rel="stylesheet" property="stylesheet" href="{{ asset('css/new.css') }}">
    <link rel="stylesheet" property="stylesheet" href="{{ asset('css/media.css') }}">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @section('seo')

        <title>{{ isset($seo) && $seo ? $seo->title : '' }}</title>
        <meta name="keywords" content="{{ isset($seo) && $seo ? $seo->meta_keywords : '' }}">
        <meta name="description" content="{{ isset($seo) && $seo ? $seo->meta_description : '' }}">
    @endsection

    @yield('seo')
</head>
<body>
<div class="navbar-fixed hidden-xs">
    <div class="container" itemscope itemtype="https://schema.org/Organization">
        <span itemprop="name" class="hidden-xs hidden-sm hidden-md hidden-lg">EXPRESS БУКЕТ</span>
        <a href="https://expressbuket.com/" rel="nofollow" itemprop="url" class="hidden-xs hidden-sm hidden-md hidden-lg"></a>
        <span itemprop="address" class="hidden-xs hidden-sm hidden-md hidden-lg">г. Волгоград, пр-т Ленина 89</span>
        <div class="row">
            <div class="col-xs-10 col-sm-12 col-md-12 col-lg-3 logo">
                <a href="/" title="Expressbuket - Доставка цветов в Ростове.">
                    <img itemprop="logo" class="img-responsive" src="{{ asset('img/mobile-logo.png') }}" alt="Expressbuket">
                </a>
            </div>
            <div class="hidden-xs hidden-sm col-sm-5 col-md-4 col-lg-3 text-center bordered timework-block">
                <div class="callback">
                    <span class="text-center">Бесплатно для звонков по РФ</span><br>
                    <a itemprop="telephone" href="tel:{{ setting('.phone_call_rf') }}" title="бесплатно для звонков по РФ">{{ setting('.phone_call_rf') }}</a>
                </div>
            </div>
            <div class="hidden-xs col-sm-5 col-md-3 col-lg-3 text-center bordered">
                <div class="callback">
                    <span>Главный офис в {{ get_subdomain_city() }}</span><br>
                    <button class="btn-phones dropdown" type="button" id="dropdownMenu4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <a href="tel:{{ $phones->first()->phone }}" title="бесплатно для звонков по РФ">{{ $phones->first()->phone }}</a>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu menu-phones" aria-labelledby="dropdownMenu4">
                        @foreach($phones as $phone)
                            @if($loop->first)
                                @continue
                            @endif
                            <li>
                                <span>Пишите.Звоните.</span>
                                <a href="tel:{{ $phone }}" title="">{{ $phone }}</a>
                            </li>
                        @endforeach
                        <li>
                            <a href="#" data-toggle="modal" data-target="#callbackPopup">заказать звонок</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-6 col-sm-5 col-md-2 col-lg-3 cart-top">
                <div class="row">
                    <div class="col-lg-8 col-sm-8">
                        <a href="{{ url('page/favorite') }}" class="link-favorite">
                            <span>
                                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M23.5 2.5C20.25 2.5 17.05 4.6 16 7.5C14.95 4.6 11.75 2.5 8.5 2.5C6.51088 2.5 4.60322 3.29018 3.1967 4.6967C1.79018 6.10322 1 8.01088 1 10C1 16.5 6.5 23 16 29.5C25.5 23 31 16.5 31 10C31 8.01088 30.2098 6.10322 28.8033 4.6967C27.3968 3.29018 25.4891 2.5 23.5 2.5V2.5Z" stroke="#FF8877" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                                <strong>@if(session('favorite')) {{ count(session('favorite')) }} @endif</strong>
                            </span>
                            Избранное
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <a href="{{ url('page/korzina') }}" class="cart-top-icon">
                            <span class="badge @if(!session('basket-products')) hide @endif">{{ getCountBasket() }}</span>
                        </a>
                        <div class="wrap-price @if(!session('basket-products')) hide @endif">
                            <span class="hidden-xs hidden-sm hidden-md cart-top-price">{{ getPriceBasket() }}</span>
                            <span class="hidden-xs hidden-sm hidden-md">руб</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="top-navbar hidden-xs">
    <div class="container">
        <div class="row">
            <ul class="dib">
                @foreach($pages->where('is_show_top', 1) as $page)
                    <li><a href="{{ url('page/'.$page->slug) }}">{{ $page->name }}</a></li>
                @endforeach
            </ul>
            <div class="messangers pull-right">
                <a href="#whatsapp" title="Мы в Whatsapp" rel="nofollow" data-toggle="modal" data-target="#whatsapp-modal">
                    <img width="25" src="{{ asset('img/whats.png') }}" alt="">
                </a>
                <a href="#whatsapp" title="Мы в Viber" rel="nofollow" data-toggle="modal" data-target="#whatsapp-modal">
                    <img width="25" src="{{ asset('img/viber.png') }}" alt="">
                </a>
                <a href="https://telegram.me/expressbuket" title="Мы в Telegram" rel="nofollow" target="_blank">
                    <img width="25" src="{{ asset('img/telegram.png') }}" alt="">
                </a>
                <span class="mess-manager" data-toggle="modal" data-target="#whatsapp-modal">Персональный менеджер</span>
            </div>
            <div id="search" class="pull-right">
                <form action="/search" method="post" class="has-validation-callback">
                    {{ csrf_field() }}
                    <input class="order-input" name="search" id="SiteSearchForm_string1" type="text">
                    <button type="submit" class="search-btn">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="container">
    <div class="row header">
        <div class="col-xs-10 col-sm-12 col-md-12 col-lg-3 logo">
            <a href="/" title="Expressbuket - Доставка цветов в Волгограде и Волжском.">
                <img class="hidden-xs img-responsive" src="{{ asset('img/mobile-logo.png') }}" alt="Expressbuket">
                <img class="hidden-sm hidden-md hidden-lg img-responsive" src="{{ asset('img/mobile-logo.png') }}" alt="Expressbuket">
                <div class="hidden-xs hidden-sm hidden-md hidden-lg text-left logo-text">
                    <span>Доставка цветов</span>
                    по Волгограду и Волжскому
                </div>
            </a>
        </div>
        <div class="hidden-xs col-sm-3 col-md-4 col-lg-2 text-center delivery-block">
            <div class="callback text-left">
                <span><b>Куда доставить букет?</b></span><br>
                <button class="btn-phones dropdown select-city-btn" type="button" id="dropdownMenu4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <a href="#" title="{{ get_subdomain_city() }}" data-toggle="modal" data-target="#city-modal">{{ get_subdomain_city() }}</a>
                </button>
            </div>
        </div>
        <div class="select-city modal fade" id="city-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="new-modal">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <img src="{{ asset('img/delivery.jpg') }}" alt="" class="new-modal__img img-responsive">
                    <span class="new-modal__title"><img src="{{ asset('img/icons/maps-and-flags.svg') }}" alt="" class="new-modal__ico">Куда доставить букет?</span>
                    <span class="new-modal__subtitle">
			          			Доставляем цветы по Ростову-на-Дону, Волгограду и ближайшим городам.
			          			Ассортимент и цена зависят от города.
			          		</span>
                    <div class="new-modal__content row">
                        <div class="col-xs-12 col-md-6 new-modal__item">
                            <img src="{{ asset('img/vlg.png') }}" alt="" class="float-left-desktop new-modal__item-img">
                            <div class="float-left new-modal__info float-right">
                                <span class="new-modal__city">Волгоград<br>(и область)</span>
                                <a href="//{{ env('APP_URL2') }}" class="new-modal__btn">Выбрать</a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 new-modal__item">
                            <div class="float-left new-modal__info">
                                <span class="new-modal__city">Ростов-на-Дону<br>(и область)</span>
                                <a href="//rostov-na-donu.{{  env('APP_URL2') }}" class="new-modal__btn">Выбрать</a>
                            </div>
                            <img src="{{ asset('img/rnd.png') }}" alt="" class="float-right-desktop new-modal__item-img">
                        </div>
                    </div>
                    <div class="modal-footer new-modal__footer">
                        <p>*Стоимость доставки в отдаленные районы или за пределы города уточняйте у оператора по телефону 8 800 500 79-94 </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="hidden-xs hidden-sm col-sm-5 col-md-4 col-lg-2 text-center bordered timework-block">
            <div class="callback">
                <span class="text-center">Бесплатно для звонков по РФ</span><br>
                <a href="tel:{{ setting('.phone_call_rf') }}" title="бесплатно для звонков по РФ">{{ setting('.phone_call_rf') }}</a>
            </div>
        </div>
        <div class="hidden-xs col-sm-4 col-md-3 col-lg-2 text-center bordered">
            <div class="callback">
                <span>Главный офис в {{ get_subdomain_city() }}</span><br>
                <button class="btn-phones dropdown" type="button" id="dropdownMenu4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <a href="tel:{{ $phones->first()->phone }}" title="бесплатно для звонков по РФ">{{ $phones->first()->phone }}</a>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu menu-phones" aria-labelledby="dropdownMenu4">
                    @foreach($phones as $phone)
                        @if($loop->first)
                            @continue
                        @endif

                        <li>
                            <span>Пишите.Звоните.</span>
                            <a href="tel:{{ $phone }}" title="">{{ $phone }}</a>
                        </li>
                    @endforeach
                    <li>
                        <a href="#" title="" data-target="#callbackPopup" data-toggle="modal" class="header__callback __element_sl btn __cmd_order">заказать звонок</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-2 col-lg-3 cart-top">
            <div class="row">
                <div class="col-lg-8 col-xs-6 col-sm-8">
                    <a href="{{ url('page/favorite') }}" class="link-favorite">
                            <span>
                                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M23.5 2.5C20.25 2.5 17.05 4.6 16 7.5C14.95 4.6 11.75 2.5 8.5 2.5C6.51088 2.5 4.60322 3.29018 3.1967 4.6967C1.79018 6.10322 1 8.01088 1 10C1 16.5 6.5 23 16 29.5C25.5 23 31 16.5 31 10C31 8.01088 30.2098 6.10322 28.8033 4.6967C27.3968 3.29018 25.4891 2.5 23.5 2.5V2.5Z" stroke="#FF8877" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                                <strong>@if(session('favorite')) {{ count(session('favorite')) }} @endif</strong>
                            </span>
                        Избранное
                    </a>
                </div>
                <div class="col-lg-4 col-xs-6 col-sm-4">
                    <a href="{{ url('page/korzina') }}" class="cart-top-icon">
                        <span class="badge @if(!session('basket-products')) hide @endif">{{ getCountBasket() }}</span>
                    </a>
                    <div class="wrap-price @if(!session('basket-products')) hide @endif">
                        <span class="hidden-xs hidden-sm hidden-md cart-top-price">{{ getPriceBasket() }}</span>
                        <span class="hidden-xs hidden-sm hidden-md">руб</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid categories-menu">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button class="hidden-md hidden-lg header-phone btn-phones"
                        type="button" id="dropdownMenu5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">{{ setting('.phone_call_rf') }}<span class="caret"></span> </button>
                <ul class="dropdown-menu menu-phones" aria-labelledby="dropdownMenu5">
                    <li>
                        <span>Бесплатно для РФ</span>
                        <a href="tel:{{ setting('.phone_call_rf') }}">{{ setting('.phone_call_rf') }}</a>
                    </li>
                    <li>
                        <span>Для звонков из {{ get_subdomain_city() }}</span>
                        <a href="tel:{{ $phones->first()->phone }}">{{ $phones->first()->phone }}</a>
                    </li>
                </ul>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button><br>
                <a title="" class="hidden-md hidden-lg header-callback __cmd_order" data-toggle="modal" data-target="#callbackPopup">заказать звонок</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    @foreach($categories as $category)
                        @if(count($category->children) > 0)
                            <li class="dropdown dropdown-submenu">
                                <span class="dropdown-toggle">{{ $category->name }}<span class="caret"></span></span>
                                <ul class="dropdown-menu">
                                    @foreach($category->children as $subcategory)
                                        @if(count($subcategory->children) > 0)
                                            <li class="dropdown dropdown-submenu">
                                                <a href="{{ url($category->slug.'/'.$subcategory->slug) }}" class="dropdown-toggle">{{ $subcategory->name }}<span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    @foreach($subcategory->children as $sub)
                                                        <li>
                                                            <a href="{{ url($category->slug.'/'.$subcategory->slug.'/'.$sub->slug) }}" title="{{ $sub->name }}">{{ $sub->name }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @else
                                            <li><a href="{{ url($category->slug.'/'.$subcategory->slug) }}" title="{{ $subcategory->name }}">{{ $subcategory->name }}</a></li>
                                        @endif

                                    @endforeach
                                </ul>
                            </li>
                        @else
                            <li>
                                <a title="{{ $category->name }}" href="{{ url($category->slug) }}">{{ $category->name }}</a>
                            </li>
                        @endif
                    @endforeach

                    <li>
                        @if(auth('account')->user())
                            <a href="/account" class="user-active">Личный кабинет</a>
                        @else
                            <a href="#" data-toggle="modal" data-target="#auth">Авторизация</a>
                        @endif
                    </li>
                    <li>
                        @if(auth('account')->check())
                            <a href="#" data-toggle="modal" data-target="#status">Статус заказа</a>
                        @endif
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<div class="hidden-md hidden-lg col-sm-5 col-md-4 col-lg-2 text-center delivery-block">
    <div class="callback text-left">
        <span><b>Куда доставить букет?</b></span>
        <button class="btn-phones dropdown select-city-btn" type="button" id="dropdownMenu4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <a href="#" title="Волгоград" data-toggle="modal" data-target="#city-modal">Волгоград</a>
        </button>
    </div>
</div>
<div class="hidden-md hidden-lg">
    <div id="search" class="pull-right">
        <form action="/search" method="post" class="has-validation-callback">
            {{ csrf_field() }}
            <input class="order-input" name="search" id="SiteSearchForm_string" type="text">
            <button type="submit" class="search-btn">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </form>
    </div>
</div>
@yield('content')
<div class="hidden-xs shops">
    <div class="container">
        <div class="row">
            <i class="hicon hicon-white"></i>
            <div class="title">Наши магазины</div>
            @foreach($magazines as $magazine)
                <div class="col-sm-5 col-lg-3 shop">
                    <span class="shop-geo">{{ $magazine->cityId->name }}, {{ $magazine->address }}</span>
                    <span class="shop-phone"></span>
                </div>
            @endforeach
        </div>
    </div>
    <p class="text-center">
        <a href="{{ url('page/kontakty') }}" class="all-objects-button">Смотреть все контакты<i class="all-objects-arr"></i></a>
    </p>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="hidden-xs col-xs-12 col-md-3 copyright">
                <a href="/" title="Expressbuket - Доставка цветов в Волгограде и Волжском.">
                    <img class="hidden-xs img-responsive" src="{{ asset('img/mobile-logo.png') }}" alt="Expressbuket">
                    <img class="hidden-sm hidden-md hidden-lg img-responsive" src="{{ asset('img/mobile-logo.png') }}" alt="Expressbuket">
                    <div class="hidden-xs hidden-sm hidden-md hidden-lg text-left logo-text">
                        <span>Доставка цветов</span>
                        по Волгограду и Волжскому
                    </div>
                </a>
                <div class="icons">
                    <i class="icons-visa" title="Банковские карты Visa и MasterCard"></i>
                    <i class="icons-yad" title="Яндекс.Деньги"></i>
                    <i class="icons-wm" title="WebMoney (WMR)"></i>
                    <i class="icons-nal" title="Наличными в терминале"></i>
                    <i class="icons-ac" title="Альфа-Клик"></i>
                    <i class="icons-sb" title="Сбербанк: оплата по SMS или Сбербанк Онлайн"></i>
                    <i class="icons-qiwi" title="QIWI-кошелек"></i>
                </div>
                <span>Copyright © 2014 - {{ date('Y') }} expressbuket.com – интернет-магазин
				        доставки букетов.</span>
            </div>
            <div class="col-xs-6 col-md-2 menu">
                <span>Компания</span>
                <ul>
                    @foreach($pages->where('is_show_footer', 1) as $page)
                        <li><a href="{{ url('page/'.$page->slug) }}">{{ $page->name }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-xs-6 col-md-2 hidden-xs hidden-sm menu">
                <span>Информация</span>
                <ul>
                    @foreach($pages->where('is_info_block', 1) as $page)
                        <li><a href="{{ url('page/'.$page->slug) }}">{{ $page->name }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-xs-6 col-md-2 menu">
                <span>Каталог</span>
                <ul>
                    @foreach($categories as $category)
                        @if(count($category->children) > 0)
                            <li><a href="{{ url($category->slug) }}">{{ $category->name }}</a></li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <div class="col-xs-12 col-md-3 contacts">
                <div class="callback">
                    <button class="btn-phones" type="button" id="dropdownMenu6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">{{ setting('.phone_call_rf') }}<span class="caret"></span> </button>
                    <ul class="dropdown-menu menu-phones" aria-labelledby="dropdownMenu6">
                        <li>
                            <span>Бесплатно для РФ</span>
                            <a href="tel:{{ setting('.phone_call_rf') }}">{{ setting('.phone_call_rf') }}</a>
                        </li>
                        <li>
                            <span>Для звонков из {{ get_subdomain_city() }}</span>
                            <a href="tel:{{ $phones->first()->phone }}">{{ $phones->first()->phone }}</a>
                        </li>
                    </ul>
                </div>
                <a href="#" class="callback-btn header__callback __element_sl btn __cmd_order">заказать звонок</a>
                <div class="messangers">
                    <a href="#whatsapp" title="Мы в Whatsapp" rel="nofollow" data-toggle="modal" data-target="#whatsapp-modal">
                        <img width="25" src="{{ asset('img/whats.png') }}" alt="">
                    </a>
                    <a href="#whatsapp" title="Мы в Viber" rel="nofollow" data-toggle="modal" data-target="#whatsapp-modal">
                        <img width="25" src="{{ asset('img/viber.png') }}" alt="">
                    </a>
                    <a href="https://telegram.me/expressbuket" title="Мы в Telegram" rel="nofollow" target="_blank">
                        <img width="25" src="{{ asset('img/telegram.png') }}" alt="">
                    </a>
                </div>
                <div class="clearfix"></div>
                <i class="soc-line"></i>
                <span class="titlesoc">Мы в социальных сетях</span>
                <a href="{{ setting('.vk') }}" rel="nofollow" class="soc vk"></a>
                <a href="{{ setting('.fb') }}" rel="nofollow" class="soc fb"></a>
                <a href="{{ setting('.instagram') }}" rel="nofollow" class="soc ig"></a>
            </div>
            <div class="hidden-sm hidden-md hidden-lg col-xs-12 col-md-4 copyright">
				        <span>
				        Copyright © 2014 - {{ date('Y') }} expressbuket.com – интернет-магазин
				        доставки букетов.
				        </span>
                <div class="icons">
                    <i class="icons-visa" title="Банковские карты Visa и MasterCard"></i>
                    <i class="icons-yad" title="Яндекс.Деньги"></i>
                    <i class="icons-wm" title="WebMoney (WMR)"></i>
                    <i class="icons-nal" title="Наличными в терминале"></i>
                    <i class="icons-ac" title="Альфа-Клик"></i>
                    <i class="icons-sb" title="Сбербанк: оплата по SMS или Сбербанк Онлайн"></i>
                    <i class="icons-qiwi" title="QIWI-кошелек"></i>
                </div>
                <a href="/" title="Expressbuket - Доставка цветов в Волгограде и Волжском.">
                    <img class="hidden-xs img-responsive" src="{{ asset('img/mobile-logo.png') }}" alt="Expressbuket">
                    <img class="hidden-sm hidden-md hidden-lg img-responsive" src="{{ asset('img/mobile-logo.png') }}" alt="Expressbuket">
                    <div class="hidden-xs hidden-sm hidden-md hidden-lg text-left logo-text">
                        <span>Доставка цветов</span>
                        по Волгограду и Волжскому
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="modal fade" id="whatsapp-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center">Ваш персональный менеджер</h4>
                </div>
                <div class="modal-body">
                    <div class="modal-contacts">
                        <p class="text-center">
                            <img width="35" src="{{ asset('img/whats.png') }}" alt="">
                            <img width="35" src="{{ asset('img/viber.png') }}" alt="">
                            <img width="35" src="{{ asset('img/telegram.png') }}" alt="">
                        </p>
                        <p class="whatsapp-phone"><a href="tel:{{ setting('.messanger') }}" title="">{{ setting('.messanger') }}</a></p>
                    </div>
                    <p>Добавьте наш номер телефона в любой удобный для вас мессенджер и наш менеджер:</p>
                    <hr>
                    <ul class="manager-list">
                        <li>поможет оформить заказ</li>
                        <li>проконсультирует по любому возникшему вопросу</li>
                        <li>даст рекомендации</li>
                        <li>сообщит о статусе заказа</li>
                    </ul>
                </div>
                <div class="modal-footer">
                    Телефон для связи с менеджером - {{ setting('.messanger') }}
                </div>
            </div>
        </div>
    </div>
    <div class="select-city modal fade" id="delivery-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="new-modal free-delivery">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <img src="{{ asset('img/free-delivery.jpg') }}" alt="" class="img-responsive">
            </div>
        </div>
    </div>
</footer>
<a href="#" id="toTop"><span id="toTopHover"></span>To Top</a>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
{{--<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>--}}
<script type="text/javascript" src="{{ asset('js/lib/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lib/jquery.session.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lib/jquery.maskedinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lib/lightslider.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lib/jquery.ui.totop.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lib/copyright.min.js') }}"></script>


<script type="text/javascript" src="{{ asset('js/lib/jquery.fancybox.pack.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lib/ion.rangeSlider.min.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('/js/build/script.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('js/lib/general.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/script.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lib/dialog.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lib/validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lib/jquery.jgrowl.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lib/sisyphus.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lib/garlic.js') }}"></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.1/jquery.form-validator.min.js'></script>

<div id="auth">
    <a href="#" data-dismiss="modal" class="close-popup">
        <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M15.562 15.5638L1.12517 1.125M15.562 1.12631L1.12451 15.5638L15.562 1.12631Z" stroke="white" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
    </a>
    <div class="wrap-content-auth">
        <svg class="logo-auth" width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clip-path="url(#clip0)">
                <path d="M18.8605 59.9901C24.5815 65.0735 36.778 65.1998 44.888 61.1584C47.2996 59.9287 49.5142 58.3429 51.4578 56.4539C44.3536 57.559 41.7132 58.9798 33.7917 56.4539C30.2107 55.1269 26.922 53.1106 24.11 50.518C21.0845 47.8725 18.6723 44.5936 17.0432 40.9117C15.414 37.2298 14.6073 33.2342 14.6798 29.2057C9.43026 36.0257 11.002 50.8969 18.8605 59.9901Z" fill="#FF8877"/>
                <path d="M46.4912 45.8451C38.4755 50.1707 33.446 49.2235 24.4873 47.7711C25.2417 48.371 25.336 48.7183 26.2791 49.4445C31.836 53.7704 38.787 55.8841 45.7997 55.3804C55.5443 54.2753 58.0276 51.5915 61.5168 44.3296C64.6602 38.0148 65.006 25.3853 61.5168 21.1228C60.3851 32.1105 56.8331 40.2881 46.4912 45.8451Z" fill="#FF8877"/>
                <path d="M59.4421 17.8076C59.0013 14.373 57.3105 11.2238 54.6955 8.96695C47.4342 1.23138 40.8645 0 30.9941 0C33.666 1.95757 44.0079 4.64134 48.4087 16.7025C53.0609 29.5215 49.0374 35.3626 45.0138 43.6981C51.5521 41.33 54.444 35.2995 57.0216 31.0686C58.7336 26.9075 59.5684 22.4352 59.4735 17.9339" fill="#FF8877"/>
                <path d="M13.5168 5.05177C11.6077 6.29753 9.86469 7.78297 8.33012 9.4721C5.09596 13.1703 2.85616 17.6371 1.82324 22.4489L3.83503 20.7439C6.33417 17.7246 9.4754 15.3063 13.0277 13.6671C16.5799 12.0279 20.4528 11.2094 24.3616 11.2718C31.2071 11.2457 37.8299 13.7136 43.002 18.218L47.4028 22.6699C46.2151 16.3481 42.8825 10.6353 37.9726 6.50416C35.5505 4.64886 32.8712 3.15963 30.0197 2.08384C23.9215 -0.221045 18.8606 1.45236 13.5168 5.05177Z" fill="#FF8877"/>
                <path d="M2.16907 25.259C0.619118 28.4433 -0.123072 31.9631 0.00855031 35.5051C0.140172 39.0471 1.14152 42.5016 2.92349 45.5609C5.29776 50.6716 9.21282 54.9023 14.1141 57.6537C13.5797 55.1909 5.43822 45.4346 10.9706 30.8791C15.4972 19.481 22.1926 17.776 28.731 14.1135C23.2811 13.3856 17.7392 14.3207 12.8253 16.7973C8.66348 18.7423 5.01373 21.6405 2.16907 25.259Z" fill="black"/>
                <path d="M29.9254 15.7869C26.5439 17.4327 23.3767 19.4899 20.4952 21.9122C18.6632 22.9143 17.2684 24.5657 16.5827 26.5445C15.8969 28.5233 15.9694 30.6877 16.7859 32.6157C20.7467 45.0873 19.7408 46.5082 34.2633 46.5713C39.5757 46.5713 42.7191 45.3399 44.2909 41.4879C50.9549 25.259 47.4343 25.038 40.8645 19.8915C38.2869 17.8076 34.5777 14.4608 29.894 15.7869H29.9254ZM32.7231 18.6285C37.1867 17.7445 39.0099 20.8703 40.9274 23.333C47.2142 31.6054 46.837 32.4894 40.7074 40.667C38.8527 43.1298 37.2182 45.8135 32.6602 45.0558C29.2451 44.2376 25.9181 43.0849 22.727 41.6142C19.5836 39.2778 19.5836 36.6572 19.5836 31.8579C19.5836 27.0587 19.7722 24.0592 23.0413 21.9122C26.1728 20.5261 29.4106 19.3963 32.7231 18.5338V18.6285Z" fill="#FF8877"/>
                <path d="M38.6957 32.7104C39.3873 30.5003 39.4187 29.2373 37.4383 27.7218C33.1004 24.4065 32.2831 23.6172 28.511 26.3325C27.1279 27.3429 25.5877 28.0691 25.2104 29.9004C25.2104 30.6897 25.2104 38.2043 30.5228 38.52C37.1554 38.9305 36.9983 38.0148 38.6957 32.7104Z" fill="#FF8877"/>
                <path d="M27.002 21.6596C23.2928 23.1436 23.8586 23.3962 22.3183 27.7849C21.4067 30.6581 19.9608 33.7208 21.8154 36.6571C23.7305 38.5007 25.8383 40.131 28.1022 41.5195C29.2616 42.5154 30.7505 43.0387 32.2751 42.9861C33.7997 42.9335 35.2494 42.3088 36.338 41.2353C41.9647 36.9097 44.6995 37.036 41.6818 27.8796C40.6445 24.7223 40.0786 22.3858 37.061 21.5649C33.7923 20.9124 30.4278 20.9124 27.1592 21.5649L27.002 21.6596ZM23.0413 32.2368C23.0413 30.4371 26.4991 22.7647 31.0256 23.0489C33.3308 23.5389 35.5759 24.28 37.7211 25.259C39.7329 26.4273 40.2358 28.2585 40.2358 31.4159C40.2358 36.4993 40.2358 39.0252 31.9686 40.5407C26.4991 41.3932 23.0728 32.8999 23.0413 32.2368Z" fill="#FF8877"/>
            </g>
            <defs>
                <clipPath id="clip0">
                    <rect width="64" height="64" fill="white"/>
                </clipPath>
            </defs>
        </svg>


        <h4>Вход в личный кабинет</h4>
        <form action="" method="post" autocomplete="off">
            {{ csrf_field() }}
            <label for="">
                <span>Ваш e-mail</span>
                <input type="email" name="email" autocomplete="false" placeholder="Например: example@gmail.com">
                <span class="error-field">Неправильные данные</span>
            </label>
            <ul>
                <li>
                    <label for="">
                        <span>Введите пароль</span>
                        <input type="password" name="password">
                    </label>
                </li>
                <li>
                    <a class="btn-auth" href="javascript:void(0);">Войти</a>
                </li>
            </ul>
        </form>


        <div class="info-message">
            <ul>
                <li><svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M14.6805 46.5L1.5 33.3195V14.6797L14.6805 1.5H33.321L46.5 14.6797V33.3195L33.321 46.5H14.6805Z" stroke="#9F0226" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M24 9.75V27.75" stroke="#9F0226" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M24 35.25C24.8284 35.25 25.5 34.5784 25.5 33.75C25.5 32.9216 24.8284 32.25 24 32.25C23.1716 32.25 22.5 32.9216 22.5 33.75C22.5 34.5784 23.1716 35.25 24 35.25Z" stroke="#9F0226" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>

                </li>
                <li>Регистрация происходит автоматически после создания заказа. Доступ направляется на e-mail и на телефон</li>
            </ul>
        </div>

    </div>
</div>
<div id="status">
    <a href="#" data-dismiss="modal" class="close-popup">
        <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M15.562 15.5638L1.12517 1.125M15.562 1.12631L1.12451 15.5638L15.562 1.12631Z" stroke="white" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
    </a>

    <div class="img-wrap-status">
        <svg width="93" height="93" viewBox="0 0 93 93" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M2.90625 8.71875H11.625L26.1562 66.8438H72.6562L78.6141 49.4062M55.2188 31.9688H17.4375H55.2188Z" stroke="#FF8877" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M29.0625 84.2812C32.2727 84.2812 34.875 81.6789 34.875 78.4688C34.875 75.2586 32.2727 72.6562 29.0625 72.6562C25.8523 72.6562 23.25 75.2586 23.25 78.4688C23.25 81.6789 25.8523 84.2812 29.0625 84.2812Z" stroke="#FF8877" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M66.8438 84.2812C70.0539 84.2812 72.6562 81.6789 72.6562 78.4688C72.6562 75.2586 70.0539 72.6562 66.8438 72.6562C63.6336 72.6562 61.0312 75.2586 61.0312 78.4688C61.0312 81.6789 63.6336 84.2812 66.8438 84.2812Z" stroke="#FF8877" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M66.8438 37.7812C74.8691 37.7812 81.375 31.2754 81.375 23.25C81.375 15.2246 74.8691 8.71875 66.8438 8.71875C58.8184 8.71875 52.3125 15.2246 52.3125 23.25C52.3125 31.2754 58.8184 37.7812 66.8438 37.7812Z" stroke="#FF8877" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M77.3062 33.7125L90.0937 46.5" stroke="#FF8877" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
    </div>

    <h3>Проверить статус заказа</h3>
    <span>Чтобы узнать статус заказа введите его номер <br> и нажмите кнопку Проверить</span>

    <div class="wrap-content-auth">
        <form action="">
            {{ csrf_field() }}
            <label for="">
                <span>Ваш номер заказа</span>
                <input type="text" name="number" autocomplete="off" placeholder="Например: 4234234">
                <span class="error-field">Ошибка поиска</span>
            </label>
            <a class="btn-check-status" href="javascript:void(0);">Проверить</a>
        </form>
    </div>

</div>
@if(session('see-later') && Route::currentRouteName() == 'product')

    @php
        $productsSee = \App\Product::whereIn('id', array_values(session('see-later')))->get();
    @endphp

    @if($productsSee)
        <div class="see-products">
            <a href="javascript:void(0);" class="btn-hide-element" data-active="1">
                <svg width="24" height="9" viewBox="0 0 24 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.77734 8.4964C4.73828 7.36359 3.83984 6.50031 3.08203 5.90656C2.32422 5.32062 1.60938 4.88312 0.9375 4.59406V4.06671C1.71094 3.69171 2.46094 3.21124 3.1875 2.62531C3.91406 2.04718 4.78125 1.19562 5.78906 0.0706177H6.69141C5.95703 1.64093 5.1875 2.84796 4.38281 3.69171L23.0742 3.69171V4.87531L4.38281 4.87531C4.97656 5.63312 5.39063 6.19562 5.625 6.56281C5.85938 6.92999 6.20703 7.57452 6.66797 8.4964H5.77734Z" fill="white"/>
                </svg>
            </a>
            <div class="content-see">
                <h4>Вы просматривали</h4>
                <ul>
                    @foreach($productsSee as $p)

                        @if($loop->iteration == 4)
                            @break
                        @endif

                        @if($p->id == $productId)
                            @continue
                        @endif
                        <li>
                            <a href="{{ route('product', ['slug' => $p->slug]) }}">
                                <img src="{{ url('storage/'.$p->main_img) }}" alt="">
                            </a>
                            <a class="product-name" href="{{ route('product', ['slug' => $p->slug]) }}">{{ $p->name }}</a>
                        </li>
                    @endforeach

                    <li class="btn-show-more-see">
                        <a href="javascript:void(0);">
                            <svg width="10" height="23" viewBox="0 0 10 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.26563 18.1427C8.13281 19.1817 7.26953 20.0802 6.67578 20.838C6.08984 21.5958 5.65234 22.3106 5.36328 22.9825H4.83594C4.46094 22.2091 3.98047 21.4591 3.39453 20.7325C2.81641 20.0059 1.96484 19.1387 0.839844 18.1309V17.2286C2.41016 17.963 3.61719 18.7325 4.46094 19.5372V0.845779L5.64453 0.845779L5.64453 19.5372C6.40234 18.9434 6.96484 18.5294 7.33203 18.295C7.69922 18.0606 8.34375 17.713 9.26563 17.252V18.1427Z" fill="black"/>
                            </svg>

                        </a>
                    </li>
                </ul>
            </div>
        </div>
    @endif
@endif

<div class="callback-popup" id="callbackPopup">
    <a href="#" data-dismiss="modal" class="close-popup">
        <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M15.562 15.5638L1.12517 1.125M15.562 1.12631L1.12451 15.5638L15.562 1.12631Z" stroke="white" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
    </a>

    <div class="logo-callback">
        <img src="/svg/logo.svg">
    </div>

    <h4>Оставьте заявку и мы перезвоним Вам в ближайщее время</h4>

    <form action="/save-application" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <ul class="field-form-application">
            <li>
                <label for="">Ваше имя</label>
                <input type="text" data-validation="required" data-validation-error-msg="Обязательное поле" name="name" placeholder="Например: Александр">
            </li>
            <li>
                <label for="">Ваш номер телефона</label>
                <input type="text" data-validation="required" data-validation-error-msg="Обязательное поле" name="phone" class="phone" placeholder="+7 ( ) ___-__-__">
            </li>
        </ul>


        <label style="padding-left:15px;" class="style-label" for="">Опишите кратко свой вопрос или проблему</label>
        <textarea name="text" placeholder="Например: здравствуйте, я хотел бы поменять товары в заказе № 012345, но заказ уже в пути, как мне можно отказаться или заменить букет? Спасибо" id="" cols="30" rows="10">

        </textarea>

        <div class="file-popup">
            <a href="">Прикрепить файл</a>
            <input type="file" name="file" style="display: none;">
            <span>(jpg, png, pdf, doc, txt - до 10 мб)</span>
        </div>

        <ul class="confidenc">
            <li>Нажимая на кнопку, Вы соглашаетесь на <a href="">обработку персональных данных</a></li>
            <li>
                {{--<a href="javascript:void(0);" class="btn-send-form">Отправить</a>--}}
                <input type="submit" value="Отправить" name="submit" class="btn-send-form">
            </li>
        </ul>


    </form>



</div>

</body>
</html>