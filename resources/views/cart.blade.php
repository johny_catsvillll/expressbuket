@extends('layout')
@section('content')
	@if($products)
		<div class="ww-wrap">
			<div id="cart" class="cart container stage12">
				<div class="row" style="margin:0;">

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:20px;margin-top: 30px;margin-bottom:40px;">
						<div class="line-zakaz">
							<div class="col-xs-4 col-lg-4" style="position:relative;">
								<div class="line-zakaz-wrap">
									<div class="line-title active">Корзина</div>
									<div class="line-circle active">1</div>
								</div>
							</div>
							<div class="col-xs-4 col-lg-4" style="position:relative;text-align:center">
								<div class="line-zakaz-wrap">
									<div class="line-title hidden-xs hidden-sm" style="left: -35px;">Оформление</div>
									<div class="line-circle">2</div>
								</div>
							</div>
							<div class="col-xs-4 col-lg-4" style="position:relative;">
								<div class="line-zakaz-wrap" style="float:right;">
									<div class="line-title hidden-xs hidden-sm" style="left: -5px;">Оплата</div>
									<div class="line-circle">3</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<form id="pay-form1" action="/site/order" method="post" class="has-validation-callback">
					{{ csrf_field() }}
					<div class="row" style="margin: 0;">

						<table class="cart-table">
							@include('blocks.cart')
						</table>
						<div class="row">
							<div class="col-xs-12 col-md-6 cart-checkboxes">
								<span class="col-xs-12 order-title">У вас есть промокод?</span>
								<div class="promocode-input col-xs-12">
									<input id="promo" type="text" name="promocode" placeholder="Промокод" class="order-input" value="">
									<span class="active-promo">Применить</span>
								</div>
								<div class="promo-text help-block form-error"></div>
								<label class="col-xs-12 col-sm-6 otkrytka">
									<input id="checkotkr" class="checkbox" type="checkbox" name="otkrytka">
									<span class="checkbox-custom"></span>
									<span class="label">Добавить открытку <i>бесплатно</i></span>
								</label>
								<label class="hidden-xs col-sm-6 tr otkrytka">
									<input id="photo" class="checkbox" type="checkbox" name="photo">
									<span class="checkbox-custom"></span>
									<span class="label">Сделать фото при получении</span>
								</label>
								<label class="hidden-sm hidden-md hidden-lg col-xs-12 col-sm-6 tr otkrytka">
									<input class="checkbox" type="checkbox" name="photo">
									<span class="checkbox-custom"></span>
									<span class="label">Сделать фото при получении</span>
								</label>
								<div class="textotkr" style="display:none;">
									<label for="" class="label-textarea">Текст поздравления</label>
									<textarea name="otkrytkatext" id="" class="otkrytka-text"></textarea>
								</div>
							</div>
							<div class="col-xs-12 col-md-6 cart-prices">
								<div class="cart-prices-row cart-total">
									Итого по позициям <span class="cart-prices-price"><b>{{ $allPrice }}</b> <i class="glyphicon glyphicon-ruble"></i></span>
								</div>
								<div class="cart-prices-row">
									<span class="small-block" style="float:left">Выберите зону доставки</span>
									<div class="radio-btn">
										<input type="radio" value="Волгоград" data-price="0" id="rc1" name="zonename" checked="checked" style="display: none">
										<label for="rc1" onclick="">Волгоград</label>
									</div>
									<div class="radio-btn">
										<input type="radio" value="Волжский" data-price="0" id="rc2" name="zonename">
										<label for="rc2" onclick="">Волжский</label>
									</div>
									<span class="cart-prices-price">
									@if($allPrice <= 500)
											<b class="zone-price">300 р</b>
										@else
											<b class="zone-price">бесплатно</b>
										@endif

								</span>
									<div class="clearfix"></div>
									<div class="hint">Стоимость доставки за пределы указанных зон уточняйте по телефону +7 (8442) 500-731</div>
									<input type="hidden" name="zone" value="Волгоград">
								</div>
								<div class="clearfix"></div>
								<div class="cart-prices-row itogo" style="border:none">
									Итого <span class="cart-prices-price"><b class="js-total-price">{{ $allPrice }}</b> <i class="glyphicon glyphicon-ruble"></i></span>
									<input type="hidden" name="sum" value="{{ $allPrice }}">
									<input type="hidden" name="standartPrice" value="{{ $allPrice }}">
								</div>
								@if(auth('account')->check())
									<div class="cart-prices-row" style="border:none; padding-top:0;">

											Бонусы <span class="bonuses-style"><b>{{ auth('account')->user()->balance ? auth('account')->user()->balance->sum('price') : 0 }}</b>
												<i class="glyphicon glyphicon-ruble"></i></span>
									</div>
								@endif
							</div>
						</div>
						<div class="row text-center cart-final">
							<a href="{{ url('page/katalog') }}" title="" class="back-to-catalog hidden-xs hidden-sm">Вернуться к покупкам</a>
							<button type="submit" name="submit" class="white-button">Оформить заказ <i class="why-we-arr">+</i></button>
							<p class="text-center">
								<input type="checkbox" name="accepted" required="" checked="checked">&nbsp;Оформляя заказ, Вы соглашаетесь с <a style="text-decoration:underline;" target="_blank"
																																				href="{{ url('page/oferta') }}">условиями оферты</a>
							</p>
						</div>

					</div>
				</form>
			</div>
			<div class="container">
				<div class="row catalog cart-catalog">
					<i class="hicon"></i>
					<h1 id="gifts" class="title">Добавьте к букету</h1>
					<div class="catalog">
						<div class="products-slider">
							@foreach($presents as $present)
								<div class="product">
									<div class="product-inner">
										<a href="{{ route('product', ['slug' => $present->slug]) }}" title="{{ $present->name }}" class="img-wrap">
											<img class="product-img img-responsive" src="{{ url('storage/'.$present->main_img) }}" alt="{{ $present->name }}" title="{{ $present->name }} - Expressbuket">
										</a>
										<div class="text-center product-title ">
											<a href="{{ route('product', ['slug' => $present->slug]) }}">{{ $present->name }}</a>
										</div>
										<div class="text-center product-price">
											<span class="glyphicon glyphicon-ruble" aria-hidden="true"></span>
											@if($present->price)
												<b>{{ $present->price }}</b><span class="hidden-xs">руб</span>
											@elseif(json_decode($present->construktor_sting))
												<b>{{ getHelperPrice($present->construktor_sting) }}</b><span class="hidden-xs">руб</span>
											@endif
										</div>
										<div class="text-center">
											<a data-id="{{ $present->id }}" href="{{ url('page/korzina') }}" class="white-button catalog-buy__button" style="min-width:auto">Добавить <i class="why-we-arr">+</i></a>
										</div>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			@else
				@include('blocks.cart-empty')
			@endif
			@include('blocks.news')
		</div>
@endsection