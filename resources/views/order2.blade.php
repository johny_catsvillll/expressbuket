@extends('layout')
@section('content')
    <div class="container">
        <div class="row" style="margin:0;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:20px;margin-top: 30px;margin-bottom:40px;">
                <div class="line-zakaz">
                    <div class="col-xs-4 col-lg-4" style="position:relative;">
                        <div class="line-zakaz-wrap">
                            <div class="line-title hidden-xs hidden-sm">Корзина</div>
                            <div class="line-circle active">1</div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-lg-4" style="position:relative;text-align:center">
                        <div class="line-zakaz-wrap">
                            <div class="line-title hidden-xs hidden-sm" style="left: -35px;">Оформление</div>
                            <div class="line-circle active">2</div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-lg-4" style="position:relative;">
                        <div class="line-zakaz-wrap" style="float:right;">
                            <div class="line-title active" style="left: -5px;">Оплата</div>
                            <div class="line-circle active">3</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="row">
                <div class="col-xs-12">
                    <span class="order-title first">Ваш заказ:</span>
                    @php
                        $p = $order->allPrice - $order->bonuses;

                        if($p < 0) {
                            $p = 0;
                        }
                    @endphp
                    <span class="order-text">№ {{ $order->id }}  на сумму <b class="order-price">{{ $p }} <i class="glyphicon glyphicon-ruble"></i></b></span>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-xs-12 col-md-7 payment">
                <form action="https://money.yandex.ru/eshop.xml" method="POST" class="has-validation-callback">
                    <input name="shopId" value="94693" type="hidden">
                    <input name="scid" value="86445" type="hidden">
                    <input name="customerNumber" value="9623" type="hidden">
                    <input name="sum" value="5650" type="hidden">
                    <input type="hidden" name="shopSuccessURL" value="https://expressbuket.com/payment/success">
                    <input type="hidden" name="shopFailURL" value="https://expressbuket.com/payment/fail">
                    <span class="payment-title">Выберите способ оплаты</span>
                    <span class="payment-subtitle">1. Банковскими картами</span>

                    <label class="payment-method visa">
                        <input type="radio" data-name="paymethod" name="paymentType" id="payments-AC" class="payments inline-block" value="AC" aria-invalid="false" style="
				    position: absolute;
				    z-index: -999;">
                        <small>Visa/Mastercard</small>
                    </label>
                    <!--label class="payment-method sb">
                      <input type="radio"
                        data-name="paymethod"
                        name="paymentType"
                        id="payments-AC"
                        class="payments inline-block"
                        value="SB"
                        aria-invalid="false"
                        style="
                      position: absolute;
                      z-index: -999;">
                      <small>Карта Сбербанк</small>
                    </label-->
                    <label class="payment-method ac">
                        <input type="radio" data-name="paymethod" name="paymentType" id="payments-AC" class="payments inline-block" value="AB" aria-invalid="false" style="
	          position: absolute;
	          z-index: -999;">
                        <small>Альфа-клик</small>
                    </label>

                    <div class="clearfix"></div>
                    <span class="payment-subtitle">2. Наличными</span>
                    <label class="payment-method salon">
                        <input type="radio" data-name="paymethod" name="paymentType" id="payments-AC" class="payments inline-block" value="GP" aria-invalid="false" style="
	          position: absolute;
	          z-index: -999;">
                        <small>Связной и Евросеть</small>
                    </label>
                </form>
                <form class="courier-form has-validation-callback" name="courier-form" method="get" action="{{ url('status-order?order='. $order->id) }}">
                    <label class="payment-method courier">
                        <input type="radio" data-name="paymethod" name="order" id="payments-AC" class="payments inline-block" value="{{ $order->id }}" aria-invalid="false" style="
	          position: absolute;
	          z-index: -999;">
                        <small>При получении</small>
                    </label>
                </form>
                <div class="clearfix"></div>
                <form action="https://money.yandex.ru/eshop.xml" method="POST" class="has-validation-callback">
                    <input name="shopId" value="94693" type="hidden">
                    <input name="scid" value="86445" type="hidden">
                    <input name="customerNumber" value="9623" type="hidden">
                    <input name="sum" value="5650" type="hidden">
                    <input type="hidden" name="shopSuccessURL" value="https://expressbuket.com/payment/success">
                    <input type="hidden" name="shopFailURL" value="https://expressbuket.com/payment/fail">
                    <span class="payment-subtitle">3. Электронные платежные системы</span>
                    <!--label class="payment-method phone">
                      <input type="radio"
                        data-name="paymethod"
                        name="paymentType"
                        id="payments-AC"
                        class="payments inline-block"
                        value="MC"
                        aria-invalid="false"
                        style="
                      position: absolute;
                      z-index: -999;">
                      <small>С телефона</small>
                    </label-->
                    <label class="payment-method qiwi">
                        <input type="radio" data-name="paymethod" name="paymentType" id="payments-AC" class="payments inline-block" value="QW" aria-invalid="false" style="
	          position: absolute;
	          z-index: -999;">
                        <small>Qiwi-кошелек</small>
                    </label>
                    <label class="payment-method yad">
                        <input type="radio" data-name="paymethod" name="paymentType" id="payments-PC" class="payments inline-block" value="PC" aria-invalid="false" style="
	          position: absolute;
	          z-index: -999;">
                        <small>Яндекс.Деньги</small>
                    </label>
                    <label class="payment-method wm">
                        <input type="radio" data-name="paymethod" name="paymentType" id="payments-AC" class="payments inline-block" value="WM" aria-invalid="false" style="
	          position: absolute;
	          z-index: -999;">
                        <small>WebMoney</small>
                    </label>
                </form>
            <!--form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
		    <input id="amount" type="hidden" value="5650" />
		    <input type="hidden" name="cmd" value="_xclick" />
		    <input type="hidden" name="charset" value="utf-8" />
		    <input type="hidden" name="bussiness" value="expressbuketkassa@yandex.ru" />
		    <input type="hidden" name="item_name" value="Заказ 9623" />
		    <input type="hidden" name="currency_code" value="RUB" />
		    <input type="hidden" name="undefined_quantity" value="1" />
		    <input type="hidden" name="return" value="https://expressbuket.com/payment/success" />
		    <input type="hidden" name="cancel_return" value="https://expressbuket.com/payment/fail" />
		    <input type="hidden" name="no_note" value="1" />
		    <input type="hidden" name="no_shipping" value="1" />
		    <input type="hidden" name="rm" value="" />
		    <label class="payment-method paypal">
	          <input type="radio"
	            data-name="paymethod"
	            class="payments inline-block"
	            value="Paypal"
	            aria-invalid="false"
	            style="
	          position: absolute;
	          z-index: -999;">
	          <small>PayPal</small>
	        </label>
		 </form-->

            </div>

            <div class="col-xs-12 col-md-5 payment-benefits">
                <div class="benefit1">
                    <span class="benefit-title">Вы можете на нас положиться!</span>
                    Мы используем только защищенные способы приема денежных средств. Безопасность платежей по пластиковым картам обеспечивается сертифицированными по международным стандартам безопасности системами электронных платежей.
                </div>
                <div class="benefit2">
                    <span class="benefit-title">Зачисление платежа моментально!</span>
                    Мы незамедлительно сообщим Вам о поступлении денежных средств на счет!
                </div>
            </div>
        </div>
        <div class="row text-center cart-final">
            <!--a href="https://expressbuket.com/page/korzina" title="" class="back-to-catalog hidden-xs">Вернуться к оформлению заказа</a-->
            <!--button type="submit" name="" class="white-button">Оплатить <i class="why-we-arr"></i></button-->
            <!--p class="text-center oferta">Оформляя заказ вы соглашаетесь с <a href="#" title="" data-toggle="modal" data-target="#oferta-modal">условиями оферты</a></p-->
        </div>

        <div class="row hidden-sm hidden-md hidden-lg text-center" style="margin-bottom: 60px"><a href="" title="" class="back-to-catalog">Вернуться к оформлению заказа</a></div>
    </div>
@endsection