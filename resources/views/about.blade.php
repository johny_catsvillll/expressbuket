@extends('layout')
@section('content')
		<div class="breadcrumbs">
			<div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<a href="/" itemprop="url"><span itemprop="title">Главная</span></a>
			</div>
			<div class="active"><span>О компании</span></div>
		</div>
		 <div class="about-page">
			<div class="about-page__header">
				<div class="container">
					<h1 class="about-title">О компании</h1>
					<span class="subtitle">
						Могут ли в сфере цветочных продаж быть какие-либо инновации? Да, если речь идет о нашей компании. «EXPRESS  БУКЕТ» - это всегда живые и свежие цветы, исключительный уровень сервиса, широкий спектр услуг и приемлемые для наших граждан цены.
					</span>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<i class="hidden-xs hicon"></i>
						<div class="about-page__title title">
							Наши принципы и приоритеты
						</div>
						<div class="row">
							<div class="col-xs-12">
								<p>Основная задача нашей компании – это обеспечение клиентов первоклассными цветами. Но за таким, казалось бы, простым мероприятием, стоит очень тяжелый труд многих людей. Профессионалы, работающие у нас, создают уникальные цветочные подарки. Благодаря нам, вы сможете порадовать близкого человека или просто друга роскошным букетом, оформленным опытными флористами. Мы предлагаем вам принять участие в создании цветочных подарков, с обязательным учетом пожеланий и индивидуальных предпочтений клиента. </p>
								<p>Как и у каждой динамично развивающейся компании, у нас есть принципы, на которых мы и строим свою деятельность:</p>
								<ul>
									<li>качественная продукция. В наших магазинах вы не найдете искусственные или несвежие цветы. Ассортимент обновляется каждые два дня, и растения поставляются напрямую с плантаций, минуя посредников;</li>
									<li>высокий уровень сервиса – мы делаем все для того, чтобы клиенты были довольны сотрудничеством с компанией «EXPRESS БУКЕТ». Да, это сложно, ведь каждый человек индивидуален, но если подойти к этому вопросу со всей ответственностью, то результатом станет тысячи благодарных клиентов;</li>
									<li>оперативность – структурированный штат, а также наличие собственной курьерской службы, позволяет нам доставлять готовые букеты минимум за два часа с момента оформления заказа. Кроме того, мы сможем доставить ваш заказ в любое время суток, даже среди ночи.</li>
								</ul>
								<blockquote>«Значимость этих проблем настолько очевидна, что рамки и место обучения кадров позволяет оценить
									значение соответствующий условий активизации. Товарищи! постоянное информационно-пропагандистское
									обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации позиций, занимаемых
									участниками в отношении поставленных задач передового производства!».
									<span class="signature">Директор по развитию.</span>
								</blockquote>
							</div>
						</div>
						<div class="row about-gallery">
							<div class="col-xs-12 col-md-4 col">
								<div>
									<img src="{{ asset('img/about-photo-1.jpg') }}" alt="" class="img-responsive">
								</div>
								<div>
									<img src="{{ asset('img/about-photo-1.jpg') }}" alt="" class="img-responsive">
								</div>
							</div>
							<div class="col-xs-12 col-md-4 center-col">
								<div href="img/about-photo-2.jpg" class="fancybox">
									<img src="{{ asset('img/about-photo-2.jpg') }}" alt="">
								</div>
							</div>
							<div class="col-xs-12 col-md-4 col">
								<div href="img/about-photo-1.jpg" class="fancybox">
									<img src="{{ asset('img/about-photo-1.jpg') }}" alt="" class="img-responsive">
								</div>
								<div href="img/about-photo-1.jpg" class="fancybox">
									<img src="{{ asset('img/about-photo-1.jpg') }}" alt="" class="img-responsive">
								</div>
							</div>
						</div>
						<i class="hidden-xs hicon"></i>
						<div class="about-page__title title">
							Наши компетенции
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-offset-2 col-md-8">
								<div class="row compet">
									<div class="col-xs-12 col-md-6 rocket">
										<span class="compet-title">Стремление к совершенству</span>
										<span class="compet-desc">
											Совершенству нет предела – это факт. Поэтому мы продолжаем усиленно трудиться, с целью повышения своего профессионализма, что положительно скажется и на уровне услуг для наших клиентов.
										</span>
									</div>
									<div class="col-xs-12 col-md-6 user">
										<span class="compet-title">Работа на результат</span>
										<span class="compet-desc">
											У нас 11 магазинов, где каждый день покупают огромное количество роскошных цветов на любой вкус. Кроме того, заказать букет с подарком вы можете и через сайт компании.
										</span>
									</div>
									<div class="clearfix"></div>
									<div class="col-xs-12 col-md-6 like">
										<span class="compet-title">Главная ценность – персонал</span>
										<span class="compet-desc">
											И это не преувеличение. Мы ценим каждого нашего сотрудника, ведь прекрасно понимаем, что они вкладывают в работу частичку своей души. А иначе создавать столь великолепные и уникальные по своему стилю цветочные композиции просто невозможно.
										</span>
									</div>
									<div class="col-xs-12 col-md-6 light">
										<span class="compet-title">Креативность – наше все!</span>
										<span class="compet-desc">
											Выбор букетов впечатляет. Присутствующие в каталоге композиции – это лишь верхушка айсберга. Мы готовы воплотить в жизнь любые фантазии клиента. Создаем необычные букеты под любой повод – дни рождения, юбилеи, свадьбы, годовщины, календарные праздники и многое другое.
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="history hidden-xs">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-2"></div>
						<div class="col-xs-12 col-md-8">
							<div class="history-title">
								История компании
							</div>
							<div class="history-desc">
								За пять лет своего существования, компания «EXPRESS БУКЕТ» превратилась ведущего дистрибьютора живых цветов, поставляемых к нам напрямую с плантаций из стран Латинской Америки, Европы, Африки. И вот как это происходило:
							</div>
							<div class="box ">
								<ul id="first-list" class="force-overflow">
									<li>
										<span></span>
										<div class="info">начало становления компании.</div>
										<div class="time">
											<span>2014-2015</span>
											<span></span>
										</div>
									</li>
									<li>
										<span></span>
										<div class="info">старт продаж. Первые трудности, которые мы учимся преодолевать. Первые благодарные клиенты и положительные отзывы о нашей работе.</div>
										<div class="time">
											<span>2015</span>
											<span></span>
										</div>
									</li>
									<li>
										<span></span>
										<div class="info">год, посвященный наращиванию возможностей компании. Расширение штата сотрудников, открытие новых магазинов.</div>
										<div class="time">
											<span>2016</span>
											<span></span>
										</div>
									</li>

									<li>
										<span></span>
										<div class="info">усовершенствуем наработанные процессы. Благодаря этому, существенно повышается уровень сервиса. Мы работаем для того, чтобы дарить людям радость и положительные эмоции.</div>
										<div class="time">
											<span>2017</span>
											<span></span>
										</div>
									</li>
									<li>
										<span></span>
										<div class="info">компания «EXPRESS БУКЕТ» представляет собой развитую сеть цветочных магазинов, которая включает в себя 11 точек продаж, качественный сайт, собственную службу доставки с автопарком, а также расширенный штат сотрудников, насчитывающий более 50 профессионалов высокого уровня.</div>
										<div class="time">
											<span>2018</span>
											<span></span>
										</div>
									</li>

								</ul>
							</div>
						</div>
						<div class="col-xs-12 col-md-2"></div>
					</div>
				</div>
			</div>
			<div class="team">
				<div class="container">
					<div class="row">
						<div class="team-slider">
							<div class="row">
								<div class="team-slider__photo col-xs-12 col-md-5">
									<img src="{{ asset('img/team1.png') }}" alt="" class="img-responsive">
								</div>
								<div class="team-slider__info col-xs-12 col-md-7">
									<div class="team-slider__title">Наша команда</div>
									<div class="team-slider__name"></div>
									<div class="team-slider__rank">Генеральный директор</div>
									<hr>
									<div class="team-slider__desc">
										«Мы не просто создавали очередную серую фирму, которая бы плыла по течению профильного рынка. Нет, мы изначально ставили перед собой задачу организовать работу на самом высоком уровне, и постоянно совершенствоваться. Можно смело сказать, что у нас получилось, так как компания «EXPRESS БУКЕТ» уже сегодня является лидером рынка.»
									</div>
									<a href="mailto:info@expressbuket.com" class="team-slider__mail">info@expressbuket.com</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="statistic">
				<div class="container">
					<i class="hidden-xs hicon"></i>
					<div class="about-page__title title">
						Немного статистики
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-2"></div>
						<div class="col-xs-12 col-md-8">
							<p class="text-center">
								Если не смотреть за рынком, очень быстро потеряешь конкурентоспособность. Поэтому мониторингу мы уделяем особое внимание. Профессиональные аналитики постоянно анализируют нашу деятельность и работу конкурентов, чтобы найти оптимальные пути для дальнейшего развития и устранения обнаруженных недостатков. <br>
								Мы ведем тщательный учет своей деятельности, и готовы показать его вам в виде простых и понятных цифр.

							</p>
						</div>
						<div class="col-xs-12 col-md-2"></div>
					</div>
					<div class="row statistic-list">
						<div class="col-xs-12 col-md-3 statistic-item-1">
							<span>8457</span>
							<p>Выполнено доставок<br> за 2017 год</p>
						</div>
						<div class="col-xs-12 col-md-3 statistic-item-2">
							<span>95%</span>
							<p>Положительных<br> отзывов на сайте</p>
						</div>
						<div class="col-xs-12 col-md-3 statistic-item-3">
							<span>1255</span>
							<p>Прошло со дня<br> основания компании</p>
						</div>
						<div class="col-xs-12 col-md-3 statistic-item-4">
							<span>97%</span>
							<p>Успешно завершенных<br> доставок</p>
						</div>
					</div>
				</div>
			</div>
		</div>

@endsection