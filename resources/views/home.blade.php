@extends('layout')
@section('content')
		<div id="carousel-example-generic" class="carousel slide hidden-xs" data-ride="carousel">
		  	<div class="carousel-inner" role="listbox">
				@foreach($slider as $slide)
					<a href="{{ $slide->link ? $slide->link : 'javascript:void(0);' }}" class="item  @if($loop->first) active @endif">
						<img src="{{ asset('storage/'. $slide->image) }}" alt="{{ $slide->title }}">
					</a>
				@endforeach
		        <div class="slider-nav">
		      		<div class="container">
		        		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
		          			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		          			<span class="sr-only">Previous</span>
		        		</a>
		        		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
		          			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		          			<span class="sr-only">Next</span>
		        		</a>
		      		</div>
		    	</div>
		  	</div>
		  	<ol class="carousel-indicators">
		        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		        <li data-target="#carousel-example-generic" data-slide-to="1" ></li>
		    </ol>
		</div>
		<div class="container">
			<i class="hidden-xs hicon"></i>
			<h1 class="col-xs-12 title">Доставка цветов в {{ get_subdomain_city() }} </h1>
			@include('blocks.filter')
			<div class="catalog row">
				@foreach($products as $product)
					<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">

						<div class="product" data-id="{{ $product->id }}">
							<div class="product-inner">
								<span class="product-size">{{ $product->width }} {{ isset($product->height) ? 'x '. $product->height . ' см' : ''}} </span>
								@if($product->is_hit)
									<span class="product-label product-hit"><i>!</i>хит продаж</span>
								@endif

								@if($product->is_new)
									<span class="product-label product-newest"><i><img src="/img/icons/newest.png" alt="Новинка"></i>новинка</span>
								@endif

								@if($product->old_price)
									<span class="product-label product-sale"><i>%</i>букет со скидкой</span>
								@endif

								@if($product->old_price)
									<span class="product-oldprice line-trough"><span><b>{{ $product->old_price }}</b> <span class="hidden-xs">руб</span></span></span>
								@endif

								<a href="{{ route('product', ['slug' => $product->slug]) }}" title="{{ $product->name }}">
									<img class="product-img img-responsive"
										 src="{{ url('storage/'.$product->main_img) }}" alt="{{ $product->name }} - Expressbuket" title="{{ $product->name }} - Expressbuket">
									<div class="catalog-consist">
										<div class="catalog-consist-inner" style="display: none;">
											@if($product->composition)
												{!! str_limit(strip_tags($product->composition), 190, '...') !!}
											@else

												@php
													$products1 = [];

                                                    foreach (json_decode($product->construktor_sting) as $key => $pr) {
                                                        $product1 = \App\Product::find($pr->itemId);
                                                        $product1->count = $pr->count;
                                                        array_push($products1, $product1);
                                                    }
												@endphp

												@foreach($products1 as $pr)
													{{ $pr->name.' - '. $pr->count }} штук <br>
												@endforeach

											@endif
										</div>
									</div>
								</a>
								<div class="text-center product-title ">
									<a href="{{ route('product', ['slug' => $product->slug]) }}" title="{{ $product->name }}">{{ $product->name }}</a>
								</div>
								<div class="text-center product-price">
									@if($product->price)
										<b>{{ $product->price }}</b>
									@elseif(json_decode($product->construktor_sting))
										<b>{{ getHelperPrice($product->construktor_sting) }}</b>
									@endif
									<span class="glyphicon glyphicon-ruble" aria-hidden="true"></span>
									<a href="javascript:void(0);" class="btn-add-to-favorite" title="Добавить в избранное">
										<svg width="32" height="28" viewBox="0 0 32 28" fill="none" xmlns="http://www.w3.org/2000/svg">
<path @if(!(session('favorite') && isset(session('favorite')[$product->id]))) class="active-favorite" @endif d="M23.5 0.5C20.25 0.5 17.05 2.6 16 5.5C14.95 2.6 11.75 0.5 8.5 0.5C6.51088 0.5 4.60322 1.29018 3.1967 2.6967C1.79018 4.10322 1 6.01088 1 8C1 14.5 6.5 21 16 27.5C25.5 21 31 14.5 31 8C31 6.01088 30.2098 4.10322 28.8033 2.6967C27.3968 1.29018 25.4891 0.5 23.5 0.5Z" fill="#FF8877" stroke="#FF8877" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
</svg>

									</a>
								</div>
							</div>
							<div class="catalog-buttons">
								<div class="text-center">
									<a id="64" class="col-xs-11 white-button"
									   href="{{ route('product', ['slug' => $product->slug]) }}">Заказать <i class="why-we-arr"></i></a>
								</div>
								<div class="text-center">
									<a id="b{{ $product->id }}" class="one-click quick_btn">Купить в 1 клик</a>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
			<p class="text-center">
			  	<a href="{{ url('page/katalog') }}" class="white-button bold"><span>Перейти в каталог</span><i class="why-we-arr"></i></a>
			</p>

		  	<div class="row why-we">
		    	<i class="hidden-xs hicon"></i>
		    	<div class="title">Почему мы?</div>
				@foreach($advantages as $advantage)
		    		<div class="col-xs-12 col-sm-6 col-md-3 why-we-block">
		      		<div class="why-we-img">
		        		<img src="{{ asset('storage/'.$advantage->image) }}" alt="{{ $advantage->title }}" class="img-responsive">
		      		</div>
		      		<span class="why-we-title">{{ $advantage->title }}</span><br/>
		      		<span class="why-we-description">
		      			{{ $advantage->description }}
					</span>
		    	</div>
				@endforeach
		    	<div class="clearfix"></div>
		    	<p class="text-center">
		      		<a href="{{ url('page/preimuschestva') }}" class="white-button">Все наши преимущества <i class="why-we-arr"></i></a>
		    	</p>
		  	</div>
		</div>
		<div class="container-fluid photos">
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="col-xs-6 active">
				  	<a href="#delivery" aria-controls="delivery" role="tab" data-toggle="tab"><i class="photos-ico"></i>Фото доставок</a>
				</li>
				<li role="presentation" class="col-xs-6">
				  	<a id="tab-instagram" href="#instagram" aria-controls="instagram" role="tab" data-toggle="tab"><i class="photos-ico"></i>Из Instagram</a>
				</li>
			</ul>
			<i class="photos-line"></i>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane container active" id="delivery">
					<ul class="photos-slider">
					    @foreach($photosDelivery as $photo)
					    <li>
							<div class="photo">
								<img src="{{ asset('storage/'.$photo->image) }}" alt="{{ $photo->title }}">
							</div>
						</li>
						@endforeach
					</ul>
					<div class="clearfix"></div>
				    <p class="text-center">
				      	<a href="{{ url('page/fotogalereya') }}" class="all-objects-button">Смотреть все фото<i class="all-objects-arr"></i></a><br/>
				      	Присоединяйтесь к нам в - <a rel="nofollow" href="https://instagram.com/expressbuket/" title="в Instagram" target="_blank">
				    	Instagram
				  		</a>
				    </p>
				</div>
				<div role="tabpanel" class="tab-pane container active"  id="instagram">
      				<ul class="photos-slider2">
						@foreach($photosInstagram as $photo)
							<li>
								<div class="photo">
									<img src="{{ asset('storage/'.$photo->image) }}" alt="{{ $photo->title }}">
								</div>
							</li>
						@endforeach
				    </ul>
				    <div class="clearfix"></div>
				    <p class="text-center">
				      	<a href="{{ url('page/fotogalereya') }}" class="all-objects-button">Смотреть все фото<i class="all-objects-arr"></i></a><br/>
				      	Присоединяйтесь к нам в - <a rel="nofollow" href="https://instagram.com/expressbuket/" title="в Instagram" target="_blank">
				    	Instagram
				  		</a>
				    </p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="container">
  			<div class="row testimonials">
	    		<i class="hidden-xs hicon"></i>
	    		<div class="title">Отзывы</div>
				<ul class="testimonials-slider">
					@foreach($reviews as $review)
						<li>
							<div class="testimonial-content">
								<div class="testimonial-header">
									<span class="testimonial-title">{{ $review->name }}</span>
									<span class="testimonial-date">{{ $review->created_at }}</span>
								</div>
								<div class="testimonial-text">
									<p>{!! $review->text !!}</p>
								</div>
							</div>
						</li>
					@endforeach
	  			</ul>
	  		</div>
	  	</div>
		<div class="clearfix text-center">
		  	<a href="{{ url('page/otzyvy') }}" class="white-button"><span>Показать все отзывы</span><i class="why-we-arr"></i></a>
		</div>
		<div class="about">
			<div class="container">
				<div class="row">
					<i class="about-icon"></i>
					<div class="title">О компании</div>
					<div class="col-md-12 text-justify">
						{!! $seo ? $seo->bottom_text : '' !!}
					</div>
				</div>
			</div>
		</div>

		@include('blocks.news')


@endsection