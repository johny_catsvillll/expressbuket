@extends('layout')

@section('content')
    <style>
        body {
            background: #EAEFF3;
        }
    </style>
    <div class="container">
        <div class="breadcrumbs">
            <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/" itemprop="url"><span itemprop="title">Главная</span></a>
            </div>
            <div class="active"><span>Условия и правила</span></div>
        </div>
    </div>


    <div class="container" style="font-family:OpenSans-regular;">
        <div class="row">
            @include('account.blocks.header')

            <div class="col-lg-12">
                <div class="wrap-info-account-page">
                    @include('account.blocks.tabs')

                    <div class="wrap-rules">
                        <ul>
                            @foreach($rules as $rule)
                                <li>
                                    <div class="one-rules">
                                        <span>{{ $rule->name }}</span>
                                        <div class="wrap-content-rules">
                                            {!! $rule->description !!}
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection