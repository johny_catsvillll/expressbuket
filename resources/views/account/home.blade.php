@extends('layout')

@section('content')
    <style>
        body {
            background: #EAEFF3;
        }
    </style>
    <div class="container">
        <div class="breadcrumbs">
            <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/" itemprop="url"><span itemprop="title">Главная</span></a>
            </div>
            <div class="active"><span>Личный кабинет</span></div>
        </div>
    </div>


    <div class="container" style="font-family:OpenSans-regular;">
        <div class="row">
           @include('account.blocks.header')

            <div class="col-lg-12">
               <div class="wrap-info-account-page">
                   @include('account.blocks.tabs')

                   <div class="list-orders-account">
                       <ul>
                           @foreach($orders as $order)

                               @php
                                   $products = [];
                                $allPrice = 0;
                                $count = 0;

                                foreach (json_decode($order->products) as $key => $pr) {
                                    $product = \App\Product::find($pr->productId);
                                    if(!$product) {
                                        continue;
                                    }
                                    $price = 0;

                                    if(isset($pr->extra) && $pr->extra == 'extra') {
                                            $price += $product->extra_price * $pr->count;
                                    }
                                    else {
                                        if(json_decode($product->construktor_sting)) {
                                            $price += getHelperPrice($product->construktor_sting);
                                        }
                                        else {
                                            $price += $product->price * $pr->count;
                                        }
                                    }




                                    $product->allPrice = $price;
                                    $product->count = $pr->count;
                                    array_push($products, $product);
                                    $allPrice += $price;
                                    $count += $pr->count;
                                }
                               @endphp


                               <li>
                                   <div class="one-order-account">
                                       <div class="container">
                                           <div class="row">
                                               <div class="col-sm-7 col-lg-8">
                                                   <ul class="status-and-number">
                                                       <li>
                                                           Заказ №{{ $order->id }} от {{ $order->created_at }}
                                                       </li>
                                                       <li>
                                                           Статус: @if($order->status == 1) В обработке @elseif($order->status == 2) Курьер в пути @elseif($order->status == 3) Заказ доставлен @elseif($order->status == 4) Заказ отменен @endif
                                                       </li>
                                                   </ul>
                                                   <ul style="width:450px;" class="list-products">
                                                       @foreach($products as $product)
                                                           <li>
                                                               <a href="{{ route('product', ['slug' => $product->slug]) }}">
                                                                   <strong>{{ $product->name }}</strong> <span>{{ $product->allPrice }}<i class="glyphicon glyphicon-ruble" aria-hidden="true"></i>
    </span></a>
                                                           </li>
                                                       @endforeach

                                                   </ul>

                                                   <ul class="info-order">
                                                       <li><span>Доставить:</span> {{ $order->date_send }}</li>
                                                       <li><span>Получатель:</span> {{ $order->recipient_name }}</li>
                                                       <li><span>Отправитель:</span>{{ $order->sender_name }}</li>
                                                       <li><span>Адрес:</span> {{ $order->adress_send }}</li>
                                                       @if($order->text_otkrytka)
                                                           <li class="text-otrkytka"><span>Текст открытки:</span> {{ $order->text_otkrytka }}
                                                               <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                   <path d="M16.8334 9.80176L17.5391 9.09564C18.267 8.36814 18.0787 7.37664 17.3516 6.64914C16.6245 5.92164 15.6326 5.73339 14.9047 6.46164L14.199 7.16776" stroke="#008AC1" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                                                   <path d="M6.81152 14.5534L9.44627 17.1881M15.6803 10.9545L16.8334 9.80175L14.1983 7.16663L13.0455 8.31938L15.6803 10.9545Z" stroke="#008AC1" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                                                   <path d="M12 23.25C18.2132 23.25 23.25 18.2132 23.25 12C23.25 5.7868 18.2132 0.75 12 0.75C5.7868 0.75 0.75 5.7868 0.75 12C0.75 18.2132 5.7868 23.25 12 23.25Z" stroke="#008AC1" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                                                   <path d="M9.44625 17.1881L15.6803 10.9545L13.0455 8.3194L6.8115 14.5534L6 17.9996L9.44625 17.1881V17.1881Z" stroke="#008AC1" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                                               </svg>

                                                           </li>
                                                       @endif
                                                       @if($order->is_photo)
                                                           <li><span>Сделать фото при получении</span></li>
                                                       @endif

                                                       @if($order->is_phone_recipient)
                                                           <li><span>Не звонить получателю перед доставкой</span></li>
                                                       @endif

                                                        @php
                                                            $p = $order->allPrice - $order->bonuses;

                                                            if($p < 0) {
                                                                $p = 0;
                                                            }
                                                        @endphp
                                                       <li><span>Итого:</span> {{ $p }} <i class="glyphicon glyphicon-ruble" aria-hidden="true"></i>
                                                           наличными</li>
                                                   </ul>
                                               </div>
                                               <div class="col-lg-3 col-sm-4">

                                                   <div class="order-action-list">
                                                       <ul>
                                                           <li><a href="/order2?order_id={{ $order->id }}" class="no-active">Оплатить заказ</a></li>
                                                           <li><a href="#" data-target="#callbackPopup" data-toggle="modal"
                                                                  class="header__callback __element_sl __cmd_order active">Позвонить мне</a></li>
                                                           <li><a href="/account/repeat-order/{{ $order->id }}" class="active">Повторить</a></li>
                                                           <li><a href="/account/remove-order/{{ $order->id }}" class="remove">Удалить из истории</a></li>
                                                       </ul>
                                                   </div>

                                               </div>
                                           </div>
                                       </div>

                                   </div>
                               </li>
                           @endforeach
                       </ul>
                   </div>

               </div>
            </div>

        </div>
    </div>

@endsection