@extends('layout')

@section('content')
    <style>
        body {
            background: #EAEFF3;
        }
    </style>
    <div class="container">
        <div class="breadcrumbs">
            <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/" itemprop="url"><span itemprop="title">Главная</span></a>
            </div>
            <div class="active"><span>Ваш бонус</span></div>
        </div>
    </div>


    <div class="container" style="font-family:OpenSans-regular;">
        <div class="row">
            @include('account.blocks.header')

            <div class="col-lg-12">
                <div class="wrap-info-account-page">
                    @include('account.blocks.tabs')
                    <div class="wrap-bonus-account">
                        <div class="header-bonus">
                            <span>Ваш бонус: {{ auth('account')->user()->balance ? auth('account')->user()->balance->sum('price') : 0 }} р</span>
                        </div>

                        <div class="tabs-account">
                            <ul>
                                <li><a @if(!request()->types) class="active" @endif href="/account/bonus">Начисления</a>
                                <li><a @if(request()->types) class="active" @endif href="/account/bonus?types=3">Списания</a>
                            </ul>
                        </div>

                        <div class="tabs-account table-table-account">
                            <ul>
                                <li><a href="">Дата:</a>
                                <li><a href="">Номер заказа:</a>
                                <li><a href="">Сумма:</a>
                                {{--<li><a href="">Комментарий:</a>--}}
                            </ul>
                            @foreach($bonuses as $bonus)
                                <ul style="margin-top:-20px;">
                                    <li><a href="">{{ $bonus->created_at }}</a>
                                    <li><a href="">{{ $bonus->order_id }}</a>
                                    <li><a href="">{{ $bonus->price }} руб</a>
                                    {{--<li><a href=""></a>--}}
                                </ul>
                            @endforeach
                        </div>

                        <div class="wrap-description-bonus">
                            <h3>Как начисляются бонусы</h3>

                            <div class="wrap-info-bonus">
                                <h4>Способ 1</h4>
                                <p>Для участия в Программе вам необходимо зарегистрироваться. Это можно сделать следующим способом: Зарегистрироваться в Программе на сайте expressbucket.com в разделе «Регистрация»</p>
                            </div>

                            <div class="wrap-info-bonus">
                                <h4>Способ 2</h4>
                                <p>Регистрация вас в качестве Участника Программы производится на основании заполненных данных на сайте. После регистрации в Программе на номер телефона, указанный в Анкете-заявлении, будет отправлена SMS/Email с подтверждением регистрации в Программе.</p>
                            </div>

                            <div class="wrap-info-bonus">
                                <h4>Примечание</h4>
                                <p>С момента регистрации вы самостоятельно контролируете изменение ваших персональных данных (Email, номер телефона, адрес). Вы можете изменить свои персональные данные (Email, номер телефона, адрес) в личном кабинете «Expressbuket», на своей персональной странице на сайте в разделе Личный кабинет.</p>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection