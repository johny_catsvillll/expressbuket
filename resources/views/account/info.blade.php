@extends('layout')

@section('content')
    <style>
        body {
            background: #EAEFF3;
        }
    </style>
    <div class="container">
        <div class="breadcrumbs">
            <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/" itemprop="url"><span itemprop="title">Главная</span></a>
            </div>
            <div class="active"><span>Данные клиента</span></div>
        </div>
    </div>


    <div class="container" style="font-family:OpenSans-regular;">
        <div class="row">
            @include('account.blocks.header')

            <div class="col-lg-12">
                <div class="wrap-info-account-page">
                    @include('account.blocks.tabs')

                    <form action="/account/info" method="post" class="has-validation-callback">
                        {{ csrf_field() }}
                        <div class="wrap-info-account-page2">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-5">
                                        <h3>Персональные данные</h3>
                                        <span>Вы можете изменить персональные данные, для этого укажите новые данные ниже и нажмите «Сохранить» внизу страницы. </span>

                                        <label for="">
                                            <span>Ваше Имя</span>
                                            <input type="text" data-validation="required" data-validation-error-msg="Обязательное поле"
                                                   name="name" autocomplete="false" value="{{ auth('account')->user()->name }}">
                                        </label>

                                        <label for="">
                                            <span>Ваш номер телефона</span>
                                            <input type="text" data-validation="required" class="phone" value="{{ auth('account')->user()->phone }}" data-validation-error-msg="Обязательное поле" name="phone" autocomplete="false">
                                        </label>

                                        <label for="">
                                            <span>Ваш e-mail</span>
                                            <input type="email" data-validation="required" value="{{ auth('account')->user()->email }}"
                                                   data-validation-error-msg="Обязательное поле" name="email" autocomplete="false" placeholder="Например: example@gmail.com">
                                        </label>

                                        <ul class="list-select-sex">
                                            <li>Пол:</li>
                                            <li>
                                                <input type="radio" @if(auth('account')->user()->sex == 1)) checked @endif name="sex" id="sex1" value="1">
                                                <label for="sex1">
                                                    Мужской
                                                </label>
                                            </li>
                                            <li>
                                                <input type="radio" name="sex" id="sex2" @if(auth('account')->user()->sex == 2)) checked @endif value="2">
                                                <label for="sex2">
                                                    Женский
                                                </label>
                                            </li>
                                        </ul>

                                    </div>
                                    <div class="col-lg-3 col-sm-5">
                                        <h3>Пароль</h3>
                                        <span>Вы можете изменить пароль для входа, для этого укажите новый пароль ниже и нажмите «Сохранить» внизу страницы.</span>

                                        <label for="">
                                            <span>Введите пароль</span>
                                            <input type="password" name="password" autocomplete="new-password">
                                        </label>

                                        <label for="">
                                            <span>Повторите пароль</span>
                                            <input type="password" name="repeat_password" autocomplete="false" >
                                        </label>


                                    </div>
                                    <div class="col-lg-4 col-sm-11">
                                        <h3>Дополнительная информация</h3>
                                        <span>Здесь вы можете указать дополнительную информацию: альтернативный способ связи, пожелания к доставке и оформлению.</span>

                                        <label for="">
                                            <span>Дополнительная информация</span>
                                            <textarea name="description" id="" cols="30" rows="10" placeholder="Здесь вы можете указать дополнительную информацию: альтернативные способы связи, пожелания к доставке и оформлению и т.д.">{{ auth('account')->user()->description }}</textarea>
                                        </label>

                                        <input type="submit" name="submit" value="Сохранить">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>

@endsection