@extends('layout')

@section('content')
    <style>
        body {
            background: #EAEFF3;
        }
    </style>
    <div class="container">
        <div class="breadcrumbs">
            <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/" itemprop="url"><span itemprop="title">Главная</span></a>
            </div>
            <div class="active"><span>Настройка уведомлений</span></div>
        </div>
    </div>


    <div class="container" style="font-family:OpenSans-regular;">
        <div class="row">
            @include('account.blocks.header')

            <div class="col-lg-12">
                <div class="wrap-info-account-page">
                    @include('account.blocks.tabs')

                    <form action="/account/notifications" method="post">
                        {{ csrf_field() }}
                        <div class="wrap-notification-account">
                            <ul>
                                <li><span>Уведомления</span></li>
                                <li>
                                    <input type="checkbox" value="1" name="sms" id="sms" @if(auth('account')->user()->is_sms) checked @endif>
                                    <label for="sms">SMS</label>
                                </li>
                                <li>
                                    <input type="checkbox" value="1" name="email" id="emailAccount" @if(auth('account')->user()->is_email) checked @endif>
                                    <label for="emailAccount">Почта</label>
                                </li>
                            </ul>

                            <input type="submit"  name="submit" class="bts-save-notifications" value="Сохранить изменения">
                        </div>
                    </form>



                </div>
            </div>

        </div>
    </div>

@endsection