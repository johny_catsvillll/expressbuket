<div class="col-lg-12">
    <div class="wrap-top-account">
        <ul>
            <li>
                <a href="/account/logout" class="btn-exit">Выйти</a>
                <div class="photo-account"></div>
                <h3>{{ auth('account')->user()->name }}, приветствуем Вас!</h3>
                <span>Баланс: {{ auth('account')->user()->balance ? auth('account')->user()->balance->sum('price') : 0 }} р</span>
            </li>
            <li>
                <div class="overlay"></div>
                <div class="wrap-background">
                    <p>Пригласи друга попробовать expressbuket.com Вы и Ваш друг получите <b>по 500 руб.</b> на счет</p>
                    <ul>
                        <li>
                            <span>Промокод для друзей</span>
                            <a href="javascript:void(0);">{{ auth('account')->user()->promocode->code }}</a>
                        </li>
                        <li>
                            <span>Поделиться промокодом:</span>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>