<div class="tabs-account">
    <ul>
        <li><a @if(Request::path() == 'account') class="active" @endif href="/account/">История заказов</a>
        <li><a @if(Request::path() == 'account/bonus') class="active" @endif href="/account/bonus">Ваш бонус</a>
        <li><a @if(Request::path() == 'account/rules') class="active" @endif href="/account/rules">Условия и правила</a>
        <li><a @if(Request::path() == 'account/info') class="active" @endif href="/account/info">Данные клиента</a>
        <li><a @if(Request::path() == 'account/notifications') class="active" @endif href="/account/notifications">Настройка уведомлений</a>
    </ul>
</div>