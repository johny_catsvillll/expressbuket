@extends('layout')

@section('content')
    <div class="container">
      <div class="breadcrumbs">
        <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
          <a href="/" itemprop="url"><span itemprop="title">Главная</span></a>
        </div>
        <div class="active">
          <span>Новости</span>
        </div>
      </div>
      <div class="row news">
        @include('blocks.left-sidebar')
      	<div class="col-xs-12 col-md-8 content" style="float:right">
      		<h1 class="content-title">{{ $seo ? $seo->h1 : '' }}</h1>
            @foreach($news as $new)
              <div class="media col-xs-12 col-md-6">
                <div class="media-left">
                  <a href="{{ url('novosti/'.$new->slug) }}">
                    <img class="media-object" src="{{ asset('/storage/'.$new->image) }}" alt="{{ $new->name }}" title="{{ $new->name }}">
                  </a>
                </div>
                <div class="media-body">
                  <span>{{ date('d.m.Y', strtotime($new->publication)) }}</span>
                  <a href="{{ url('novosti/'.$new->slug) }}" title="{{ $new->name }}" class="media-heading">{{ $new->name }}</a>
                    {!! $new->exceprt !!}
                </div>
              </div>
            @endforeach
          {{ $news->links() }}
        </div>
      </div>
    </div>
@endsection