@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="breadcrumbs">
                <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url"><span itemprop="title">Главная</span></a></div><div class="active"><span>Вопрос-ответ</span></div></div>	</div>
        <div class="row">

            <i class="hidden-xs hicon"></i>
            <h1 class="col-xs-12 title">{{ $seo ? $seo->h1 : '' }}</h1>
            <div class="col-xs-12 content product-right">
                <div class="panel-group" id="faq" role="tablist" aria-multiselectable="true">
                    <div class="panel">
                        <div class="panel-heading" role="tab" id="heading24">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#faq" href="#collapse24" aria-controls="collapse24" class="collapsed" aria-expanded="false">
                                    Как оформить заказ?				        </a>
                            </h4>
                        </div>
                        <div id="collapse24" class="panel-collapse collapse" role="tabpanel" aria-expanded="false">
                            <div class="panel-body">
                                <div class="how-to">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-2 how-to-line hidden-xs hidden-sm "></div>
                                            <div class="col-xs-12 col-md-10">
                                                <div class="float-left width60">
                                                    <span class="how-to-title">Закажите букет удобным способом:</span>
                                                    <ul class="how-to-list first">
                                                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> На нашем сайте - <a href="https://expressbuket.com" title="">expressbuket.com</a></li>
                                                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> По телефонам: 8 (8442) 500-731 (для Волгограда);  8 800 500-79-94 (бесплатно по России)</li>
                                                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> У флориста в любом из наших магазинов</li>
                                                    </ul>
                                                    <span class="how-to-title">Оплатите заказ и ожидайте доставку:</span>
                                                    <ul class="how-to-list first">
                                                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Оплатить онлайн можно любыми удобными способами без комиссии</li>
                                                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Наши операторы примут Ваш заказ и уточнят все детали</li>
                                                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Букет будет доставлен точно в указанное время</li>
                                                    </ul>
                                                </div>
                                                <img class="hidden-xs hidden-sm float-right width40" src="/img/note.png" alt="">
                                                <div class="clearfix"></div>
                                                <div class="how-to-icons hidden-xs hidden-sm">
                                                    <img src="/img/how-icons.png" alt="" class="img-responsive">
                                                </div>
                                                <div class="float-left width60">
                                                    <span class="how-to-title">Будьте уверены - букет 100% доставят:</span>
                                                    <ul class="how-to-list last">
                                                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Мы присылаем в СМС и на e-mail фото счастливого получателя букета и эта  опция абсолютно бесплатна! Для нас важно, чтобы Вы были уверены в надежности нашего сервиса.</li>

                                                    </ul>
                                                    <p>По любым возникающим вопросам Вы можете получить профессиональную консультацию от наших экспертов с помощью видежета онлан-консультанта.
                                                        Вы также можете  бесплатно заказать обратный звонок и мы свяжемся с Вами в кратчайшие сроки.</p>
                                                </div>
                                                <img class="hidden-xs hidden-sm float-right width40" src="/img/note2.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>				      </div>
                        </div>
                    </div>
                    @foreach($questions as $question)
                        <div class="panel">
                        <div class="panel-heading" role="tab" id="heading23">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#faq" href="#collapse{{ $question->id+1 }}" aria-controls="collapse{{ $question->id+1 }}" class="collapsed" aria-expanded="false">{!! $question->question !!}</a>
                            </h4>
                        </div>
                        <div id="collapse{{ $question->id+1 }}" class="panel-collapse collapse" role="tabpanel" aria-expanded="false">
                            <div class="panel-body">
                                {!! $question->answer !!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @include('blocks.news')
@endsection