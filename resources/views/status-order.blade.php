@extends('layout')
@section('content')
    <div class="container">
        <div class="breadcrumbs">
            <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/" itemprop="url"><span itemprop="title">Главная</span></a>
            </div>
            <div class="active"><span>Статус заказа</span></div>
        </div>
    </div>

    <div class="container" style="font-family:OpenSans-regular;">
        <div class="row">
            <div class="col-lg-6">
                <div class="wrap-status-order">
                    <h3>{{ $order->sender_name }}, Ваш заказ № {{ $order->id }} @if($order->status_id != 4) успешно оформлен! @else успешно отменен @endif</h3>
                    <span class="point @if($order->status == 1) active @endif">Ваш заказ принят и в данный момент обрабатывается. В течении 10 минут мы подтвердим Ваш заказ.</span>
                    
                    <ul class="type-order">
                        <li>
                            Хочу чтобы мне перезвонили для подтверждения заказа
                        </li>
                        {{--<li class="active">Проверьте Ваш заказ, если ошиблись - <a href="" class="btn-edit-order">редактировать</a></li>--}}
                    </ul>

                    <ul class="list-products">
                        @foreach($products as $product)
                            <li>
                                <a href="{{ route('product', ['slug' => $product->slug]) }}">
                                    <strong>{{ $product->name }}</strong> <span>{{ $product->allPrice }}<i class="glyphicon glyphicon-ruble" aria-hidden="true"></i>
    </span></a>
                            </li>
                        @endforeach

                    </ul>

                    <ul class="info-order">
                        <li><span>Доставить:</span> {{ $order->date_send }}</li>
                        <li><span>Получатель:</span> {{ $order->recipient_name }}</li>
                        <li><span>Отправитель:</span>{{ $order->sender_name }}</li>
                        <li><span>Адрес:</span> {{ $order->adress_send }}</li>
                        @if($order->text_otkrytka)
                            <li class="text-otrkytka"><span>Текст открытки:</span> {{ $order->text_otkrytka }}
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M16.8334 9.80176L17.5391 9.09564C18.267 8.36814 18.0787 7.37664 17.3516 6.64914C16.6245 5.92164 15.6326 5.73339 14.9047 6.46164L14.199 7.16776" stroke="#008AC1" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M6.81152 14.5534L9.44627 17.1881M15.6803 10.9545L16.8334 9.80175L14.1983 7.16663L13.0455 8.31938L15.6803 10.9545Z" stroke="#008AC1" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M12 23.25C18.2132 23.25 23.25 18.2132 23.25 12C23.25 5.7868 18.2132 0.75 12 0.75C5.7868 0.75 0.75 5.7868 0.75 12C0.75 18.2132 5.7868 23.25 12 23.25Z" stroke="#008AC1" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M9.44625 17.1881L15.6803 10.9545L13.0455 8.3194L6.8115 14.5534L6 17.9996L9.44625 17.1881V17.1881Z" stroke="#008AC1" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>

                            </li>
                        @endif
                        @if($order->is_photo)
                            <li><span>Сделать фото при получении</span></li>
                        @endif

                        @if($order->is_phone_recipient)
                            <li><span>Не звонить получателю перед доставкой</span></li>
                        @endif

                        @php
                            $bonus = \App\Balance::where('order_id', $order->id)->first();
                        @endphp

                        @if($bonus)
                            <li><span>Начисленных бонусов</span> {{ $bonus->price }} <i class="glyphicon glyphicon-ruble" aria-hidden="true"></i></li>
                        @endif

                        @php
                            $p = $order->allPrice - $order->bonuses;

                            if($p < 0) {
                                $p = 0;
                            }
                        @endphp
                        <li><span>Итого:</span> {{ $p }} <i class="glyphicon glyphicon-ruble" aria-hidden="true"></i>
                            наличными</li>
                    </ul>

                    <span class="point point-mini @if($order->status == 2) active @endif">Курьер в пути</span>
                    <span class="point point-mini @if($order->status == 3) active @endif" style="margin-bottom: 0">Заказ доставлен, оставьте отзыв</span>

                    @if($order->status != 3 && $order->status != 4)
                        <a href="/change-status-order/{{ $order->id }}" class="cancel-order">Отменить заказ</a>
                    @endif

                </div>
            </div>
            <div class="col-lg-5" style="padding-left: 0">
                <div class="wrap-question-order">
                    <ul>
                        <li>
                            Возникли вопросы или трудности? — свяжитесь с нами
                        </li>
                        <li>
                            <a href="#" data-target="#callbackPopup" data-toggle="modal">Обратная <br> связь</a>
                        </li>
                    </ul>

                    <h3>Чтобы Вам было легче нас найти  <br>  в будущем, <a id="bookmarkme" href="javascript:void(0);">добавьте сайт в закладки</a></h3>

                    <p>На Ваш телефон отправлена ссылка для входа в личный кабинет. Все дальнейшие статусы заказа можно отследить там, либо по смс.</p>

                    <svg width="148" height="148" viewBox="0 0 148 148" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M104.062 4.625H43.9375C38.8289 4.625 34.6875 8.76637 34.6875 13.875V134.125C34.6875 139.234 38.8289 143.375 43.9375 143.375H104.062C109.171 143.375 113.312 139.234 113.312 134.125V13.875C113.312 8.76637 109.171 4.625 104.062 4.625Z" fill="white" stroke="#3B4249" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M35 123H113M35 18H113H35Z" stroke="#3B4249" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M74 137.25C76.5543 137.25 78.625 135.179 78.625 132.625C78.625 130.071 76.5543 128 74 128C71.4457 128 69.375 130.071 69.375 132.625C69.375 135.179 71.4457 137.25 74 137.25Z" stroke="#3B4249" stroke-linecap="round" stroke-linejoin="round"/>
                        <g clip-path="url(#clip0)">
                            <path d="M64.672 85.3059C68.5158 88.7213 76.7103 88.8061 82.1592 86.0908C83.7796 85.2646 85.2674 84.1992 86.5733 82.93C81.8002 83.6724 80.0261 84.6271 74.7039 82.93C72.2979 82.0384 70.0883 80.6837 68.199 78.9418C66.1662 77.1644 64.5456 74.9614 63.451 72.4876C62.3564 70.0138 61.8144 67.3293 61.8631 64.6226C58.3361 69.2048 59.3921 79.1964 64.672 85.3059Z" fill="#FF8877"/>
                            <path d="M83.2363 75.8022C77.8507 78.7084 74.4715 78.072 68.4524 77.0962C68.9593 77.4992 69.0226 77.7326 69.6562 78.2205C73.3898 81.127 78.06 82.5471 82.7716 82.2087C89.3188 81.4662 90.9873 79.663 93.3316 74.7839C95.4436 70.5412 95.6759 62.0557 93.3316 59.1919C92.5713 66.5742 90.1847 72.0686 83.2363 75.8022Z" fill="#FF8877"/>
                            <path d="M91.9377 56.9645C91.6415 54.6569 90.5055 52.541 88.7486 51.0247C83.8699 45.8273 79.4559 45 72.8242 45C74.6194 46.3152 81.5678 48.1184 84.5246 56.222C87.6503 64.8347 84.947 68.7592 82.2437 74.3596C86.6366 72.7686 88.5796 68.7168 90.3114 65.8742C91.4617 63.0785 92.0226 60.0737 91.9588 57.0493" fill="#FF8877"/>
                            <path d="M61.0817 48.3941C59.799 49.2311 58.6279 50.2292 57.5969 51.3641C55.424 53.8488 53.9191 56.85 53.2251 60.0829L54.5768 58.9373C56.2559 56.9087 58.3664 55.2839 60.7531 54.1826C63.1397 53.0812 65.7419 52.5313 68.368 52.5732C72.9674 52.5557 77.417 54.2138 80.8921 57.2402L83.8489 60.2313C83.0509 55.9839 80.8118 52.1456 77.5129 49.37C75.8856 48.1234 74.0854 47.1229 72.1696 46.4001C68.0723 44.8515 64.6721 45.9758 61.0817 48.3941Z" fill="#FF8877"/>
                            <path d="M53.4575 61.9709C52.4161 64.1104 51.9174 66.4752 52.0059 68.855C52.0943 71.2348 52.7671 73.5558 53.9643 75.6113C55.5595 79.045 58.19 81.8875 61.483 83.7361C61.124 82.0814 55.6539 75.5264 59.371 65.7469C62.4123 58.0888 66.9108 56.9433 71.3037 54.4825C67.6421 53.9935 63.9187 54.6217 60.6171 56.2857C57.8209 57.5925 55.3687 59.5397 53.4575 61.9709Z" fill="black"/>
                            <path d="M72.1062 55.6069C69.8342 56.7126 67.7063 58.0948 65.7702 59.7223C64.5394 60.3956 63.6023 61.5051 63.1415 62.8346C62.6808 64.1641 62.7295 65.6183 63.2781 66.9137C65.9392 75.2931 65.2634 76.2477 75.0207 76.2901C78.59 76.2901 80.702 75.4628 81.758 72.8747C86.2354 61.9709 83.8699 61.8224 79.4559 58.3646C77.7241 56.9645 75.2319 54.7159 72.0851 55.6069H72.1062ZM73.9859 57.5161C76.9849 56.9221 78.2098 59.0222 79.4981 60.6769C83.7221 66.2349 83.4687 66.8289 79.3503 72.3232C78.1042 73.9778 77.006 75.781 73.9436 75.2719C71.6491 74.7221 69.4138 73.9477 67.2698 72.9596C65.1578 71.3898 65.1578 69.629 65.1578 66.4046C65.1578 63.1801 65.2845 61.1648 67.481 59.7223C69.5849 58.791 71.7603 58.0319 73.9859 57.4524V57.5161Z" fill="#FF8877"/>
                            <path d="M77.9986 66.9773C78.4633 65.4924 78.4844 64.6438 77.1538 63.6256C74.2393 61.3981 73.6902 60.8678 71.1558 62.6922C70.2265 63.371 69.1917 63.8589 68.9382 65.0893C68.9382 65.6196 68.9382 70.6685 72.5075 70.8806C76.9638 71.1564 76.8582 70.5412 77.9986 66.9773Z" fill="#FF8877"/>
                            <path d="M70.1419 59.5525C67.6498 60.5496 68.0299 60.7193 66.9951 63.668C66.3826 65.5984 65.4111 67.6561 66.6572 69.629C67.9438 70.8677 69.3601 71.963 70.8811 72.8959C71.6601 73.565 72.6604 73.9166 73.6848 73.8813C74.7092 73.8459 75.6831 73.4262 76.4145 72.705C80.195 69.7987 82.0324 69.8836 80.0049 63.7316C79.3079 61.6102 78.9278 60.0404 76.9003 59.4889C74.7042 59.0505 72.4436 59.0505 70.2475 59.4889L70.1419 59.5525ZM67.4808 66.6591C67.4808 65.4499 69.804 60.295 72.8453 60.4859C74.3941 60.8152 75.9025 61.3131 77.3438 61.9709C78.6955 62.7558 79.0334 63.9862 79.0334 66.1075C79.0334 69.5229 79.0334 71.22 73.4789 72.2383C69.804 72.811 67.502 67.1046 67.4808 66.6591Z" fill="#FF8877"/>
                        </g>
                        <defs>
                            <clipPath id="clip0">
                                <rect width="43" height="43" fill="white" transform="translate(52 45)"/>
                            </clipPath>
                        </defs>
                    </svg>

                    <p>Установите приложение Express Букет для IOS или Android и заказывайте удобнее и быстрее</p>
                    
                    
                    <div class="wrap-app-mobile">
                        <ul>
                            <li><a href="">
                                    <span>Скачать для IOS</span>
                                    <strong>App Store</strong>
                                </a></li>
                            <li><a href="">
                                    <span>Для Android</span>
                                    <strong>Google play</strong>
                                </a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection