@extends('layout')

@section('content')
    <div class="text-page">
        <div class="container">
            <div class="row">

                <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
                <div class="breadcrumbs">
                    <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="/" itemprop="url"><span itemprop="title">Главная</span></a></div>
                    <div class="active"><span>Оплата</span></div>
                </div>
                @include('blocks.left-sidebar')
                <div class="col-xs-12 col-md-8 content">
                    <h1 class="content-title">{{ $seo ? $seo->h1 : '' }}</h1>
                    <p><span class="payment-subtitle">Банковскими картами</span></p>
                    <div class="payment-method visa" title="Банковские карты Visa и MasterCard">
                        <small>Visa/Mastercard</small>
                    </div>
                    <div class="payment-method sb" title="Сбербанк: оплата по SMS или Сбербанк Онлайн">
                        <small>Карта Сбербанк</small>
                    </div>
                    <div class="payment-method ac" title="Альфа-клик">
                        <small>Альфа-клик</small>
                    </div>
                    <p><span class="payment-subtitle">Наличными</span></p>
                    <div class="payment-method salon" title="Связной и Евросеть">
                        <small>Связной и Евросеть</small>
                    </div>
                    <div class="payment-method courier" title="При получении">
                        <small>При получении</small>
                    </div>
                    <p><span class="payment-subtitle">Электронные платежные системы</span></p>
                    <div class="payment-method phone" title="Оплата с телефона">
                        <small>С телефона</small>
                    </div>
                    <div class="payment-method paypal" title="PayPal">
                        <small>PayPal</small>
                    </div>
                    <div class="payment-method qiwi" title="QIWI-кошелек">
                        <small>Qiwi-кошелек</small>
                    </div>
                    <div class="payment-method yad" title="Яндекс.Деньги">
                        <small>Яндекс.Деньги</small>
                    </div>
                    <div class="payment-method wm" title="WebMoney (WMR)">
                        <small>WebMoney</small>
                    </div>
                    <div class="row order-form">
                        <span class="col-xs-12 content-title">Доплата</span>
                        <form action="https://money.yandex.ru/eshop.xml" method="POST" target="_blank">
                            <input name="shopId" value="94693" type="hidden">
                            <input name="scid" value="86445" type="hidden">
                            <input name="customerNumber" value="" type="hidden">
                            <div class="col-xs-12 col-md-6 col-lg-3">
                                <input name="sum" data-validation="required"
                                       data-validation-error-msg="Обязательное поле" placeholder="Сумма*" type="text"
                                       class="order-input">
                            </div>
                            <div class="col-xs-12 col-md-6 col-lg-3">
                                <input name="orderNumber" placeholder="Номер заказа*" data-validation="required"
                                       data-validation-error-msg="Обязательное поле" type="text" class="order-input">
                            </div>
                            <div class="col-xs-12 col-md-6 col-lg-3">
                                <input name="cps_email" placeholder="E-mail" type="text" class="order-input">
                            </div>
                            <div class="col-xs-12 col-md-6 col-lg-3 text-center">
                                <input type="submit" value="Оплатить" class="white-button" style="min-width: 100%;
    margin: 0;
    height: 39px;
    line-height: 39px;
    padding: 0;">
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
    @include('blocks.news')
@endsection
