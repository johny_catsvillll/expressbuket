@extends('layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="breadcrumbs">
                <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url"><span itemprop="title">Главная</span></a></div>
                <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/page/katalog" itemprop="url"><span itemprop="title">Каталог</span></a></div>
                <div class="active"><span>Акция</span></div>
            </div>
        </div>
    </div>
    <i class="hidden-xs hicon"></i>
    <h1 class="col-xs-12 title">Букет по акции</h1>
    <div class="container category-content">
        <p>Букет по акции – это еще более выгодное предложение, с помощью которого вы сможете по приемлемой цене
            приобрести роскошную цветочную композицию. Мы предлагаем широкий ассортимент живых растений, поставляемых
            напрямую со стран Латинской Америки, Европы и Африки.</p>
    </div>
    @include('blocks.filter')
    <div class="container">
        <div class="catalog">

            <div id="ajaxListView" class="list-view">
                <div class="items">
                    @foreach($products as $product)
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">

                            <div class="product" data-id="{{ $product->id }}">
                                <div class="product-inner">
                                    <span class="product-size">{{ $product->width }} {{ isset($product->height) ? 'x '. $product->height . ' см' : ''}} </span>
                                    @if($product->is_hit)
                                        <span class="product-label product-hit"><i>!</i>хит продаж</span>
                                    @endif

                                    @if($product->is_new)
                                        <span class="product-label product-newest"><i><img src="/img/icons/newest.png" alt="Новинка"></i>новинка</span>
                                    @endif

                                    @if($product->old_price)
                                        <span class="product-label product-sale"><i>%</i>букет со скидкой</span>
                                    @endif

                                    @if($product->old_price)
                                        <span class="product-oldprice line-trough"><span><b>{{ $product->old_price }}</b> <span class="hidden-xs">руб</span></span></span>
                                    @endif

                                    <a href="{{ route('product', ['slug' => $product->slug]) }}" title="{{ $product->name }}">
                                        <img class="product-img img-responsive"
                                             src="{{ url('storage/'.$product->main_img) }}" alt="{{ $product->name }} - Expressbuket" title="{{ $product->name }} - Expressbuket">
                                        <div class="catalog-consist">
                                            <div class="catalog-consist-inner" style="display: none;">
                                                {!! str_limit(strip_tags($product->composition), 190, '...') !!}
                                            </div>
                                        </div>
                                    </a>
                                    <div class="text-center product-title ">
                                        <a href="{{ route('product', ['slug' => $product->slug]) }}" title="{{ $product->name }}">{{ $product->name }}</a>
                                    </div>
                                    <div class="text-center product-price">
                                        @if($product->price)
                                            <b>{{ $product->price }}</b>
                                        @elseif(json_decode($product->construktor_sting))
                                            <b>{{ getHelperPrice($product->construktor_sting) }}</b>
                                        @endif
                                        <span class="glyphicon glyphicon-ruble" aria-hidden="true"></span>
                                        <a href="javascript:void(0);" class="btn-add-to-favorite" title="Добавить в избранное">
                                            <svg width="32" height="28" viewBox="0 0 32 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path @if(!(session('favorite') && isset(session('favorite')[$product->id]))) class="active-favorite" @endif d="M23.5 0.5C20.25 0.5 17.05 2.6 16 5.5C14.95 2.6 11.75 0.5 8.5 0.5C6.51088 0.5 4.60322 1.29018 3.1967 2.6967C1.79018 4.10322 1 6.01088 1 8C1 14.5 6.5 21 16 27.5C25.5 21 31 14.5 31 8C31 6.01088 30.2098 4.10322 28.8033 2.6967C27.3968 1.29018 25.4891 0.5 23.5 0.5Z" fill="#FF8877" stroke="#FF8877" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            </svg>

                                        </a>
                                    </div>
                                </div>
                                <div class="catalog-buttons">
                                    <div class="text-center">
                                        <a id="64" class="col-xs-11 white-button"
                                           href="{{ route('product', ['slug' => $product->slug]) }}">Заказать <i class="why-we-arr"></i></a>
                                    </div>
                                    <div class="text-center">
                                        <a id="b{{ $product->id }}" class="one-click quick_btn">Купить в 1 клик</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="container category-content">
        <h2>Еще больше выгоды</h2>

        <p>Наше акционное предложение – отличное решение для тех, кто желает порадовать близкого человека
            оригинальным подарком. К вашему вниманию – великолепные букеты, собранные из свежих, ярких и
            благоухающих цветов.</p>

        <p>Преимущества предложения очевидны:</p>

        <ul>
            <li>цена – одна из самых низких на профильном рынке;</li>
            <li>доставка – по городу Волгоград, а также всей Волгоградской области, включая самые отдаленные
                районы;
            </li>
            <li>высокий уровень сервиса;</li>
            <li>впечатляющий выбор на любой вкус.</li>
        </ul>

        <p>Акция не значит, что мы пытаемся реализовать цветы не первой свежести. Это означает, что наша компания
            хочет, чтобы как можно большее количество граждан приобщились к прекрасному. Регулярно посещайте сайт
            интернет-магазина Express Букет, чтобы быть в курсе наиболее выгодных акционных предложений, скидок и
            бонусов.</p>

        <p>Букет по акции от нашей компании будет гарантированно доставлен в максимально сжатые сроки, причем в
            любое время суток.</p></div>
    @include('blocks.news')
@endsection