<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div id="wrapper">
        <ul>
            <li>Информация о статусе заказа: <a href="{{ url('/status-order?order='.$order->id) }}">Посмотреть</a></li>
            <li>Номер заказа: {{ $order->id }}</li>
            <li>Цена без дополнений: {{ $order->price }}</li>
            <li>Цена: {{ $order->allPrice }}</li>
            <li>Добавить стойкость букету: {{ $order->is_stoik ? 'Да' : 'Нет' }}</li>
            <li>Добавить красивую упаковку: {{ $order->is_upak ? 'Да' : 'Нет' }}</li>
            <li>Добавить зелени: {{ $order->is_green ? 'Да' : 'Нет' }}</li>
            <li>Адрес доставки: {{ $order->adress_send }}</li>
            <li>Дата доставки: {{ $order->date_send }}</li>
            <li>Имя отправителя: {{ $order->sender_name }}</li>
            <li>Почта отправителя: {{ $order->sender_email }}</li>
            <li>Телефон отправителя: {{ $order->sender_phone }}</li>
            <li>Имя получателя: {{ $order->recipient_name }}</li>
            <li>Телефон получателя: {{ $order->recipient_phone }}</li>
        </ul>


        @php

            $products = [];

            foreach (json_decode($order->products) as $key => $pr) {
                $product = \App\Product::find($pr->productId);
                if(!$product) {
                    continue;
                }
                $price = 0;

                if(isset($pr->extra) && $pr->extra == 'extra') {
                        $price += $product->extra_price * $pr->count;
                }
                else {
                    if(json_decode($product->construktor_sting)) {
                        $price += getHelperPrice($product->construktor_sting);
                    }
                    else {
                        $price += $product->price * $pr->count;
                    }

                }

                $product->allPrice = $price;
                $product->count = $pr->count;
                array_push($products, $product);
            }



        @endphp

        <ul style="width:450px;" class="list-products">
            @foreach($products as $product)
                <li>
                    <a href="{{ route('product', ['slug' => $product->slug]) }}">
                        <strong>{{ $product->name }}</strong> <span>{{ $product->allPrice }}
                            <i class="glyphicon glyphicon-ruble" aria-hidden="true"></i></span></a>
                </li>
            @endforeach

        </ul>


    </div>
</body>
</html>