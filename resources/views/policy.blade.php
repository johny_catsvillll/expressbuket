@extends('layout')

@section('content')
    <div class="container">
        <div class="row news">
            <div class="breadcrumbs">
                <div class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url"><span itemprop="title">Главная</span></a></div>
                <div class="active"><span>Политика конфиденциальности</span></div>
            </div>
            <i class="hidden-xs hicon"></i>
            <h1 class="col-xs-12 title">{{ $seo ? $seo->h1 : '' }}</h1>
            @if($seo ? $seo->sidebar : '')
                @include('blocks.left-sidebar')
            @endif
            <div class="@if($seo->sidebar )col-xs-12 col-md-8 @endif content">
                {!! $seo ? $seo->top_text : ''  !!}
            </div>
        </div>
    </div>
    @include('blocks.news')
@endsection