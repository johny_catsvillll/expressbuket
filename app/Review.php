<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use LaravelAdminPanel\Traits\Cropper;
use Illuminate\Database\Eloquent\Builder;


class Review extends Model
{

    use Cropper;

    public function order() {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function cityId() {
        return $this->belongsTo(City::class, 'city_id');
    }

    protected static function boot()
    {
        parent::boot();

        $uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
        if ($uri && strpos($uri, 'admin') === false || isset(request()->city_id)) {
            static::addGlobalScope('domain', function (Builder $builder) {

                if (request()->city_id) {
                    $builder->where('city_id', '=', request()->city_id);

                } else {
                    $builder->where('city_id', '=', get_subdomain_id());
                }
            });
        }
    }
}
