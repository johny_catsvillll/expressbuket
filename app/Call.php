<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Call extends Model
{
    public function cityId() {
        return $this->belongsTo(City::class, 'city_id');
    }
}
