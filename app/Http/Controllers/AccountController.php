<?php

namespace App\Http\Controllers;

use App\Account;
use App\Balance;
use App\Order;
use App\Rule;
use Illuminate\Http\Request;
use Mail;

class AccountController extends Controller
{
    public function home() {

        $orders = Order::where('account_id', auth('account')->user()->id)
            ->orderBy('created_at', 'desc')
            ->get();
        return view('account.home', compact('orders'));
    }

    public function bonus(Request $request) {

        $bonuses = Balance::where('account_id', auth('account')->user()->id);

        if($request->types == 3) {
            $bonuses->where('types', 3);
        }
        else {
            $bonuses->where('types', 2);
        }


        $bonuses = $bonuses->get();
        return view('account.bonus', compact('bonuses'));
    }

    public function rules() {

        $rules = Rule::orderBy('order')->get();

        return view('account.rules', compact('rules'));
    }

    public function info(Request $request) {

        if($request->isMethod('post')) {

            $account = auth('account')->user();
            $account->phone = $request->phone;
            $account->email = $request->email;
            $account->name = $request->name;
            $account->description = $request->description;
            $account->sex = $request->sex;

            if($request->password && $request->repeat_password && $request->password == $request->repeat_password) {
                $account->password = bcrypt($request->password);
            }

            $account->save();
        }


        return view('account.info');
    }

    public function notifications(Request $request) {

        if($request->isMethod('post')) {
            $account = Account::find(auth('account')->user()->id);
            $account->is_sms = $request->sms;
            $account->is_email = $request->email;
            $account->save();

            return redirect()->back();
        }

        return view('account.notifications');
    }

    public function removeOrder(Request $request, $orderId) {
        $order = Order::where('id', $orderId)->where('account_id', auth('account')->user()->id)->firstOrFail();

        $order->delete();

        return redirect()->back();

    }

    public function repeatOrder(Request $request, $orderId) {
        $order = Order::findOrFail($orderId);

        if($order->account_id != auth('account')->user()->id) {
            abort(404);
        }

        $account = auth('account')->user();

        $newOrder = $order->replicate();
        $newOrder->save();


        Mail::send('mail.order', ['order' => $newOrder], function($message) use($account, $newOrder)
        {
            $message->from('test@test.com');
            $message->to($account->email)->subject('Заказ №'. $newOrder->id);
        });

        Mail::send('mail.order', ['order' => $newOrder], function($message) use($account, $newOrder)
        {
            $message->from('test@test.com');
            $message->to('admin@admin.com')->subject('Заказ №'. $newOrder->id);
        });

        return redirect()->back()->with('message', 'success');


    }



}
