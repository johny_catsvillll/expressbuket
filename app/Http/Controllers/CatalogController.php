<?php

namespace App\Http\Controllers;

use App\FilterComposition;
use App\FilterFlower;
use App\FilterStyle;
use App\FilterWhom;
use App\News;
use App\Product;
use App\Review;
use Illuminate\Http\Request;
use LaravelAdminPanel\Models\Category;

class CatalogController extends Controller
{
    public function catalog(Request $request, $categorySlug = false, $subcategorySlug = false) {
        $news = News::take(3)->get();

        $products = Product::orderBy('order')->where('publication', 1);

        if($categorySlug && !$subcategorySlug) {
            $category = Category::where('slug', $categorySlug)->firstOrFail();
            $products = $products->where('category_id', $category->id);
        }

        if($subcategorySlug) {
            $category = Category::where('slug', $subcategorySlug)->firstOrFail();
            $products = $products->whereHas('categoryId', function($query) use($category) {
                $query->where('category_id', $category->id);
            });
        }

        $data = $request->all();

        $priceText = false;
        $flowerText = false;
        $whomText = false;
        $factText = false;
        $styleText = false;
        $specText = false;
        $styleName = false;
        $whomName = false;
        $flowerName = false;
        $priceName = false;
        $factName = false;
        $specName = false;


        if($data) {
            foreach ($data as $key => $d) {
                switch ($key) {
                    case 'style':
                        if($d) {
                            $products->whereHas('filterStyle', function($query) use($d) {
                                $query->where('style_id', $d);
                            });

                            $styleName = FilterStyle::where('id', $d)->first()->name;
                            $styleText = 'Стиль: '. $styleName;
                        }



                        break;
                    case 'whom':
                        if($d) {
                            $products->whereHas('filterWhom', function ($query) use($d) {
                                $query->where('whom_id', $d);
                            });

                            $whomName = FilterWhom::where('id', $d)->first()->name;
                            $whomText = 'Кому: '.$whomName;
                        }


                        break;
                    case 'inc':
                        if($d) {
                            $products->whereHas('filterFlowers', function ($query) use($d) {
                                $query->where('flower_id', $d);
                            });

                            $flowerName = FilterFlower::where('id', $d)->first()->name;
                            $flowerText = 'Цветы: '.$flowerName;
                        }


                        break;
                    case 'price':

                        switch ($d) {
                            case 1:
                                $products->where('price', '<', 1500);
                                $priceName = 'до 1500';
                                $priceText = 'Цена: '.$priceName;
                                break;
                            case 1500:
                                $products->where('price', '>', 1500)->where('price', '<', 3000);
                                $priceName = '1500-3000';
                                $priceText = 'Цена: '.$priceName;
                                break;
                            case 3000:
                                $products->where('price', '>', 3000)->where('price', '<', 5000);
                                $priceName = '3000-5000';
                                $priceText = 'Цена: '.$priceName;
                                break;
                            case 5000:
                                $products->where('price', '>', 5000);
                                $priceName = 'от 5000';
                                $priceText = 'Цена: '.$priceName;
                                break;
                            case 'desc':
                                $products->orderBy('price', 'desc');
                                $priceName = 'по убыванию';
                                $priceText = 'Цена: '.$priceName;
                                break;
                            case 'asc':
                                $products->orderBy('price', 'asc');
                                $priceName = 'по возрастанию';
                                $priceText = 'Цена: '.$priceName;
                                break;
                        }
                        break;
                    case 'fact':
                       if($d) {
                           $products->whereHas('filterType', function($query) use($d) {
                               $query->where('type_id', $d);
                           });

                           $factName = FilterComposition::where('id', $d)->first()->name;
                           $factText = 'По случаю: '. $factName;
                       }




                        break;
                    case 'spec':

                        switch ($d) {
                            case 1:
                                $products->whereNotNull('old_price');
                                $specName = 'Со скидкой';
                                $specText = 'Спецпредложения: '. $specName;
                                break;
                            case 2:
                                $products->where('is_hit', 1);
                                $specName = 'Хит';
                                $specText = 'Спецпредложения: '. $specName;
                                break;
                            case 3:
                                $products->where('is_new', 1);
                                $specName = 'Новинка';
                                $specText = 'Спецпредложения: '. $specName;
                                break;

                        }

                        break;
                }
            }
        }

        $products = $products->paginate(12);

        return view('catalog', compact('news', 'products', 'category', 'specText', 'styleText', 'factText', 'priceText', 'flowerText',
            'whomText', 'styleName', 'factName', 'specName', 'priceName', 'whomName', 'flowerName'));
    }

    public function product(Request $request, $slug) {
        $news = News::take(3)->get();
        $product = Product::where('slug', $slug)->firstOrFail();
        $reviews = Review::with('order')->take(6)->get();

        $products = [];
        if(session('see-later')) {
            $products = session('see-later');

            if(!in_array($product->id ,$products)) {
                array_push($products, $product->id);
            }
        }
        else {
            array_push($products, $product->id);
        }

        session(['see-later' => $products]);
        $productId = $product->id;

        $otherProducts = Product::where('id', '!=', $product->id)->limit(6)->get();

        return view('product', compact('news', 'product', 'otherProducts', 'reviews', 'productId'));
    }


    public function cart(Request $request) {
        $news = News::take(3)->get();

        if($request->ajax()) {
            $data = $request->all();

            if($data['type'] == 'delete') {
                $sessionCart = session('basket-products');
                $marker = false;
                foreach ($sessionCart as $key => $c) {
                    if($c['productId'] == $data['productId']) {

                        foreach ($c as $key2 => $currentProduct) {
                            if($key2 == 'count') {
                                continue;
                            }
                            if($currentProduct == $request->{$key2}) {
                                $marker = true;
                            }
                            else {
                                $marker = false;
                                break;
                            }
                        }

                        if($marker) {
                            unset($sessionCart[$key]);
                            break;
                        }
                    }
                }


                session()->put('basket-products', $sessionCart);
            }
            else {
                $sessionCart = session('basket-products');

                $marker = false;
                foreach ($sessionCart as $key => $c) {

                    if($c['productId'] == $data['productId']) {

                        foreach ($c as $key2 => $currentProduct) {
                            if($key2 == 'count') {
                                continue;
                            }
                            if($currentProduct == $request->{$key2}) {
                                $marker = true;
                            }
                            else {
                                $marker = false;
                                break;
                            }
                        }

                        if($marker) {
                            $sessionCart[$key]['count'] = $data['count'];
                            session()->put('basket-products', $sessionCart);
                        }
                    }
                }

            }

            $products = getProducts()['products'];
            $allPrice = getProducts()['allPrice'];
            $standartPrice = $allPrice;


            $priceWithPluses = $allPrice;

            $countProducts = getProducts()['countProducts'];
            if($data['stoik'] == 'true') {
                $priceWithPluses += 50;
            }

            if($data['upak'] == 'true') {
                $priceWithPluses += 150;
            }

            if($data['green'] == 'true') {
                $priceWithPluses += 200;
            }


            $view = view('blocks.cart', ['products' => $products, 'stoik' => $data['stoik'], 'upak' => $data['upak'], 'green' => $data['green']])->render();
            $viewEmpty = view('blocks.cart-empty')->render();

            return ['view' => $view, 'allPrice' => $allPrice, 'priceWithPluses' => $priceWithPluses,
                'viewEmpty' => $viewEmpty, 'countProducts' => $countProducts, 'standartPrice' => $standartPrice];

        }

        $products = getProducts()['products'];
        $allPrice = getProducts()['allPrice'];


        $presents = Product::whereHas('relationCategory', function($query) {
            $query->where('category_id', 6);
        })->limit(6)->get();

        return view('cart', compact('news', 'products', 'allPrice', 'presents'));
    }

}
