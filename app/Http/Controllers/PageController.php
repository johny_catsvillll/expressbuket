<?php

namespace App\Http\Controllers;

use App\Advantage;
use App\Article;
use App\Call;
use App\Galleryphoto;
use App\Magazine;
use App\News;
use App\Order;
use App\Product;
use App\QuestionAnswer;
use App\Review;
use App\Slider;
use App\TopPage;
use Illuminate\Http\Request;
use LaravelAdminPanel\Models\Category;
use Validator;
use Storage;

class PageController extends Controller
{
    public function home() {

        $news = News::take(3)->get();
        $reviews = Review::with('order')->take(6)->get();
        $photosDelivery = Galleryphoto::take(10)->where('type', 1)->get();
        $photosInstagram = Galleryphoto::take(10)->where('type', 0)->get();
        $products = Product::where('publication', 1)->orderBy('is_show_home', 1)->where('is_show_home', 1)->orderBy('order')->limit(16)->get();
        $slider = Slider::orderBy('order')->get();
        $advantages = Advantage::orderBy('order')->limit(4)->get();


        return view('home', compact('news', 'reviews', 'photos', 'products', 'slider', 'advantages', 'photosDelivery', 'photosInstagram'));
    }

    public function aboutCompany() {
        return view('about');
    }

    public function news() {
        $news = News::orderBy('created_at', 'desc')->paginate(8);
        return view('news', compact('news'));
    }

    public function oneNew($slug) {

        $news = News::take(3)->get();
        $article = News::where('slug', $slug)->firstOrFail();

        return view('article', compact('article', 'news'));
    }

    public function dostavka() {
        $news = News::take(3)->get();

        return view('dostavka', compact('news'));
    }

    public function oplata() {
        $news = News::take(3)->get();
        return view('oplata', compact('news'));
    }

    public function articles() {
        $articles = Article::orderBy('created_at', 'desc')->paginate(6);
        $news = News::take(3)->get();

        return view('articles', compact('articles', 'news'));
    }

    public function article($slug) {

        $news = News::take(3)->get();
        $article = Article::where('slug', $slug)->firstOrFail();
        return view('article', compact('news', 'article'));
    }

    public function reviews(Request $request) {


        $message = false;

        if($request->post()) {
            $data = $request->all();

            $validator = Validator::make($data, [
                'name' => 'required',
                'order_id' => 'required',
                'text' => 'required',
                //'g-recaptcha-response' => 'required'
            ]);

            if($validator->fails()) {
                $message = "Ошибка добавления отзыва";
            }
            else {

                $review = new Review;
                $review->name = $data['name'];
                $review->order_id = $data['order_id'];
                $review->text = $data['text'];
                $review->save();

                $message = 'Отзыв успешно добавлен!';
            }
        }

        $news = News::take(3)->get();
        $reviews = Review::with('order')
            ->where('publication', 1)
            ->paginate(12);

        return view('reviews', compact('news', 'reviews', 'message'));
    }

    public function contacts() {
        $news = News::take(3)->get();
        $magazines = Magazine::all();
        return view('contacts', compact('news', 'magazines'));
    }

    public function policy() {
        $news = News::take(3)->get();
        return view('policy', compact('news'));
    }

    public function faq() {
        $news = News::take(3)->get();
        $questions = QuestionAnswer::orderBy('order')->get();
        return view('faq', compact('news', 'questions'));
    }

    public function gallery() {
        $news = News::take(3)->get();
        $photos = Galleryphoto::where('type', 1)->paginate(9);
        return view('gallery', compact('news', 'photos'));
    }

    public function pluses() {
        $news = News::take(3)->get();
        $advantages = Advantage::orderBy('order')->get();
        return view('pluses', compact('news', 'advantages'));
    }

    public function discounts() {

        $products = Product::orderBy('created_at')->whereNotNull('old_price')->where('publication', 1)->get();
        $news = News::take(3)->get();
        return view('product-discount', compact('products', 'news'));

    }

    public function search(Request $request) {
        $products = Product::orderBy('created_at');

        if($request->search) {
            $products->where('name', 'LIKE', '%'. $request->search .'%');
        }

        $products = $products->where('publication', 1)->get();
        $news = News::take(3)->get();
        return view('search', compact('news', 'products'));
    }

    public function sitemap() {
        $categoriesMap = Category::with('children')->whereNull('parent_id')->get();
        $articles = Article::all();
        $news = News::all();
        return view('sitemap', compact('articles', 'categoriesMap', 'news'));
    }

    public function favorite() {

        $products = [];
        if(session('favorite')) {
            foreach (session('favorite') as $key => $favorite) {

                $product = Product::find($key);

                array_push($products, $product);
            }
        }


        return view('favorite', compact('products'));
    }

    public function addToFavorite(Request $request) {

        $productId = $request->productId;
        $card = session()->get('favorite');
        $marker = 1;

        if ($productId) {
            if (is_array($card)) {
                if (isset($card[$productId])) {
                    unset($card[$productId]);
                    $marker = 0;
                } else {
                    $card[$productId] = 1;
                }
            } else {
                $card = [
                    $productId => 1
                ];
            }
            session()->put('favorite', $card);
        }

        return ['count' => count(session('favorite')), 'marker' => $marker];

    }

    public function clearListFavorite() {
        session()->forget('favorite');
    }

    public function saveApplication(Request $request) {
        $application = new Call;
        $application->name = $request->name;
        $application->phone = $request->phone;
        $application->text = $request->text;

        if($request->hasFile('image')) {
            $file = $request->file('image');
            $file->move(public_path() . '/calls/', $file->getClientOriginalName());
            $application->file = '/calls/'.$file->getClientOriginalName();
            Storage::move('/'.$application->file, '/storage/calls/'.$file->getClientOriginalName());
        }

        $application->city_id = get_subdomain_id();
        $application->save();

        return 1;
    }

}
