<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    public function login() {

    }

    public function logout() {
        auth('account')->logout();
        return redirect('/');
    }

    public function checkIsUser(Request $request) {

        if (Auth::guard('account')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return 1;
        }
        else {
            return 0;
        }

    }
}
