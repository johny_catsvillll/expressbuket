<?php

namespace App\Http\Controllers;

use App\Account;
use App\Balance;
use App\News;
use App\Order;
use App\Product;
use App\Promocode;
use App\PromocodeAccount;
use Illuminate\Http\Request;
use Mail;

class OrderController extends Controller
{

    public function addToCart(Request $request)
    {
        $productId = $request->productId;
        $card = session()->get('basket-products');

        $miniArray = [
            'productId' => $productId,
            'extra' => $request->extra,
            'color' => $request->color,
            'count' => $request->count
        ];

        if ($productId) {
            if (!is_array($card) || !count($card)) {
                $card = [];
                array_push($card, $miniArray);
            }
            else {
                $marker = false;
                foreach ($card as $key => $c) {
                    if($c['productId'] == $request->productId) {

                        foreach ($c as $key2 => $currentProduct) {
                            if($key2 == 'count') {
                                continue;
                            }
                            if($currentProduct == $request->{$key2}) {
                                $marker = true;
                            }
                            else {
                                $marker = false;
                                break;
                            }
                        }
                    }

                    if(!$marker) {

                        array_push($card, $miniArray);
                    }
                    else {
                        $card[$key]['count'] += $request->count;
                    }

                }

            }
            session()->put('basket-products', $card);
        }
    }

    public function order(Request $request) {
        $news = News::take(3)->get();
        $products = getProducts()['products'];
        return view('order', compact('products', 'news'));
    }

    public function order2(Request $request) {
        $data = $request->all();

        if($request->isMethod('post')) {
            $order = new Order;
            $order->products = json_encode(session('basket-products'));

            if(!session('basket-products')) {
                abort(404);
            }

            $allPrice = getProducts()['allPrice'];

            $order->price = $allPrice;

            if(isset($data['stoik']) && $data['stoik']) {
                $allPrice += 50;
                $order->is_stoik = 1;
            }

            if(isset($data['upak']) && $data['upak']) {
                $allPrice += 150;
                $order->is_upak = 1;
            }

            if(isset($data['green']) && $data['green']) {
                $allPrice += 200;
                $order->is_green = 1;
            }

            if($allPrice < 500) {
                $allPrice += 300;
            }

            $order->allPrice = $allPrice;

            $order->allPrice += $data['zone_price'];



            if(isset($data['otkrytka']) && $data['otkrytka']) {
                $order->is_otkrytka = 1;
            }

            if(isset($data['otkrytkatext'])) {
                $order->text_otkrytka = $data['otkrytkatext'];
            }

            if(isset($data['photo']) && $data['photo']) {
                $order->is_photo = 1;
            }


            $order->sender_phone = $data['sender_phone'];
            $order->status = 1;
            $order->sender_email = $data['sender_mail'];
            $order->sender_name = $data['sender_name'];
            $order->recipient_name = $data['receiver_name'];
            $order->recipient_phone = $data['receiver_phone'];
            $order->is_phone_recipient = $data['target_time'];
            $order->city_id = get_subdomain_id();

            $dateSelect = $data['date'];
            $timeSelect = isset($data['time']) ? $data['time'] : '';
            $order->date_send = $dateSelect .' - '. $timeSelect;
            $order->adress_send = $data['address'];
            $order->comment = $data['comment'];



            if(isset($data['promocode'])) {

                $promo = Promocode::where('active', 1)
                    ->where('end_date', '>', now())
                    ->where('code', $request->promocode)
                    ->where('allSum', '<', $order->allPrice)
                    ->first();

                if($promo) {
                    $order->promocode = $promo->id;
                    $order->allPrice = $order->allPrice - $promo->price;
                }

            }

            if(isset($data['call_before_order']) && $data['call_before_order']) {
                $order->is_call_agree = 1;
            }

            if(isset($data['question_address']) && $data['question_address']) {
                $order->is_address_recipient = 1;
            }

            $account = Account::where('email', $data['sender_mail'])->first();


            if(auth('account')->check()) {
                $order->account_id = auth('account')->user()->id;


                $bonus = $request->isBonus;

                if($bonus) {
                    $userBalance = auth('account')->user()->balance->sum('price');

                    if($order->allPrice >= $userBalance) {
                        //$order->allPrice -= $userBalance;
                        $writeOff = new Balance;
                        $writeOff->account_id = $order->account_id;
                        $writeOff->types = 3; // списание
                        $writeOff->price = -$userBalance;
                        $order->bonuses = $userBalance;
                    }
                    else {
                        $writeOff = new Balance;
                        $writeOff->account_id = $order->account_id;
                        $writeOff->types = 3; // списание
                        $writeOff->price = -$order->allPrice;
                        //$order->allPrice = 0;
                        $order->bonuses = $order->allPrice;
                    }

                }


            }
            elseif($account) {
                $order->account_id = $account->id;

                auth('account')->loginUsingId($account->id);


                $bonus = $request->isBonus;

                if($bonus) {
                    $userBalance = auth('account')->user()->balance->sum('price');

                    if($order->allPrice >= $userBalance) {
                        $order->allPrice -= $userBalance;
                        $writeOff = new Balance;
                        $writeOff->account_id = $order->account_id;
                        $writeOff->types = 3; // списание
                        $writeOff->price = -$userBalance;
                    }
                    else {
                        $writeOff = new Balance;
                        $writeOff->account_id = $order->account_id;
                        $writeOff->types = 3; // списание
                        $writeOff->price = -$order->allPrice;
                        $order->allPrice = 0;
                    }

                }


            }
            else {
                $account = new Account;
                $account->name = $order->sender_name;
                $account->phone = $order->sender_phone;
                $account->email = $order->sender_email;
                $password = str_random(8);
                $account->password = bcrypt($password);
                $account->save();
                $order->account_id = $account->id;


                $promocode = new Promocode;
                $promocode->code = str_random(8);
                $promocode->account_id = $account->id;
                $promocode->price = 500;
                $promocode->end_date = now()->addYears(5);
                $promocode->allSum = 1000;
                $promocode->save();



                if(isset($data['promo'])) {

                    $promo = Promocode::where('active', 1)
                        ->where('end_date', '>', now())
                        ->where('code', $request->promocode)
                        ->where('allSum', '<', $request->sum)
                        ->first();

                    if($promo) {
                        $order->promocode = $promo->id;



                    }

                }

                auth('account')->loginUsingId($account->id);

                Mail::send('mail.registration', ['email' => $account->email, 'password' => $password], function($message) use($account)
                {
                    $message->from('test@test.com');
                    $message->to($account->email)->subject('Регистрация');
                });

            }

            $order->save();



            if(isset($writeOff)) {
                $writeOff->order_id = $order->id;
                $writeOff->save();
            }


            session()->forget('basket-products');

            Mail::send('mail.order', ['order' => $order], function($message) use($account, $order)
            {
                $message->from('test@test.com');
                $message->to($account->email)->subject('Заказ №'. $order->id);
            });

            Mail::send('mail.order', ['order' => $order], function($message) use($account, $order)
            {
                $message->from('test@test.com');
                $message->to('admin@admin.com')->subject('Заказ №'. $order->id);
            });

            return redirect('/order2?order_id='.$order->id);
        }

        else {
            $order = Order::findOrFail($request->order_id);
        }


        return view('order2', compact('order'));
    }

    public function success(Request $request) {
        return view('order-success');
    }

    public function quickOrder(Request $request) {

        $products = json_encode([$request->bid => 1]);

        $product = Product::find($request->bid);

        $color = '';

        if($product->is_color) {
            $color = 'красный';
        }

        $count = 1;

        if($product->is_single_item) {
            $count = 7;
        }


        $miniArray = [
            'productId' => $request->bid,
            'extra' => '',
            'color' => $count,
            'count' => 1
        ];


        $arr = [];
        array_push($arr, $miniArray);
        $price = 0;

        if($product->price) {
            $price = $product->price;

            if($product->is_single_item) {
                $price = $product->price * 7;
            }
        }
        else {
            $price = getHelperPrice($product->construktor_sting);
        }


        $order = new Order;
        $order->sender_phone = $request->phone;
        $order->sender_name = $request->name;
        $order->products = json_encode($arr);
        $order->city_id = get_subdomain_id();
        $order->price = $price;
        $order->save();

    }

    public function findpromo(Request $request) {

        if($request->ajax()) {
            $promo = Promocode::where('active', 1)
                ->where('end_date', '>', now())
                ->where('code', $request->promocode)
                ->where('allSum', '<', $request->sum)
                ->first();

            if(auth('account')->check() && $promo) {
                $isUses = PromocodeAccount::where('promocode_id', $promo->id)->where('account_id', auth('account')->user()->id)->first();

                if($isUses) {
                    return 'промокод уже был использован Вами';
                }

            }


            if($promo) {
                return $promo->price;
            }

        }

    }

    public function checkOrder(Request $request) {

    }

    public function statusOrder(Request $request) {

        if(!auth('account')->check()) {
            abort(404);
        }
//
        $order = Order::findOrFail($request->order);
//
        if($order->account_id != auth('account')->user()->id) {
            abort(404);
        }

        $products = [];
        $allPrice = 0;
        $count = 0;

        foreach (json_decode($order->products) as $key => $pr) {
            $product = \App\Product::find($pr->productId);
            if(!$product) {
                continue;
            }
            $price = 0;

            if(isset($pr->extra) && $pr->extra == 'extra') {
                    $price += $product->extra_price * $pr->count;
            }
            else {
                if(json_decode($product->construktor_sting)) {
                    $price += getHelperPrice($product->construktor_sting);
                }
                else {
                    $price += $product->price * $pr->count;
                }
            }




            $product->allPrice = $price;
            $product->count = $pr->count;
            array_push($products, $product);
            $allPrice += $price;
            $count += $pr->count;
        }


        return view('status-order', compact('order', 'allPrice', 'products'));
    }

    public function checkNumberOrder(Request $request) {

        $orderId = $request->numberOrder;

        if(!$orderId) {
            return 0;
        }
        else {
            $order = Order::find($orderId);

            if(!$order) {
                return 0;
            }
            return 1;

        }

    }

    public function changeStatusOrder(Request $request, $id) {

        $user = auth('account')->user();

        if(!$user) {
            abort(404);
        }

        $order = Order::where('id', $id)->where('account_id', $user->id)->firstOrFail();

        $order->status_id = 4;
        $order->save();


        return redirect()->back();


    }


}
