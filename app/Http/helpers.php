<?php

function getFilterPrice() {
    return \App\FilterPrice::all();
}

function getLink($type, $value) {
    $data = request()->all();
    if(isset($data[$type])) {
        $data[$type] = $value;
    }

    else {
        $data = array($type => $value) + $data;
    }

    $url = http_build_query($data);
    return '/page/katalog?'. $url;

}

function getCountBasket() {

    $count = 0;

    if(is_array(session('basket-products'))) {
        foreach (session('basket-products') as $basket) {
            $count += 1;
        }
    }

    return $count;
}

function getDomain() {
//    return Route::getCurrentRoute()->parameters['domain'];
    return '';
}

function get_subdomain() {

    if(!isset($_SERVER['HTTP_HOST'])) {
        return '';
    }

    if(env('APP_URL2') == $_SERVER['HTTP_HOST']) {
        return 'volgograd';
    }

    return str_replace('.'.env('APP_URL2'), '', $_SERVER['HTTP_HOST']);
}


function get_subdomain_id() {
    $domain = get_subdomain();
    $cityId = \App\City::where('slug', $domain)->firstOrFail();
    return $cityId->id;
}

function get_subdomain_city() {
    $domain = get_subdomain();
    $cityId = \App\City::where('slug', $domain)->firstOrFail();
    return $cityId->name;
}

function getPriceBasket() {
    $price = 0;

    if(is_array(session('basket-products'))) {
        foreach (session('basket-products') as $key => $basket) {
            $product = \App\Product::find($basket['productId']);

            if($product->price) {
                if(isset($basket['extra']) && $basket['extra'] == 'extra') {
                    if($product->extra_old_price) {
                        $price += $product->extra_old_price * $basket['count'];
                    }
                    else {
                        $price += $product->extra_price * $basket['count'];
                    }
                }
                else {
                    $price += $product->price * $basket['count'];
                }
            }
            elseif($basket['extra'] == 'extra' && $product->extra_price) {
                $price += $product->extra_price * $basket['count'];
            }

            else {
                $price += getHelperPrice($product->construktor_sting) * $basket['count'];
            }

        }
    }

    return $price;
}

function getProducts() {

    $products = [];

    $allPrice = 0;
    $count = 0;

    if(is_array(session('basket-products'))) {

        foreach (session('basket-products') as $key => $basket) {
            $product = \App\Product::find($basket['productId']);
            $product->color = $basket['color'];
            $price = 0;

            if($product->price) {
                if(isset($basket['extra']) && $basket['extra'] == 'extra') {
                    $product->extra = 'extra';
                    $price += $product->extra_price * $basket['count'];

                }
                else {
                    $price += $product->price * $basket['count'];
                }
            }
            elseif($basket['extra'] == 'extra' && $product->extra_price) {
                $price += $product->extra_price * $basket['count'];
                $product->extra = 'extra';
            }
            else {

                $price = getHelperPrice($product->construktor_sting) * $basket['count'];

            }


            $product->allPrice = $price;
            $product->count = $basket['count'];
            array_push($products, $product);
            $allPrice += $price;
            $count += $basket['count'];
        }
    }

    return ['products' => $products, 'allPrice' => $allPrice, 'countProducts' => $count];

}

function getHelperPrice($construktor) {

    $products = json_decode($construktor);
    $price = 0;

    if($products) {
        foreach ($products as $product) {
            $price += \App\Product::find($product->itemId)->price * $product->count;
        }
    }

    return $price;

}