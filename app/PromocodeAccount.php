<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromocodeAccount extends Model
{
    public $timestamps = false;
}
