<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use LaravelAdminPanel\Traits\Cropper;


class Galleryphoto extends Model
{
    use Cropper;
}
