<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class Account extends Authenticatable
{
    use Notifiable;

    public $remember_token = false;

    public function promocode() {
        return $this->hasOne(Promocode::class, 'account_id', 'id');
    }

    public function balance() {
        return $this->hasMany(Balance::class, 'account_id', 'id');
    }
}
