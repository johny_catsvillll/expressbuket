<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Promocode extends Model
{
    public function accountId() {
        return $this->belongsTo(Account::class, 'account_id');
    }
}
