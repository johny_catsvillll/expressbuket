<?php

namespace App\Providers;

use App\City;
use App\Magazine;
use App\Phone;
use App\TopPage;
use Illuminate\Support\ServiceProvider;
use App\Page;
use View;
use Request;
use App\Category;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::share('categories', $this->categories());
        View::share('pages', $this->pages());
        View::share('seo', $this->getSeoPage());
        View::share('magazines', $this->getMagazines());
        view::share('phones', $this->getPhones());

    }

    public function getPhones() {
        $phones = Phone::get();
        return $phones;
    }

    public function categories() {
        return Category::whereNull('parent_id')->with('children')->get();
    }

    public function pages() {
        return TopPage::orderBy('order')->get();
    }


    public function getSeoPage() {

        $data = false;

        $page = Page::where('slug', Request::path())->first();

        if($page) {
            return $page;
        }


        return $data;

    }

    public function getMagazines() {
        return Magazine::all();
    }


}
