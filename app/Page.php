<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use LaravelAdminPanel\Traits\Cropper;
use LaravelAdminPanel\Traits\Translatable;
use Illuminate\Database\Eloquent\Builder;

class Page extends Model
{
    use Cropper;
    use Translatable;

    protected $translatable = ['title', 'slug', 'body', 'top_text', 'bottom_text'];

    /**
     * Statuses.
     */
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_INACTIVE = 'INACTIVE';

    /**
     * List of statuses.
     *
     * @var array
     */
    public static $statuses = [self::STATUS_ACTIVE, self::STATUS_INACTIVE];

    protected $guarded = [];

    public function save(array $options = [])
    {
        // If no author has been assigned, assign the current user's id as the author of the post
        if (!$this->author_id && Auth::user()) {
            $this->author_id = Auth::user()->id;
        }

        parent::save();
    }

    /**
     * Scope a query to only include active pages.
     *
     * @param  $query  \Illuminate\Database\Eloquent\Builder
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', static::STATUS_ACTIVE);
    }

    public function cityId() {
        return $this->belongsTo(City::class, 'city_id');
    }

    protected static function boot()
    {
        parent::boot();

        $uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';

        if($uri && strpos($uri, 'admin') === false || isset(request()->city_id)) {
            static::addGlobalScope('domain', function (Builder $builder) {

                if(request()->city_id) {
                    $builder->where('city_id', '=', request()->city_id);

                }
                else {
                    $builder->where('city_id', '=', get_subdomain_id());
                }
            });
        }

    }



}
