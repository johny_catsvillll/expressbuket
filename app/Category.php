<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Category extends Model
{


    public function parentId()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id')
            ->with('children');
    }

    public function cityId() {
        return $this->belongsTo(City::class, 'city_id');
    }

    protected static function boot()
    {
        parent::boot();

        $uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
        if($uri && strpos($uri, 'admin') === false || isset(request()->city_id)) {
            static::addGlobalScope('domain', function (Builder $builder) {

                if(request()->city_id) {
                    $builder->where('city_id', '=', request()->city_id);

                }
                else {
                    $builder->where('city_id', '=', get_subdomain_id());
                }
            });
        }
    }
}
