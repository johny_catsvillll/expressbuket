<?php

namespace App\Console\Commands;

use App\Article;
use App\City;
use App\News;
use App\Product;
use App\TopPage;
use Illuminate\Console\Command;
use LaravelAdminPanel\Models\Category;
use Artisan;

class XmlSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'xml:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */



    public function handle()
    {

        $cities = City::all();

        foreach ($cities as $city) {

            if($city->id != 1) {
                $link = 'https://'.$city->slug.'.expressbuket.com';
            }
            else {
                $link = 'https://expressbuket.com';
            }

            $base = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml"></urlset>';
            $xmlbase = new \SimpleXMLElement($base);

            $dom = new \DOMDocument('1.0');
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;

            $row = $xmlbase->addChild("url");
            $row->addChild('loc', $link.'/');
            $row->addChild('changefreq', 'weekly');
            $row->addChild('priority', '1.0');

            $pages = TopPage::all();

            foreach ($pages as $page) {
                $row = $xmlbase->addChild("url");
                $row->addChild('loc', $link.'/page/'.$page->slug);
                $row->addChild('changefreq', 'daily');
                $row->addChild('priority', '0.8');
            }

            $news = News::where('city_id', $city->id)->get();

            foreach ($news as $new) {
                $row = $xmlbase->addChild("url");
                $row->addChild('loc', $link.'/novosti/'.$new->slug);
                $row->addChild('changefreq', 'daily');
                $row->addChild('priority', '0.5');
            }

            $articles = Article::where('city_id', $city->id)->get();

            foreach ($articles as $article) {
                $row = $xmlbase->addChild("url");
                $row->addChild('loc', $link.'/stati/'.$article->slug);
                $row->addChild('changefreq', 'daily');
                $row->addChild('priority', '0.5');
            }

            $categories = Category::all();

            foreach ($categories as $category) {
                $row = $xmlbase->addChild("url");

                if(!$category->parent_id) {
                    $row->addChild('loc', $link.'/'.$category->slug);
                }
                else {
                    $row->addChild('loc', $link.'/'.$category->parentId->slug.'/'.$category->slug);
                }

                $row->addChild('changefreq', 'weekly');
                $row->addChild('priority', '0.5');
            }

            $products = Product::where('publication', 1)->get();

            foreach ($products as $product) {
                $row = $xmlbase->addChild("url");
                $row->addChild('loc', $link.'/buket/'.$product->slug);
                $row->addChild('changefreq', 'weekly');
                $row->addChild('priority', '0.5');
            }

            $dom->loadXML($xmlbase->asXML());
            $dom->save(public_path()."/sitemap_". $city->slug .".xml");
        }




    }
}
