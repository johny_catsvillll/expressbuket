<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    public function cityId() {
        return $this->belongsTo(City::class, 'city_id');
    }


    public function save(array $options = []) {

        if($this->status == 3 && !$this->is_active) {
            $this->is_active = 1;
            $balance10 = new Balance;
            $balance10->account_id = $this->account_id;
            $balance10->types = 2; // кешбек

            $p = $this->allPrice - $this->bonuses;

            if($p < 0) {
                $p = 0;
            }
            else {
                $p = $p * 10 / 100;
            }


            $balance10->price = $p;
            $balance10->order_id = $this->id;
            $balance10->save();

            if($this->promocode) {
                $promo = Promocode::where('id', $this->promocode)->where('active', 1)->first();
                $isUses = PromocodeAccount::where('promocode_id', $promo->id)->where('account_id', $this->account_id)->first();
                if($promo->account_id && !$isUses) {
                    $balance = new Balance;
                    $balance->account_id = $this->account_id;
                    $balance->price = 500;
                    $balance->types = 1;
                    $balance->save();

                    $balance10Cashback = new Balance;
                    $balance10Cashback->account_id = $promo->account_id;
                    $balance10Cashback->types = 2; // кешбек
                    $balance10Cashback->price = 500;
                    $balance10Cashback->save();

                    $isUse = new PromocodeAccount;
                    $isUse->account_id = $this->account_id;
                    $isUse->promocode_id = $promo->id;

                    $isUse->save();

                }
            }


        }

        parent::save();
    }

    public function accountId() {
        return $this->belongsTo(Account::class, 'account_id');
    }
}
