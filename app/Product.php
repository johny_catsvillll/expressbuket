<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use LaravelAdminPanel\Models\Category;
use LaravelAdminPanel\Traits\Cropper;
use Illuminate\Database\Eloquent\Builder;


class Product extends Model
{
    use Cropper;

    public function categoryId() {
        return $this->belongsToMany(Category::class, 'product_categories', 'product_id', 'category_id');
    }

    public function filterFlowers() {
        return $this->belongsToMany(FilterFlower::class, 'product_flowers', 'product_id', 'flower_id');
    }

    public function filterWhom() {
        return $this->belongsToMany(FilterWhom::class, 'product_whoms', 'product_id', 'whom_id');
    }

    public function filterType() {
        return $this->belongsToMany(FilterComposition::class, 'product_types', 'product_id', 'type_id');
    }

    public function filterStyle() {
        return $this->belongsToMany(FilterStyle::class, 'product_styles', 'product_id', 'style_id');
    }

    public function filterFlowersList() {
        return FilterFlower::all();
    }

    public function filterWhomList() {
        return FilterWhom::get();
    }

    public function getUrlAttribute() {
        return '/buket/'. $this->slug;
    }


    public function save(array $options = []) {

        if(isset(request()->all()['construktor-string'])) {
            $this->construktor_sting = request()->all()['construktor-string'];
        }

        parent::save();

        $filterWhom = request()->filter_whom;
//        if($filterWhom) {
        $this->filterWhom()->sync($filterWhom);
//        }

        $filterFlowers = request()->filter_flowers;

//        if($filterFlowers) {
        $this->filterFlowers()->sync($filterFlowers);
//        }

        $filterCategories = request()->category_id;
//        if($filterCategories) {
        $this->categoryId()->sync($filterCategories);
//        }

        $filterStyle = request()->filter_style;
//        if($filterStyle) {
        $this->filterStyle()->sync($filterStyle);
//        }

        $filterType = request()->filter_type;
//        if($filterType) {
        $this->filterType()->sync($filterType);
//        }

    }

    public function relationFilterWhom() {
        return $this->filterWhom();
    }

    public function relationFilterFlower() {
        return $this->filterFlowers();
    }

    public function relationCategory() {
        return $this->categoryId();
    }

    public function cityId() {
        return $this->belongsTo(City::class, 'city_id');
    }

    protected static function boot()
    {
        parent::boot();

        $uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
        if($uri && strpos($uri, 'admin') === false || isset(request()->city_id)) {
            static::addGlobalScope('domain', function (Builder $builder) {

                if(request()->city_id) {
                    $builder->where('city_id', '=', request()->city_id);

                }
                else {
                    $builder->where('city_id', '=', get_subdomain_id());
                }
            });
        }

    }



}
