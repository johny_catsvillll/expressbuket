<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CitiesProductPrice extends Model
{

    public function cityId() {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function productId() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function cityIdList() {
        return City::where('id', '!=', 1)->get();
    }

    public function productIdList()
    {
        if (isset(request()->crud_id)) {
            return Product::where('id', request()->crud_id)->get();
        }
        return Product::get();
    }

}
