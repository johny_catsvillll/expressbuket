<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


$domain = get_subdomain();

if($domain == 'volgograd') {
    $arr = [];
}
else {
    $arr = ['domain' => $domain.'.expressbuket.loc'];
}

Route::group($arr, function() {

    Route::get('/robots.txt', function () {

        echo 'User-agent: Google<br>';
        echo 'Host: '. env('APP_SCHEMA').'://'.$_SERVER['HTTP_HOST'].'<br>';
        echo 'Sitemap:'. env('APP_SCHEMA').'://'.$_SERVER['HTTP_HOST'].'/sitemap.xml<br>';

        echo 'User-agent: Yandex<br>';
        echo 'Host: '. env('APP_SCHEMA').'://'.$_SERVER['HTTP_HOST'].'<br>';
        echo 'Sitemap:'. env('APP_SCHEMA').'://'.$_SERVER['HTTP_HOST'].'/sitemap.xml<br>';
    });

    Route::get('/sitemap.xml', function () {
        header("Content-Type: text/xml;");
        $domain = get_subdomain();
        $file = file_get_contents(public_path().'/sitemap_'.$domain.'.xml');
        echo $file;
    });

    Route::get('/', [
        'as' => 'home',
        'uses' => 'PageController@home'
    ]);

    Route::get('akciya', [
        'uses' => 'PageController@discounts'
    ]);

    Route::match(['get', 'post'], 'search', [
        'uses' => 'PageController@search'
    ]);

    $categoriesSlug = \LaravelAdminPanel\Models\Category::whereNull('parent_id')->get();
    $subcategoriesSlug = \LaravelAdminPanel\Models\Category::whereNotNull('parent_id')->get();
    $filteredCategory = implode('|', $categoriesSlug->pluck('slug')->toArray());
    $filteredSubCategory = implode('|', $subcategoriesSlug->pluck('slug')->toArray());

    Route::get('/{category}', [
        'uses' => 'CatalogController@catalog'
    ])->where('category', $filteredCategory);

    Route::get('{category}/{subcategory}', [
        'uses' => 'CatalogController@catalog'
    ])
        ->where('category', $filteredCategory)
        ->where('subcategory', $filteredSubCategory);

    Route::group(['prefix' => 'account'], function() {

        Route::get('/', [
            'uses' => 'AccountController@home'
        ]);

        Route::get('/bonus', [
            'uses' => 'AccountController@bonus'
        ]);

        Route::get('/rules', [
            'uses' => 'AccountController@rules'
        ]);

        Route::match(['get', 'post'], '/info', [
            'uses' => 'AccountController@info'
        ]);

        Route::match(['get', 'post'], '/notifications', [
            'uses' => 'AccountController@notifications'
        ]);

        Route::get('/logout', [
            'uses' => 'AuthController@logout'
        ]);

        Route::get('/remove-order/{orderId}', [
            'uses' => 'AccountController@removeOrder'
        ]);

        Route::get('/repeat-order/{orderId}', [
            'uses' => 'AccountController@repeatOrder'
        ]);

    });



    Route::group(['prefix' => 'page'], function() {

        Route::get('o-kompanii', [
            'uses' => 'PageController@aboutCompany'
        ]);

        Route::get('katalog', [
            'as' => 'catalog',
            'uses' => 'CatalogController@catalog'
        ]);

        Route::get('novosti', [
            'uses' => 'PageController@news'
        ]);

        Route::get('dostavka', [
            'uses' => 'PageController@dostavka'
        ]);

        Route::get('oplata', [
            'uses' => 'PageController@oplata'
        ]);

        Route::get('stati', [
            'uses' => 'PageController@articles'
        ]);

        Route::match(['get', 'post'], 'otzyvy', [
            'uses' => 'PageController@reviews'
        ]);

        Route::get('kontakty', [
            'uses' => 'PageController@contacts'
        ]);

        Route::get('politika-konfidentsialnosti', [
            'uses' => 'PageController@policy'
        ]);

        Route::get('faq', [
            'uses' => 'PageController@faq'
        ]);

        Route::get('fotogalereya', [
            'uses' => 'PageController@gallery'
        ]);

        Route::get('preimuschestva', [
            'uses' => 'PageController@pluses'
        ]);

        Route::get('korzina', [
            'uses' => 'CatalogController@cart'
        ]);

        Route::get('sitemap', [
            'uses' => 'PageController@sitemap'
        ]);

        Route::get('favorite', [
            'uses' => 'PageController@favorite'
        ]);

    });

    Route::post('/site/order', [
        'uses' => 'OrderController@order'
    ]);

    Route::get('/stati/{slug}', [
        'uses' => 'PageController@article'
    ]);

    Route::get('/novosti/{slug}', [
        'uses' => 'PageController@oneNew'
    ]);

    Route::get('/add-to-cart', [
        'uses' => 'OrderController@addToCart'
    ]);

    Route::match(['get', 'post'], '/order2', [
        'uses' => 'OrderController@order2'
    ]);

    Route::match(['get', 'post'], '/site/success', [
        'uses' => 'OrderController@success'
    ]);

    Route::get('/add-to-favorite', [
        'uses' => 'PageController@addToFavorite'
    ]);

    Route::post('/save-application', [
        'uses' => 'PageController@saveApplication'
    ]);

    Route::get('/clear-list-favorite', [
        'uses' => 'PageController@clearListFavorite'
    ]);

    Route::get('/site/quick', [
        'uses' => 'OrderController@quickOrder'
    ]);

    Route::get('/findpromo', [
        'uses' => 'OrderController@findpromo'
    ]);

    Route::get('/check-order', [
        'uses' => 'OrderController@checkOrder'
    ]);

    Route::get('/status-order', [
        'uses' => 'OrderController@statusOrder'
    ]);

    Route::get('/check-number-order', [
        'uses' => 'OrderController@checkNumberOrder'
    ]);

    Route::get('/buket/{product}', [
        'uses' => 'CatalogController@product',
        'as' => 'product'
    ]);

    Route::get('/change-status-order/{id}', [
        'uses' => 'OrderController@changeStatusOrder'
    ]);




});


Route::get('/check-is-user', [
    'uses' => 'AuthController@checkIsUser'
]);

Route::group(['prefix' => 'admin'], function () {
    Admin::routes();
});


